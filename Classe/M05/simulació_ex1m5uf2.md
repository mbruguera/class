# SIMULACIÓ DEL PARCIAL 
#### 1. Respon a les següents preguntes:
##### a) Com podem saber si una adreça IPv4 és de classe A, B o C?

Classe A: de la 0.0.0.0 fins a la 127.255.255.255

Classe B: de la 128.0.0.0 fins a la 191.255.255.255

Classe C: de la 192.0.0.0 fins a la 223.0.0.0

##### b) Com podem saber si una adreça és pública o privada?

Com a la pregunta anterior, hi ha un rang especific de IP's privades:

```
10.0.0.0 – 10.255.255.255
172.16.0.0 – 172.31.255.255
192.168.0.0 – 192.168.255.255
169.254.0.0 – 169.254.255.255
```

La resta son públiques.

##### c) Com podem saber si una adreça és classfull o classless?

Per si la mascara de la ip per defecta es la mateixa que la de la seva mascara. Exemple:

Si tenim un 172.15.0.0/24 es classless ja que aquesta ip es una classe b amb el /16 com a mascara per defecte.

##### d) Omple la següent taula:
|IP|Classe|Pub o Priv|Classful o classless|
|-|-|-|-|
|172.15.0.0/24|Classe B|pública|classless|
|172.31.62.0/24|Classe B|privada|classless|
|10.5.0.0/16|Classe A|privada|classless|
|80.0.0.0/8|Classe A|pública|classful|
|200.0.0.0/8|Classe C|pública|classless|
|192.168.0.0/16|Classe C|privada|classless|
|130.100.0.0/16|Classe B|pública|classful|
|192.169.50.0/24|Classe C|privada|classful|

#### 2. Donada la xarxa 192.168.50.0/24, volem crear 5 subxarxes que parteixin d'aquesta.
#### a) Quants bits hem de dedicar a la part de les subxarxes i com l’has calculat?
2^n += 5+2
n = 3

##### b) Quina és la nova màscara de xarxa de les subxarxes i com l’has calculat?
255.255.255.224 o /27

##### c) Construeix la taula on hi apareguin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal
|  |xarxa|subxarxa|host|ax en decimal|
|-|-|-|-|-|
|X1|192.168.50.0|/27|50.1--50.30||      
|X2|192.168.50.32|/27|50.33--50.62||
|X3|192.168.50.64|/27|50.65--50.94||
|X4|192.168.50.96|/27|50.97--50.126||
|X5|192.168.50.128|/27|50.129--50.158||
|X6|192.168.50.160|/27|50.161--50.190||
|X7|192.168.50.192|/27|50.193--50.222||
|X8|192.168.50.224|/27|50.225--50.254||

#### 3. Donada la xarxa 172.20.0.0/16, volem crear tantes xarxes com sigui possible, de 5000 dispositius cadascuna d’elles.
##### a) Quants bits hem de dedicar a la part dels hosts i com l’has calculat?

##### b) Quants bits hem de dedicar a la part de les subxarxes? Quantes subxarxes diferents podem fer? Justifica amb càlculs la resposta.

##### c) Quina és la nova màscara de xarxa de les subxarxes i com l’has calculat?

##### d) Construeix la taula on hi apareguin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal

| |xarxa|subxarxa|host|ax en decimal|
|-|-----|--------|----|-------------|



#### 4. Donada la xarxa mare 10.0.0.0/8, volem fer un repartiment de la xarxa ajustat al número de dispositius de cada departament:

- Informàtica: 200 hosts
- Administratius: 500 hosts
- Soldadura: 50 hosts
- WAN: 2 hosts

##### a) Per a cada xarxa, calcula la nova MX necessària, incloent els càlculs necessaris:

##### b) Per a cada xarxa, calcula’n l’adreça de Xarxa i l’adreça de Broadcast:

#### 5. Donades les següents IPv6, omple la següent taula. ~~Si expresses alguna part de l’adreça en binari, subratlla-la.~~

|IP|Part de prefix|Bits de la part de prefix|Part d'interfície|Bits de la part d'interfície|
|-|-|-|-|-|
|ABCD:EF12:345::/12|||||
|ABCD:ED12:345::/14|||||
|ABCD:EF12:345::/34|||||
|ABCD:EF12:345::/38|||||

#### 6. Observa el següent diagrama de xarxa:
![diagrama](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/Screenshot_from_2023-02-14_09-41-22.png)

Suposant que només disposem del rang d’adreces de la xarxa 192.168.100.0/24 i que volem crear 4 subxarxes on
hi càpiguen el màxim de PC possible:

##### a) Ompliu la següent taula on s’especifica quina és l’adreça de xarxa (és a dir, on comencen) de cadascuna de les xarxes:
|Xarxa|Adreça de xarxa|Màscara de xarxa en CIDR|
|-|-|-|
|X1 (PC0 I PC1)|||
|X2 (PC2 I PC3)|||
|X3 (PC4 I PC5)|||
|X4 (PC6 I PC7)|||

##### b) Ompliu la següent taula que correspon a la configuració de les adreces IP de cadascun dels PC:
|PC|Adreça IP|Màscara de xarxa en decimal|Gateway|
|-|-|-|-|
|PC0||||
|PC1||||
|PC2||||
|PC3||||
|PC4||||
|PC5||||
|PC6||||
|PC7||||

##### c) Les següents adreces IP serien assignables a un dispositiu en la xarxa que es demana? Per què?

|Xarxa|IP|És assignable a un dispositiu d'aquesta xarxa?|
|-|-|-|
|x1|192.168.100.70||
|x2|192.168.100.64||
|x3|192.168.100.191||
|x4|192.168.100.254||
