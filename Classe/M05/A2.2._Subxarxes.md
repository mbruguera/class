# A2.2. Subxarxes
## 1. Calcula l’adreça de xarxa i de broadcast, donades aquestes IP:

a) 172.16.4.12/8

    ADREÇA DE XARXA: 172.0.0.0
    ADREÇA DE BRODCAST: 172.255.255.255

b) 172.16.4.12/16
    
    ADREÇA DE XARXA: 172.16.0.0
    ADREÇA DE BRODCAST: 172.16.255.255

c) 172.16.4.12/24

    ADREÇA DE XARXA: 172.16.4.0
    ADREÇA DE BRODCAST: 172.16.4.255

d) 172.16.4.12/11

    ADREÇA DE XARXA: 172.224.0.0
    ADREÇA DE BRODCAST: 172.255.255.255

e) 172.16.4.12/12

    ADREÇA DE XARXA: 172.240.0.0
    ADREÇA DE BRODCAST: 172.255.255.255


## 2. Calcula el host mínim, màxim i el número de dispositius disponibles de cadascuna de les adreces anteriors.

a) 172.16.4.12/8

    ADREÇA HOST MINIMA: 172.0.0.1
    ADREÇA HOST MAXIMA: 172.255.255.254
    Nº DE DISPOSITIUS: 16581375

b) 172.16.4.12/16
    
    ADREÇA HOST MINIMA: 172.16.0.1
    ADREÇA HOST MAXIMA: 172.16.255.254
    Nº DE DISPOSITIUS: 65025

c) 172.16.4.12/24

    ADREÇA HOST MINIMA: 172.16.4.1
    ADREÇA HOST MAXIMA: 172.16.4.254
    Nº DE DISPOSITIUS: 255

d) 172.16.4.12/11

    ADREÇA HOST MINIMA: 172.224.0.1
    ADREÇA HOST MAXIMA: 172.255.255.254
    Nº DE DISPOSITIUS: 2097150

e) 172.16.4.12/12

    ADREÇA HOST MINIMA: 172.240.0.1
    ADREÇA HOST MAXIMA: 172.255.255.254
    Nº DE DISPOSITIUS: 1048578

## 3. En una empresa tenim actualment 2 subxarxes ben diferenciades: la de servidors i la d'ordinadors. Totes dues volem que parteixin de la xarxa mare 10.20.30.0/24. La de servidors serà la primera subxarxa i la d'ordinadors la segona. Digues:

a) Quines serien les dues adreces de xarxa de les subxarxes, inclosa la seva màscara de xarxa, tant en format CIDR com en decimal

    10.20.30.0/25
    10.20.30.0      10.20.30.1-10.20.30.126     10.20.30.127
                    10.20.30.0hhhhhhh
    
    10.20.30.128    10.20.30.129-10.20.30.254   10.20.30.255
                    10.20.30.1hhhhhhh

b) Fes un diagrama amb Packet Tracer, col·locant dos dispositius a cada xarxa i interconnectant-les entre elles amb un Router 1841. Els distribuirem de la següent manera:

    Xarxa de servidors: Server0 i Server1
    Xarxa d'ordinadors: PC0 i PC1

c) Fes les configuracions necessàries, tant als PC com al Router, per garantir la connectivitat entre els diferents PC. Fes una taula de resum de quines han sigut aquestes configuracions per a cada dispositiu. Per als routers, caldrà que configureu i activeu les dues interfícies, una per a cada xarxa:

    Des de la pestanya "Config", seleccioneu FastEthernet0/0 o FastEthernet0/1.
    Introduïu la IP i la màscara de xarxa als camps corresponents
    Activeu la interfície amb el checkbox que diu "On", a la part superior

## 4. Donada la xarxa 172.16.0.0/16, volem crear 3 subxarxes que parteixin d'aquesta.

a) Quants bits hem de dedicar a la part de les subxarxes?

    3

b) Quina és la nova màscara de xarxa de les subxarxes?

    /19

c) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX

	Part de xarxa
	Part de subxarxa
	Part de host
	 Adreça en decimal i màscara

|ADREÇA DE XARXA|RANG DE XARXES             |BRODCAST      |MÀSCARA      |
|---------------|---------------------------|--------------|-------------|
|172.16.0.0     |172.16.0.1-172.16.63.254   |172.16.63.255 |255.255.192.0|
|172.16.64.0    |172.16.64.1-172.16.127.254 |172.16.127.255|255.255.192.0|
|172.16.128.0   |172.16.128.1-172.16.191.254|172.16.191.255|255.255.192.0|
|172.16.192.0   |172.16.192.1-172.16.255.254|172.16.255.255|255.255.192.0|				 

## 5. Donada la xarxa 10.0.0.0/8, volem crear 9 subxarxes que parteixin d'aquesta.

a) Quants bits hem de dedicar a la part de les subxarxes?

    2^4=16  2 bits

b) Quina és la nova màscara de xarxa de les subxarxes?

    10.0.0.0/12 255.240.0.0

c) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX

|ADREÇA DE XARXA|RANG DE XARXES             |BRODCAST      |MÀSCARA      |
|---------------|---------------------------|--------------|-------------|
|10.0.0.0       |10.0.0.1 - 10.15.255.254   |10.15.255.255 |255.240.0.0  |
|10.16.0.0      |10.16.0.1 - 10.31.255.254  |10.31.255.255 |255.240.0.0  |
|10.32.0.0      |10.32.0.1 - 10.47.255.254  |10.47.255.255 |255.240.0.0  |
|10.48.0.0      |10.48.0.1 - 10.63.255.254  |10.63.255.255 |255.240.0.0  |	
|10.64.0.0	    |10.64.0.1 - 10.79.255.254	|10.79.255.255 |255.240.0.0  |
|10.80.0.0	    |10.80.0.1 - 10.95.255.254	|10.95.255.255 |255.240.0.0  |
|10.96.0.0	    |10.96.0.1 - 10.111.255.254	|10.111.255.255|255.240.0.0  |
|10.112.0.0	    |10.112.0.1 - 10.127.255.254|10.127.255.255|255.240.0.0  |
|10.128.0.0	    |10.128.0.1 - 10.143.255.254|10.143.255.255|255.240.0.0  |
|10.144.0.0	    |10.144.0.1 - 10.159.255.254|10.159.255.255|255.240.0.0  |
|10.160.0.0	    |10.160.0.1 - 10.175.255.254|10.175.255.255|255.240.0.0  |
|10.176.0.0	    |10.176.0.1 - 10.191.255.254|10.191.255.255|255.240.0.0  |


## 6. Donada la xarxa 192.168.0.0/24, volem crear tantes subxarxes possibles on hi càpiguen almenys 60 dispositius

a) Quants bits hem de dedicar a la part dels dispositius?

    2^6= 64
    6 bits

b) Quants bits hem de dedicar a la part de les subxarxes? Quantes subxarxes diferents podem fer?

    6 bits i podem fer-hi 4 subxarxes.

c) Quina és la nova màscara de xarxa de les subxarxes?

    192.168.0.0/26

d) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX

|ADREÇA DE XARXA|RANG DE XARXES               |BRODCAST     |MÀSCARA        |
|---------------|-----------------------------|-------------|---------------|
|192.168.0.0    |192.168.0.1 - 192.168.0.62	  |192.168.0.63 |255.255.255.192|
|192.168.0.64	|192.168.0.65 - 192.168.0.126 |192.168.0.127|255.255.255.192|
|192.168.0.128	|192.168.0.129 - 192.168.0.190|192.168.0.191|255.255.255.192|
|192.168.0.192	|192.168.0.193 - 192.168.0.254|192.168.0.255|255.255.255.192|

## 7. Donada la xarxa 192.168.200.0/24, volem crear tantes subxarxes possibles on hi càpiguen almenys 32 dispositius (assegura't que hi càpiguen tots!)

a) Quants bits hem de dedicar a la part dels dispositius?

    2^5=32

b) Quants bits hem de dedicar a la part de les subxarxes? Quantes subxarxes diferents podem fer?

    5 bits per fer-hi subxarxes, podem fer-hi 8 subxarxes.

c) Quina és la nova màscara de xarxa de les subxarxes?

    192.168.200.0/29

d) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX

|ADREÇA DE XARXA|RANG DE XARXES                   |BRODCAST       |MÀSCARA        |
|---------------|---------------------------------|---------------|---------------|
|192.168.200.0	|192.168.200.1 - 192.168.200.30	  |192.168.200.31 |255.255.255.224|
|192.168.200.32	|192.168.200.33 - 192.168.200.62  |192.168.200.63 |255.255.255.224|
|192.168.200.64	|192.168.200.65 - 192.168.200.94  |192.168.200.95 |255.255.255.224|
|192.168.200.96	|192.168.200.97 - 192.168.200.126 |192.168.200.127|255.255.255.224|
|192.168.200.128|192.168.200.129 - 192.168.200.158|192.168.200.159|255.255.255.224|
|192.168.200.160|192.168.200.161 - 192.168.200.190|192.168.200.191|255.255.255.224|
|192.168.200.192|192.168.200.193 - 192.168.200.222|192.168.200.223|255.255.255.224|
|192.168.200.224|192.168.200.225 - 192.168.200.254|192.168.200.255|255.255.255.224|

Exercicis extra (opcionals):

## 8. Donada la xarxa 172.16.0.0/22, volem crear 7 subxarxes que parteixin d'aquesta.

a) Quants bits hem de dedicar a la part de les subxarxes?

    3 bits per la part de subxarxes

b) Quina és la nova màscara de xarxa de les subxarxes?

172.16.0.0 **/22**

255.255.255.128

c) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX

|ADREÇA DE XARXA|RANG DE XARXES             |BRODCAST    |MÀSCARA        |
|---------------|---------------------------|------------|---------------|
|172.16.0.0	    |172.16.0.1 - 172.16.0.126  |172.16.0.127|255.255.255.128|
|172.16.0.128   |172.16.0.129 - 172.16.0.254|172.16.0.255|255.255.255.128|
|172.16.1.0     |172.16.1.1 - 172.16.1.126  |172.16.1.127|255.255.255.128|
|172.16.1.128   |172.16.1.129 - 172.16.1.254|172.16.1.255|255.255.255.128|
|172.16.2.0     |172.16.2.1 - 172.16.2.126  |172.16.2.127|255.255.255.128|
|172.16.2.128   |172.16.2.129 - 172.16.2.254|172.16.2.255|255.255.255.128|
|172.16.3.0     |172.16.3.1 - 172.16.3.126  |172.16.3.127|255.255.255.128|
|172.16.3.128   |172.16.3.129 - 172.16.3.254|172.16.3.255|255.255.255.128|

## 9. Donada la xarxa 192.168.100.0/23, volem crear tantes subxarxes possibles on hi càpiguen almenys 50 dispositius.

a) Quants bits hem de dedicar a la part dels dispositius?

    6 bits 

b) Quants bits hem de dedicar a la part de les subxarxes? Quantes subxarxes diferents podem fer?

    3 bits per subxarxes    

c) Quina és la nova màscara de xarxa de les subxarxes?

    255.255.255.218 /29

d) Construeix la taula on hi apareixin les diferents parts de l'adreça i, finalment, expressa l'adreça de xarxa de cada subxarxa en decimal i la seva MX
