# M05
## 
## Xarxes punt a punt
Una xarxa punt a punt es pot utilitzar que le client sigui un servidor i viceversa. Aixo va molt bé per una petita xarxa per intercanviar arxius. Una ventatja es que cualsevol host pot ser client o servidor.

Un exemple pot ser una impressora, ens permet *descentralitzar* un host per només una operacio (en aquest cas imprimir) i fer-ho desde un host cualsevol. En un arxiu torrent, lo que fa es poder baixar la pelicula en cuestió sino que també puja l'arxiu per poder fer un mirror de la propia URL i que demés usuaris puguin descarregar lo mateix [model hibrid]. 

## Servidors DNS
A dia d'avui es practicament imposible que només hi hagi un unic servidor per tot el planeta. Una cosa que fa el DNS es guardar una ip i donar-li un nom (ej: 198.68.10.1 amb el nom pepito.com) per poder utilitzarlo al navegador.

Pero per no poder colapsar el servidor en res, lo que es fa son macro-xarxes de servidors. 

Per poder entrar a un client de alguna pagina web s'ha de fer el seguent.

**DNS RAÍZ > SERVIDOR AMB (.org/.com/.es/etc) > Servidor de la pagina en concret > Client per pantalla.**

**nslookup** lo que fa aquesta comanda es dir-te per quins servidors passa fins arribar al client.
```
[ism49298715@a10 ~]$ nslookup www.ara.cat
Server:		10.200.240.10
Address:	10.200.240.10#53

Non-authoritative answer:
www.ara.cat	canonical name = www.ara.cat.cdn.bitban.net.
www.ara.cat.cdn.bitban.net	canonical name = caching.ara.edge2befaster.com.
caching.ara.edge2befaster.com	canonical name = cec02.esg.edgetcdn.com.
Name:	cec02.esg.edgetcdn.com
Address: 31.170.100.122
Name:	cec02.esg.edgetcdn.com
Address: 185.99.186.179
Name:	cec02.esg.edgetcdn.com
Address: 31.170.100.121
Name:	cec02.esg.edgetcdn.com
Address: 185.103.39.27
```

## HTTP
El client demana una adreça, el protocol HTTP t'envia un arxiu .html i es lo que veus per pantalla. A dia d'avui la gran majoria de pagines estan majoritariament construides amb *.html*.

## WireShark
Capturem tot el que pasa a l'hora de connectarnos en un servidor web. Ho farem escribient en el navegador la IP, ja que le domini no te servidor DNS.

L'adreça es **10.200.247.245**. La primera vegada si que hi haurá connexió pero despres no, ja que anira a la memoria caché pero poder establir una connexió, connexió que no serveix. Per poder tornar-ho a fer es pot o esborrant les dades del navegador o en una navegacióó privada.

Ens hem de conectar a dos locs:
- 10.200.247.245
- 10.200.247.245/index2.html

Per poder veure el transit de la xarxa que hi ha entre nosaltres i la pagina web (10.200.247.245 o 10.200.247.245/index.html) hem de fer un filtre. Aquets filtres es fan de la seguent manera amb la pagina ja iniciada:
```
ip.addr == (La IP corresponent)
ip.addr == 10.200.247.245
```
I t'ha de sortir tot el transit de la propia xarxa. En ella podem trobar el protocol HTTP i el TCP, que son els que de moment, la informació que ens dona dels paquets. Els protocols HTTP, en alguns, venen el HTML de la pagina. En el cas de la pagina aquesta, vindría el seguent codi:
```
<!DOCTYPE html>
    <html>
        <body>
            <h1>Benvinguts a les Xarxes2</h1>
            <p>Servidor web2</p>
            <img src="Network2.png" alt="W3Schools.com" width="800" height="400$>
        </body>
    </html>
```
Aquest codi es codi font (index2) de la pagina que hem vist abans (10.200.247.245/index2.html).

