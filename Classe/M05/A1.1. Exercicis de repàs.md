## Exercici 1
Des del Wireshark, captura dades del protocol TCP (fent una connexió HTTP, per exemple) i respon a les següents preguntes.

a)  De quina capa és el protocol TCP?
    - De la capa de **transport**

b)  Quin protocol d’aplicació coneixes que utilitzi el protocol TCP? Filtra per aquest protocol per tal de capturar les dades.

c) Quina és la mida de la capçalera TCP? Aporta una captura de pantalla que ho demostri.

Per obtenir la mida de la capçalera TCP ho podeu fer amb l'opció "Show packet bytes" que hi ha al menú que apareix fent clic amb el botó dret sobre la capçalera TCP.
