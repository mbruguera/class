1. Fes una relació de les diferents interfícies de xarxa i les IP que li assignaràs, d’acord a l’adreça de xarxa de la xarxa on es troben. Utilitza els números més baixos de la xarxa, començant pel “gateway” i seguint per la resta de dispositius.

|Dispositius|Interfície de xarxa|Adreça IP|
|-|-|-|
|PC0|FastEthernet 0/0|10.0.0.5|
|PC1|FastEthernet 0/0|10.0.1.5|
|PC2|FastEthernet 0/0|10.0.2.5|
|PC3|FastEthernet 0/0|10.0.2.6|
|PC4|FastEthernet 0/0|10.0.4.5|
|PC5|FastEthernet 0/0|10.0.5.5|
|Switch0|FastEthernet 0/0|NO USA IP|
|Switch1|FastEthernet 0/0|NO USA IP|
|Switch2|FastEthernet 0/0|NO USA IP|
|Switch4|FastEthernet 0/0|NO USA IP|
|Switch5|FastEthernet 0/0|NO USA IP|
|Router0|FastEthernet 0/0|10.0.0.1|
|Router0|FastEthernet 1/0|10.0.1.1|
|Router0|Serial 2/0|192.168.1.1|
|Router1|FastEthernet 0/0|10.0.5.1|
|Router1|Serial 2/0|192.168.1.5|
|Router1|Serial 3/0|192.168.1.2
|Router2|FastEthernet 0/0|10.0.2.1|
|Router2|FastEthernet 1/0|10.0.4.1|
|Router2|Serial 0/0/0|192.168.1.6|

2. Digues les taules d’enrutament que cal que tinguin configurades els tres routers per tal que tots els PC puguin connectar-se entre ells

### Router 0

|Adreça de xarxa destí|Màscara de xarxa|Ip per la qual reenviar|
|-|-|-|
|10.0.2.0|255.255.255.0|192.168.1.2|
|10.0.4.0|255.255.255.0|192.168.1.2|
|10.0.5.0|255.255.255.0|192.168.1.2|

### Router 1

|Adreça de xarxa destí|Màscara de xarxa|Ip per la qual reenviar|
|-|-|-|
|10.0.0.0|255.255.255.0|192.168.1.1|
|10.0.1.0|255.255.255.0|192.168.1.1|
|10.0.2.0|255.255.255.0|192.168.1.6|
|10.0.4.0|255.255.255.0|192.168.1.6|

### Router 2

|Adreça de xarxa destí|Màscara de xarxa|Ip per la qual reenviar|
|-|-|-|
|10.0.0.0|255.255.255.0|192.168.1.2|
|10.0.1.0|255.255.255.0|192.168.1.2|
|10.0.5.0|255.255.255.0|192.168.1.2|

3. Configura la xarxa a PacketTracer. Per a la part de configuració dels routers, enganxa en forma de text (no incloguis captures de pantalla) les comandes necessàries per modificar la taula d’enrutament dels dos routers. Les comandes apareixen a la part inferior de la pantalla cada cop que modifiques algun paràmetre de la taula d’enrutament des de la interfície gràfica.

```
ROUTER 0:
R0(config)#
%SYS-5-CONFIG_I: Configured from console by console
ip route 10.0.2.0 255.255.255.0 192.168.1.2
R0(config)#ip route 10.0.4.0 255.255.255.0 192.168.1.2
R0(config)#ip route 10.0.5.0 255.255.255.0 192.168.1.2

ROUTER 1:
R1(config)#ip route 10.0.0.0 255.255.255.0 192.168.1.1
R1(config)#ip route 10.0.1.0 255.255.255.0 192.168.1.1
R1(config)#ip route 10.0.2.0 255.255.255.0 192.168.1.6
R1(config)#ip route 10.0.4.0 255.255.255.0 192.168.1.6

ROUTER 2:
R2(config)#ip route 10.0.0.0 255.255.255.0 192.168.1.2
R2(config)#ip route 10.0.1.0 255.255.255.0 192.168.1.2
R2(config)#ip route 10.0.5.0 255.255.255.0 192.168.1.2
```

4. Demostra el correcte funcionament de la configuració enganxant en forma de text (no incloguis captures de pantalles) les comandes executades per a comprovar-ne el correcte funcionament, així com el resultat que han donat.

5. Configura el Router0 per tal que es pugui configurar remotament per SSH. Fes la prova connectant-te per SSH al Router0 des del PC0 i el PC4. Un cop connectat per SSH, executa la instrucció per mostrar per pantalla la taula d'enrutament del Router0.
