# Subneting

**Direcció de xarxa: 0 a la part de Host (H)**

**Direcció de Broadcast: 1 a la part de Host (X)**

1. 154.141.49.87

xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxhhh
					01010111 -> 87
Direcció de xarxa:
```
154.141.49.80
154.141.49.010010000
```
Direcció de Broadcast:
```
154.141.49.87
154.141.49.010010111
```
Primera i ultima direcció de Host ultilitzable:
```
154.141.49.81
154.141.49.86
```
2. 163.112.65.90

xxxxxxxx.xxxxxxxx.xxxxxhhh.hhhhhhhh
                  01000001 -> 65 en Binari  

Direcció de xarxa:    
```
163.112.64.0
163.112.01000000.00000000 
```
Direcció de Broadcast:
```
163.112.71.255
163.112.01000111.11111111 
```
Primera i ultima direcció de Host utilitzable:
```
163.112.64.1
163.112.71.254
```
3. 154.30.90.120

xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxhh
					01111000 -> 120
Direcció de xarxa:
```
154.30.90.120
154.30.90.01111000
```
Direcció de Broadcast:
```
154.30.90.123
154.30.90.01111011
```
Primera i ultima direcció de Host Ultilitzable:
```
154.30.90.121
154.30.90.122
```
4. 192.168.10.15/28

xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxhhhh
				     00001111 -> 15
Direcció de xarxa:
```
192.168.10.0
192.168.10.00000000
```
Direcció de Broadcast:
```
192.168.10.15
192.168.10.00001111
```
Primera i ultima direcció de Host Ultilitzable:
```
192.168.10.1
192.168.10.14
```	
5. 10.20.10.5/5

xxxxxhhh.hhhhhhhh.hhhhhhhh.hhhhhhhh
00001010 -> 10
Direcció de xarxa:
```
8.0.0.0
00001000.00000000.00000000.00000000
```
Direcció de Broadcast
```
15.255.255.255
00001111.11111111.11111111.11111111
```
Primera i ultima direcció de Host ultilitzable:
```
8.0.0.1
15.255.255.254
```
6. 163.10.15.90/31

xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxxh
						   01011010 -> 90
Direcció de xarxa:
```
163.10.15.90
163.10.15.01011010
```
Direcció de Broadcast
```
163.10.15.91
163.10.15.01011011
```
Primera i ultima direcció de host utilitzable

No té

### Preguntes capitol 6C
163.112.65.90/21
xxxxxxxx.xxxxxxxx.xxxxxhhh.hhhhhhhh
- Máscara: 
	255.255.248.0
- Direcció de Xarxa:
	163.112.01000000.0
	163.112.64.0
- Direcció de Broadcast: 
	163.112.01000111.11111111
	163.112.69.255
- Primera i ultima
	163.112.64.1
	163.112.69.254 

154.30.90.120/30
xxxxxxxx.xxxxxxxx.xxxxxxxx.xxxxxxhh
- Máscara:
	255.255.255.252
- Direcció de xarxa:
	154.30.90.01111000
	154.30.90.120
- Direcció de broadcast:
	154.30.90.01111111
	154.30.90.127
- Primera i ultima
	154.30.90.121
	154.30.90.126

# Activitat 6.7.3
*Problema 1*
172.30.1.33
Máscara: 
	255.255.0.0
Direcció de xarxa:
	172.30.0.0
Direcció de broadcast:
	172.30.255.255
Primer i ultim host:
	172.30.0.1
	172.30.255.254
    509 hosts

*Problema 2*
172.30.1.33
Máscara: 
	255.255.255.0
Direcció de xarxa:
	172.30.1.0
Direcció de broadcast:
	172.30.1.255
Primer i ultim host:
	172.30.1.1
	172.30.1.254
    254 hosts

*Problema 3*
192.168.10.234
Máscara:
	255.255.255.0
Direcció de xarxa:
	192.168.10.0
Direcció de Broadcast:
	192.168.10.255
Primer i ultim host:
	192.168.10.1
	192.168.10.254
    254 hosts

*Problema 4*
172.17.99.71
Máscara:
	255.255.0.0
Direcció de xarxa:
	172.17.0.0
Direcció de Broadcast:
	172.17.255.255	
Primer i ultim host:
	172.17.0.1
	172.17.255.254
	509 hosts

*Problema 5*
192.168.3.219
Máscara:
    255.255.0.0
Direcció de xarxa:
    192.168.0.0
Direcció de Broadcast:
    192.168.255.255
Primer i ultim host:
    192.168.0.1
	192.168.255.255
	509 hosts

*Problema 6*
    192.168.3.219
	192.168.3.11011011
Máscara:
    255.255.255.224
	255.255.255.11100000
Direcció de xarxa:
    192.168.3.xxxhhhhh
	192.168.3.192
Direcció de broadcast:
    192.168.3.223
	192.168.3.xxxhhhhh
Primer i ultim host
    192.168.3.193
	192.168.3.222
	30 hosts

USABLE SUBNETS: 1000
USABLE HOST: 60
NETWORK ADDRESS: 165.100.0.0
ADDRESS CLASS: B
DEFAULT SUBNET MASK: 255.255.0.0
CUSTOM SUBNET MASK: 255.255.255.192
TOTAL NUMBER OF SUBNETS: 2¹⁰ (1024)
TOTAL USABLE SUBNETS: 2¹⁰ - 2 (1022)
TOTAL NUMBER OF HOST: 2⁶ (64)	
TOTAL USABLE HOST: 2⁶ - 2 (62)
NUMBER OF BITS BORROWED: 10
CÀLCULS:
Quants bits necesito per tenir 60 host?
	2^n - 2= >/= 60
	2⁶-2= >/= 60
		Aixo diu que tinc 6 bits per host.
	xxxxxxxx (255).xxhhhhhh (192)

# Preguntes 6H
3rd usable subnet range?
0	0000 0000 -> 0
	0000 1111 -> 15

1	0001 0000 -> 16
	0001 1111 -> 31

2	0010 0000 -> 32
	0010 1111 -> 47 

3	0011 1111 -> 48
	0011 1111 -> 63		[ 192.10.10.48 >> 192.10.10.63 ]

4	0100 0000 -> 64
	0100 1111 -> 79
	
5	0101 0000 -> 80
	0101 1111 -> 96

6	0110 0000 -> 97
	0110 1111 -> 111

7	0111 0000 -> 112
	0111 1111 -> 127	[ 192.10.10.112 >> 192.10.10.127 ]

8	1000 0000 -> 128
	1000 1111 -> 143	[ 192.10.10.128 >> 192.10.10.143 ]

9	1001 0000 -> 144
	1001 1111 -> 159

10	1010 0000 -> 160
	1010 1111 -> 175

11	1011 0000 -> 176
	1011 1111 -> 191

12	1100 0000 -> 192
	1100 1111 -> 207	[ 192.10.10.192 >> 192.10.10.207 (broadcast)]



165.100.0.0
Address class: B
Default mask: 255.255.0.0
Custom mask: 255.255.255.192
Total de SUBNETS:2⁶
usable subnets:2¹⁰- 2
host addresses:2¹⁰ 
usable addresses: 2⁶- 2
bits borrowed:6

0	0000 0000 -> 0		165.100.0.0 
	0011 1111 -> 63		165.100.0.63

1	0100 0000 -> 64		165.100.0.64
	0111 1111 -> 127	165.100.0.127

2	1000 0000 -> 128	165.100.0.128
	1011 1111 -> 191	165.100.0.191

3	1100 0000 -> 192	165.100.0.192
	1111 1111 -> 255	165.100.0.255

4	0000 0000 -> 1.0	165.100.1.0
	0011 1111 -> 1.63	165.100.1.63		

5	0100 0000 -> 1.64	165.100.1.64
	0111 1111 -> 1.127	165.100.1.127

6	1000 0000 -> 1.128	165.100.1.128
	1011 1111 -> 1.191	165.100.1.191

7	1100 0000 -> 1.192	165.100.1.192
	1111 1111 -> 1.255	165.100.1.255	

8	0000 0000 -> 2.0	165.100.2.0 
	0011 1111 -> 2.63	165.100.2.63

9	0100 0000 -> 2.64	165.100.2.64
	0111 1111 -> 2.127	165.100.2.127

10	1000 0000 -> 2.128	165.100.2.128
	1011 1111 -> 2.191	165.100.2.191

11	1100 0000 -> 2.192	165.100.2.192
	1111 1111 -> 2.255	165.100.2.255		

12	0000 0000 -> 3.0	165.100.3.0 
	0011 1111 -> 3.63	165.100.3.63

13	0100 0000 -> 3.64	165.100.3.64
	0111 1111 -> 3.127	165.100.3.127

14	1000 0000 -> 3.128	165.100.3.128
	1011 1111 -> 3.191	165.100.3.191

## VLSM
192.168.15.0/24

AtlantaHQ 58 direccions host
PerthHQ 26 direccions host
SydneyHQ 10 direccions host
CorpusHQ 10 direccions host
WAN 2 direccions host
 
| NOM       | Direcció Subxarxa | RANG                            | Direcció Broadcast | XARXA/Prefix       |
| --------- | ----------------- | ------------------------------- | ------------------ | ------------------ |
| Atlanta HQ|  192.168.15.0     | 192.168.15.1 - 192.168.15.62    | 192.168.15.63      | 192.168.15.0 /26   |
| PerthHQ   |  192.168.15.64    | 192.168.15.65 - 192.168.15.94   | 192.168.15.95      | 192.168.15.64 /27  |
| SydeyHQ   |  192.168.15.96    | 192.168.15.97 - 192.168.15.110  | 192.168.15.111	   | 192.168.15.96 /28  |
| CorpusHQ  |  192.168.15.112	| 192.168.15.113 - 192.168.15.126 | 192.168.15.127     | 192.168.15.112 /28 |
| WAN       |  192.168.15.128   | 192.168.15.129 - 192.168.15.130 | 192.168.15.131	   | 192.168.15.128 /30 |
| WAN       |  192.168.15.132   | 192.168.15.133 - 192.168.15.134 | 192.168.15.135	   | 192.168.15.132 /30 |

CALCULS:
ATLANTA (58)
	2⁶ >/= 58 (64)
Perth (26)
	2⁵ >/= 26 (32)
Sydney/Corpus (10)
	2⁴ >/= 10 (16)
WAN (2)
	2² >/= 2 (4)

## Teoria
Enllaç de dades forma  part de la tarjeta de xarxa. Aquest li arriba informacio que ha de cambiar de una señal fisica (electromagnetica, optica, etc.) a xarxa. La trama que s'envia per l'enllaç de dades, passa per diferents dispositus, dita trama anira canviant per cada xarxa que passem (WAN, optica, etc.) pero la informació de dins, el paquet, no canvia. 

La xarxa d'ethernet es una xarxa de contenció, en cas de que colisionin s'ha de tornar a enviar la trama, pero lo que faran es tenir un "cooldown" per tornar envar. El receptor tendra un numero x de milisegons i el que envia tindrà un numero y de milisegons. Aquest tipos de xarxes no son eficients en xarxes molt grans.

̣̣--------------------------------------------------------------------------------------------------------------------------------------


##
