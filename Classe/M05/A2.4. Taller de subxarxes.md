Aquesta activitat és una preparació d'una pràctica de taller.

Se'ns demana, partint de la xarxa mare 172.16.0.0/24, crear 4 subxarxes, distribuïdes tal i com indica el següent diagrama:

És a dir, a cada taula hi haurà una subxarxa de servidors i una altra de PC.

Genereu un document amb les respostes a les següents preguntes:

### a) Calculeu amb els mètodes vistos a clase i per a cada subxarxa, quina seria la seva adreça de xarxa, inclosa la màscara de xarxa?

2³ - 2 = +4

32 - 3 = /29

|IP DE XARXA|RANG DE IP|IP DE BROADCAST|MÀSCARA|
|-----------|----------|---------------|-------|
|172.16.0.0|172.16.0.1-172.16.0.30|172.16.0.31|255.255.255.224|
|172.16.0.32|172.16.0.33-172.16.0.62|172.16.0.63|255.255.255.224|
|172.16.0.64|172.16.0.65-172.16.0.94|172.16.0.95|255.255.255.224|
|172.16.0.96|172.16.0.97-172.16.0.126|172.16.0.127|255.255.255.224|
|172.16.0.128|172.16.0.129-172.16.0.158|172.16.0.159|255.255.255.224|
|172.16.0.160|172.16.0.161-172.16.0.190|172.16.0.191|255.255.255.224|
|172.16.0.192|172.16.0.193-172.16.0.222|172.16.0.223|255.255.255.224|
|172.16.0.224|172.16.0.225-172.16.0.254|172.16.0.255|255.255.255.224|

### b) Crea el diagrama amb Packet Tracer i enganxa'l al document. Tingues en compte de configurar i connectar les subxarxes de la següent manera:

- Taula A: Conté dues subxarxes, la 1A 172.16.0.0/26 (fa0/0) i la 2A 172.16.0.64/26 (fa0/1)
- Taula B: Conté dues subxarxes, la 1B 172.16.0.128/26 (fa0/0) i la 2B 172.16.0.192/26 (fa0/1)

### c) Fes les configuracions necessàries a Packet Tracer i comprova que els hosts de cada taula tenen connectivitat entre ells

![pkt](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/Screenshot_from_2023-02-07_08-52-56.png)

### d) Fes una taula resum amb totes les configuracions a realitzar als PC i Servidors, amb les següents columnes:

    Nom del host: Server 0 - XARXA 1 - A
    IP: 172.16.0.5
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.1

    Nom del host: Server 1 - XARXA 1 - A
    IP: 172.16.0.40
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.1

    Nom del host: PC 0 - XARXA 2 - A
    IP: 172.16.0.73
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.65

    Nom del host: PC 1 - XARXA 2 - A
    IP: 172.16.0.74
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.65

    Nom del host: Server 0(1) - XARXA 1 - B
    IP: 172.16.0.153
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.129

    Nom del host: Server 1(1) - XARXA 1 - B
    IP: 172.16.0.140
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.129
    
    Nom del host: PC 0(1) - XARXA 2 - B
    IP: 172.16.0.196
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.193

    Nom del host: PC 1(1) - XARXA 2 - B
    IP: 172.16.0.197
    Màscara en decimal: 255.255.255.192 
    Gateway: 172.16.0.193


Sense aquesta taula resum no se us deixarà accedir al proper taller.

### e) Quines ordres fariem servir a cada PC o servidor per comprovar que tot funciona correctament? Recorda comprovar:

    Que el host té la IP i màscara correctament assignada

        $ip a

    Que el host té el gateway correctament configurat

        $netstat -rn

    Que el host té connectivitat amb un company de xarxa, amb el gateway i amb un dispositiu d'una altra xarxa

        $ping @ip
