1. Un linux que faci servir una partició de grub, una de swap i una per la resta del sistema de fitxers
```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 4KB 5GB
parted -s /dev/vda mkpart primary linux-swap 5GB 9GB
parted -s /dev/vda mkpart extended 9GB 100%
parted -s /dev/vda mkpart logical 9GB 15GB
parted -s /dev/vda mkpart logical 17.5GB 21.5GB
parted -s /dev/vda mkpart logical 16.5GB 17.5GB
```
2. Un linux que faci servir una partició de grub, una de swap, una per la home i una per la resta del sistema de fitxers
```
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 4KB 5GB
parted -s /dev/vda mkpart primary linux-swap 5GB 9GB
set boot on 1
parted -s /dev/vda mkpart primary 9GB 13GB
parted -s /dev/vda mkpart extended 13GB 100%
parted -s /dev/vda mkpart logical 13GB 15GB
parted -s /dev/vda mkpart logical 17.5GB 21.5GB
parted -s /dev/vda mkpart logical 16.5GB 17.5GB

```
3. Un windows amb una partició de dades i un altre pel sistema operatiu
```
seleccionar disco 0

limpiar

crear partición primaria

asignar letra = C

activo

format fs = ntfs label = Windows quick
```
save in .txt

4. Un windows amb una partició de dades, una partició pel sistema operatiu i una partició per fer un backup
```
select disk 0
clean
convert gpt
create partition primary =10000000
format quick fs=ntfs label=Windows RE tools
assign letter=C
create partition primary size=300
format quick fs=ntfs label=Windows RE tools
assign letter=T
```

5. Un arranc dual d'un windows i un linux
