# Instrucciones para abrir el firewall y permitir acceso a un ssh

Definir la política de aceptar todo lo que entra y sale por la red:
```
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
```
Flush y borrado de las reglas:
```
iptables -F
iptables -X
```
Poner password a root:
```
passwd
(contraseña)
```
Cambiar distribución de teclado: 
```
setkmap es
```
# DISCOS DUROS Y SUS CONFIGURACIONES

## SISTEMAS DE FICHEROS
- **Fat16** = Limitación en tamaño máximo de la partición.
- **Fat32** = Quita la limitación de tamaño de la anterior versión, pero solo se puede poner archivos de 4 GB.
- **exFAT** = No hay limitación de nada. Pensado para pendrives sin control de usuarios estrictos.
- **ntfs** = Sistema de ficheros de Windows. Permite controlar mucho el nivel de permisos.
- **ext2 / ext3 / ext4** = Sistema de ficheros de Linux más habitual tanto para ficheros como para S.O.
- **xfs, btrfs** = Sistema más nuevo de Linux.
- **Swap** = Memória de intercambio, cuando te quedas sin RAM se utiliza. Esto se ralentiza el PC, pero no se colgará. Normalmente se usa en caso de emergencia.

## PARTICIONES
Exercice 1
- LINUX + Partición de datos
- WINDOWS + Partición de datos
- Dual -> WIN + LINUX

URL DE DESCARGAS DE ISO:

Debian (ISO)
    
    https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-10.7.0-amd64-netinst.iso

Windows 10 (ISO)
    
    https://www.microsoft.com/es-es/software-download/windows10ISO

Ubuntu (ISO)
    
    https://ubuntu.com/download/desktop/thank-you?version=20.10&architecture=amd64

CentOS (ISO)
    
    http://ftp.csuc.cat/centos/8-stream/isos/x86_64/CentOS-Stream-8-x86_64-20201211-boot.iso

ArchLinux (ISO)
    
    https://mirror.cloroformo.org/archlinux/iso/2021.01.01/archlinux-2021.01.01-x86_64.iso

Repartición de particiónes:
#### LINUX:
    /boot = 500MB
    SO (Linux) = 8GB [/]
    Particion extendida:
        Swap = 2GB
        Datos = el resto [/home]

parted -s /dev/vda mklabel msdos

parted -s /dev/vda mkpart primary 2MB 500MB (boot)

parted -s /dev/vda mkpart primary 500M 8GB (Linux)

parted -s /dev/vda mkpart extended 8GB 100%

parted -s /dev/vda mkpart logical 8GB 10GB (swap)

parted -s /dev/vda mkpart logical 10GB 100% (data) 

#### WINDOWS:
    S0 = 12GB
    Particion extendida:
        ext = 8GB (datos)

parted -s /dev/vda mklabel msdos

parted -s /dev/vda mkpart primary 2MB 12GB (Windows)

parted -s /dev/vda mkpart extended 12GB 100%

parted -s /dev/vda mkpart logical 12GB 100% (data)


#### PARTICION DUAL:
    /boot = 500MB (ext4)
    SO (WIN) = 10 GB (ntfs)
    SO (Linux) = 8GB 
    Partición extendida:
        Swap = 1GB
        Datos = 1GB


parted -s /dev/vda mklabel msdos

parted -s /dev/vda mkpart primary 2MB 500MB (boot)

parted -s /dev/vda mkpart primary 500MB 15GB (Windows)

parted -s /dev/vda mkpart primary 15GB 23GB (Linux)

parted -s /dev/vda mkpart extended 23GB 100% 

parted -s /dev/vda mkpart logical 23GB 25GB (swap)

parted -s /dev/vda mkpart logical 25GB 100% (data)

### CONFIGURACIONES DE LOS DISCOS

mkfs.ext4 /dev/vdax

mkdir /unnombreparaelfichero

mount /dev/vdax /unnombreparaelfichero/

dd if =/dev/random of=/unnombreparaelfichero/ficherogrande bs=1024 count=100000

ls -lah /unnombreparaelfichero

```
lsblk
```

Tiene que salir en /dev/sda1 la particon /boot
Dentro de boot tenemos los ficheros que necesita el menú de arranque para poder ser visualizado y cargar el SO y el fichero de kernel.

Gestor de arranque es un programa muy reducuido pensado para que puedas seleccionar con que dispositivo o partición arrancadas...

Un ordenador puede arrancar:

- Legacy (el modo de arranque de equipos antiguos)
    - Seleccionas en la bios los **dispositivos** de arranque y un orden:
        1. CD
        2. Network
        3. HD
        4. USB


Lo habitual es que una vez que ha instalado el SO arranque desde disco duro


- Si el dispositivo de arranque es un HD se realiza una primera comprobación mirando el MBR (primer sector del disco que son 512 bytes):
    - Hay código en el sector de arranque? si hay código se ejecuta
        - El Windows no pone ningun codigo en los primeros 446 bytes
        - EL Linux, pone el principio del código de grub (stage 1) => el menú de grub es un codigo un poco extenso que 446 bytes, como no le cabe hace un salto al stage 1.5 y al stage 2.0 que es código que se encuentra en los sectores siguientes
            - Por eso es importante dejar algo de espacio libre antes de la primera partición (1024kb) 
    - Hay alguna partición con flag (bit) en boot activado


- UEFI : Pequeño programa que se integra en la BIOS de las placas base y que permite desde la propia BIOS seleccionar los dispositivos de arranque
    - La informacion sobre la configuracion de estos dispositivos de arranque puede estar compartida dentro de la BIOS y en una particion especifica pa UEFI.
    - Linux, WIN y otros SO se han de integrar en ese UEFI

## WINDOWS CON UEFI
Para que el windows arranque por uefi tenemos que hacer lo siguiente. Hay que instalar 2 discos, el de instalación de WINDOWS y el de los drivers de disco. Iniciamos la maquina, pulsamos un botón cualquiera y arrancara por el instaldor de WIN. 

Una vez allí en ¿Dónde quieres instalar Windows?, tenemos que cargar el contenido de los discos. Tenemos que buscar *win10/amd64*. Y nos saldrá el disco en questión e instalamos el WINDOWS.

Una vez que se haya acabado la instalación, vamos a configurar nuestro Windows. Y para que quede la instalación lo más limpia posible, tendremos que deshacernos de cosas que pre-instala Windows. Estos serán los pasos que voy a seguir para hacerlo:

- Escojer el Idioma: Español (España)
- Escojer el display (distribución) del teclado: Español
    - NO AGREGARÉ UNA SEGUNDA OPCIÓN DE DISPLAY DE teclado
- A la hora de crear una cuenta, le daremos en **CUENTA SIN CONEXIÓN**
- Haremos que nuestra cuenta sea **CUENTA LIMITADA**
- Y crearemos un usuarios
    - User: isard 
    - Password: pirineus
    - En la pregunta de seguridad ya es lo que cada uno quiera hacer
- Servicios
    - Voz: **NO**
    - Ubicación: **NO**
    - Encontrar el dispositivo: **NO**
    - Diagnostico a microsoft: **Enviar Datos de diagnostico requeridos**
    - Entradas manuscritas: **NO**
    - Obtener experiencias personalizadas: **NO**
    - Usar id. de publicidad: **NO**
    - Omitir app de WIN
    - Paquete Office: **NO**
    - Cortana **NO**
Ahora nos saldrá el mensaje de bienvenida, solo queda esperar.

Una vez que hayamos entrado al escritorio vamos a instalar los drivers. Abriremos de nuestro CD de drivers el **virtio-win-gt-x64**. Y sigueinte siguiente con todo. E instalamos el paquete **win-guest-tools**. Lo que hacen estos dos paquetes de drivers es que se mucho más eficiente a la hora de virtualizar la maquina. No olvidemos que todo estre procedimiento se esta haciendo desde una mquina virtual. Un ordenador convencional lo que se tiene que hacer es buscar sus respectivos drivers e instalarlos o uno a uno o por paquetes de todo un poco.

Una vez acabada toda la instalación vamos a ir al buscador de Windows e escribir **windows update**, nos ha de salir una opción llamada **buscar actualizaciones**. Nos saldrá que tiene descargas pendientes, dejamos que se descarguen y hacemos un reboot del ordenador para que acabe de descargarlas. Este proceso tardará lo suyo, paciencia.

Vamos a configurar todo de nuevo:

- **IDIOMA**
    - Configuración ->  Hora e idioma -> Idioma
    - Instalamos todos los paquetes
    - Ponemos por defecto el idioma
        - Configuración de idioma administrativo
            - Cambiar los parametros regionales del sistema... = El idioma que quieras cambiar.
            - Copia la Configuración...
                - Pantalla de bienvenida i cuentas del sistema
                - Cuentas de nuevos usuarios
            - Reiniciar ahora
- **ADAPTAR ENTORNO GRAFICO A ENTORNO DE VIRTUALIZACIÓN**
    - Instalación de Firefox
        - https://www.mozilla.org/es-ES/firefox/new/
        - Poner como navegador predeterminado (es una opción que nos va a saltar al instalar el navegador)
    - Desinstalar Microsoft Edge
        - En el shell o (cmd) tenemos que poner el siguiente comando:
        ```
        cd %PROGRAMFILES(X86)%\Microsoft\Edge\Application\8*\Installer
        setup --uninstall --force-uninstall --system-level
        ```
        Y le damos a permitir
    - Instalar Autologon
        - https://docs.microsoft.com/en-us/sysinternals/downloads/autologon (495KB)
        - Cojer el instalador **Autologon64**
            - User (Vuestro usuario)
            - Domain (El que os salga por defecto)
            - Password (La que vosotros useis)
        - Reiniciamos
    - Crear un nuevo administrador
        - Abrimos Power shell en modo administrador y ponemos los sigueintes comandos:
        Vamos a crear una contraseña para el admin y creamos el usuario admin con sus privilegios.
        ```
        PS C:\Windows\system32> # Guardar el password en una variable
        >> $Password = Read-Host -AsSecureString
        ******** (la contraseña que queramos)
        PS C:\Windows\system32> # Crear usuario admin del grupo administradores
        >> New-LocalUser "admin" -Password $Password -FullName "admin"
        >> Add-LocalGroupMember -Group "Administradores" -Member "admin"

        Name  Enabled Description
        ----  ------- -----------
        admin True
        ```
        Ahora creamos un usuario llamado *user* sin privilegios de admin.
        ```
        PS C:\Windows\system32> New-LocalUser "user" -Password $Password -FullName "user"
        >> Add-LocalGroupMember -Group "Usuarios" -Member "user"

        Name Enabled Description
        ---- ------- -----------
        user True
        ```
    - Directorio c:\admin
    - Desinstalar otras aplicaciones de Windows
        - Abriremos el Power Shell y borraremos todo esto:
        ```
        Microsoft.Xbox*
        Microsoft.Bing*
        Microsoft.3DBuilder
        Microsoft.Advertising.Xaml
        Microsoft.AsyncTextService
        Microsoft.BingWeather
        Microsoft.BioEnrollment
        Microsoft.DesktopAppInstaller
        Microsoft.GetHelp
        Microsoft.Getstarted
        Microsoft.Microsoft3DViewer
        Microsoft.MicrosoftEdge
        Microsoft.MicrosoftEdgeDevToolsClient
        Microsoft.MicrosoftOfficeHub
        Microsoft.MicrosoftSolitaireCollection
        Microsoft.MicrosoftStickyNotes
        Microsoft.MixedReality.Portal
        Microsoft.Office.OneNote
        Microsoft.People
        Microsoft.ScreenSketch
        Microsoft.Services.Store.Engagement
        Microsoft.Services.Store.Engagement
        Microsoft.SkypeApp
        Microsoft.StorePurchaseApp
        Microsoft.Wallet
        Microsoft.WindowsAlarms
        Microsoft.WindowsCamera
        Microsoft.WindowsFeedbackHub
        Microsoft.WindowsMaps
        Microsoft.WindowsSoundRecorder
        Microsoft.WindowsStore
        Microsoft.XboxGameCallableUI
        Microsoft.YourPhone
        Microsoft.ZuneMusic
        Microsoft.ZuneVideo
        SpotifyAB.SpotifyMusic
        Windows.CBSPreview
        microsoft.windowscommunicationsapps
        windows.immersivecontrolpanel
        ```
        Y delante de todo estas aplicaciones pondremos **Get-AppxPackage -Name** nombre de lo que queramos eliminar **-ErrorAction SilentlyContinue**. Quedaría algo asíí, si quereis haceis copiar/pegar.
        ```
        Get-AppxPackage -Name Microsoft.Xbox* | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Bing* | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.3DBuilder | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Advertising.Xaml | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.AsyncTextService | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.BingWeather | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.BioEnrollment | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.DesktopAppInstaller | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.GetHelp | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Getstarted | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Microsoft3DViewer | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MicrosoftEdge | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MicrosoftEdgeDevToolsClient | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MicrosoftOfficeHub | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MicrosoftStickyNotes | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.MixedReality.Portal | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Office.OneNote | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.People | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.ScreenSketch | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Services.Store.Engagement | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.SkypeApp | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.StorePurchaseApp | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.Wallet | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsAlarms | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsCamera | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsFeedbackHub | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsMaps | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsSoundRecorder | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.WindowsStore | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.XboxGameCallableUI | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.YourPhone | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.ZuneMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Microsoft.ZuneVideo | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name SpotifyAB.SpotifyMusic | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name Windows.CBSPreview | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name microsoft.windowscommunicationsapps | Remove-AppxPackage -ErrorAction SilentlyContinue
        Get-AppxPackage -Name windows.immersivecontrolpanel | Remove-AppxPackage -ErrorAction SilentlyContinue

        ```
    - Barra de programa simple
        - En la barra de inicio que sale dandole al botón del teclado de Windows, pondemos quitar todo lo que hay alli dandole click derecho y *desanclar de inicio*. 
    - Desactivar Servicios 
        - Escribir en el buscador **msconfig**
            - Desactivar los siguientes Servicios
                - Administrador de autenticación Xbox Live
                - Centro de seguridad
                - Firewall de Windows Defender
                - Mozilla Maintenance Service
                - Servicio de antivirus de Microsoft Defender
                - Servicio de Windows Update Medic
                - Windows Update
            - Reiniciar
        - Ir a administrador de tareas y deshabilitar:
            - Microsoft OneDrive
            - Windows Security notification
            - Reiniciar
        - Volver a repetir todo el proceso en los demas usuarios
        - Instalar APP's de software libre  
            - Libre Office
            - Geany
            - Gimp
            - Inkscape
            - LibreCAD
        - Instalar APP's de software no libre  
            - Google Chome
            - Adobe Acrobat Reader
            - Al ser app que no son libres del todo, les tendremos que quitar servicios para mayor limpiza y seguridad.
                - Adobe Acrobat Update Service
                - Servicio de Google Update (gupdate)
                - Servicio de Google Update (gupdatem)
                - Google Chrome Elevator Service
# Particiones duales con editores de particiones
### GParted
El GParted es un editor de particiones grafico, es uno de los más simples de editar, pero ahora para ir probandolo tenemos que hacer las siguientes particiones:

160GB tabla -> gpt
- 10MB a 2GB ext4
- 2GB a 20GB brtfs
- 20GB a 40GB xfs 
- 5 particiones de 2GB cada una de ntfs

Para que el GParted te cambie la tabla de particiones has de hacer (Dispositivo > Crear una nueva tabla de particiones > GPT)

Ha de quedar algo similar a esto:

![GParted](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/GParted_2-2-21.png)


### Hirens
El Hirens es un editor de particiones grafico con la base de Windows, és de los menos intuituvos que he tenido, pero igualmente lo tenemos que probar haciendo lo siguiente:

160GB tabla -> mbr
- 10MB a 2GB ext4 (boot)
- 2GB a 30GB ntfs (win10)
- 30GB a 60GB ext4  (debian)
- 60GB a 100% (extendida)
- 60GB a 64GB swap (swap)
- 64GB a 130GB nrfs (dades)
- 130GB a 100% ext4 (backup)

Primero tenemos que cambiar la tabla de particiones a MBR (Disk > Convert GPT to MBR) y luego borramos todas las particiones que hay y las hacemos de nuevo. Hay un error que no deja hacer 3 logicas, entonces juntaremos la particion de data con la particion de backup, que en si no inluye en nada.

Ha de quedar algo similar a esto:

![AOMEI](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/2-2-21_Hirens.png)

# Boot dual con Windows y Linux
| donde empieza | sistema de archivos | punto de montaje |     size   |
| ------------- | ------------------- | ---------------- | ---------- |
|  /dev/vda1    |        Fat32        |  /boot/efi       |  100.00 MiB|  
|  /dev/vda2    |      No format      |                  |  16.00 MiB |
|  /dev/vda3    |        ntfs         |                  |  52.50 GiB |
|  /dev/vda5    |        ext4         |  /boot           |  2.00 GiB  |
|  /dev/vda6    |        ext4         |  /               |  25.80 GiB |
|     new       |        ntfs         |  /home           |  21.70 GiB |
|     new       |      No format      |                  |  17.30 GiB |
|  /dev/vda4    |        ntfs         |                  |  512.00 MiB|
|    libre      |     desconocida     |                  |  2.00 MiB  |

sudo parted -s /dev/vda mklabel gpt
sudo parted -s /dev/vda mkpart primary 2MB 102MB 
sudo parted -s /dev/vda mkpart primary 102MB 118MB
sudo parted -s /dev/vda mkpart primary 118MB 53GB 	
sudo parted -s /dev/vda mkpart primary 53GB 55GB 		
sudo parted -s /dev/vda mkpart primary 55GB 80.80GB 	
sudo parted -s /dev/vda mkpart primary 80.80GB 102.50GB 	
sudo parted -s /dev/vda mkpart primary 102.50GB 119.80GB 
sudo parted -s /dev/vda mkpart primary 119.80GB 121GB		
sudo parted -s /dev/vda mkpart primary 121GB 100% 

sudo mkfs.fat /dev/vda1 
sudo mkfs.ntfs /dev/vda3
sudo mkfs.ext4 /dev/vda5
sudo mkfs.ext4 /dev/vda6
sudo mkfs.ntfs /dev/vda7
sudo mkfs.ntfs /dev/vda8

Estas son las particiones en script.

Una vez entrado en Lubuntu para instalar el SO quedrían algo así una vez con el windows instalado.
![PARTICIONES LUBUNTU](https://gitlab.com/mbruguera/class/-/blob/master/Pictures/07-02-21_Lubuntu.png)
![PARTICIONES LUBUNTU FINAL](https://gitlab.com/mbruguera/class/-/blob/master/Pictures/07-02-21_Fase_final.png)
