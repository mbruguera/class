# Boot dual con Windows y Linux
| donde empieza | sistema de archivos | punto de montaje |     size   |
| ------------- | ------------------- | ---------------- | ---------- |
|  /dev/vda1    |        Fat32        |  /boot/efi       |  100.00 MiB|  
|  /dev/vda2    |      No format      |                  |  16.00 MiB |
|  /dev/vda3    |        ntfs         |                  |  52.50 GiB |
|  /dev/vda5    |        ext4         |  /boot           |  2.00 GiB  |
|  /dev/vda6    |        ext4         |  /               |  25.80 GiB |
|     new       |        ntfs         |  /home           |  21.70 GiB |
|     new       |      No format      |                  |  17.30 GiB |
|  /dev/vda4    |        ntfs         |                  |  512.00 MiB|
|    libre      |     desconocida     |                  |  2.00 MiB  |

sudo parted -s /dev/vda mklabel gpt
sudo parted -s /dev/vda mkpart primary 2MB 102MB 
sudo parted -s /dev/vda mkpart primary 102MB 118MB
sudo parted -s /dev/vda mkpart primary 118MB 53GB 	
sudo parted -s /dev/vda mkpart primary 53GB 55GB 		
sudo parted -s /dev/vda mkpart primary 55GB 80.80GB 	
sudo parted -s /dev/vda mkpart primary 80.80GB 102.50GB 	
sudo parted -s /dev/vda mkpart primary 102.50GB 119.80GB 
sudo parted -s /dev/vda mkpart primary 119.80GB 121GB		
sudo parted -s /dev/vda mkpart primary 121GB 100% 

sudo mkfs.fat /dev/vda1 
sudo mkfs.ntfs /dev/vda3
sudo mkfs.ext4 /dev/vda5
sudo mkfs.ext4 /dev/vda6
sudo mkfs.ntfs /dev/vda7
sudo mkfs.ntfs /dev/vda8

Estas son las particiones en script.

Una vez entrado en Lubuntu para instalar el SO quedrían algo así una vez con el windows instalado.
![PARTICIONES LUBUNTU](https://gitlab.com/mbruguera/class/-/blob/master/Pictures/07-02-21_Lubuntu.png)
![PARTICIONES LUBUNTU FINAL](https://gitlab.com/mbruguera/class/-/blob/master/Pictures/07-02-21_Fase_final.png)