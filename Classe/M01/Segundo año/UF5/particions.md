# Un linux que faci servir una partició de grub, una de swap i una per la resta del sistema de fitxers
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 2MB 500MB (boot)
parted -s /dev/vda mkpart primary 500M 8GB (Linux)
parted -s /dev/vda mkpart extended 8GB 100%
parted -s /dev/vda mkpart logical 8GB 10GB (swap)
parted -s /dev/vda mkpart logical 10GB 100% (data)

# Un windows amb una partició de dades i un altre pel sistema operatiu
parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 2MB 12GB (Windows)
parted -s /dev/vda mkpart extended 12GB 100%
parted -s /dev/vda mkpart logical 12GB 100% (data)

# Un windows amb una partició de dades, una partició pel sistema operatiu i una partició per fer un backup

parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 2MB 12GB (Windows)
parted -s /dev/vda mkpart extended 12GB 100%
parted -s /dev/vda mkpart logical 12GB 18GB (data)
parted -s /dev/vda mkpart logical 18GB 100% (backup)

# Un arranc dual d'un windows i un linux

parted -s /dev/vda mklabel msdos
parted -s /dev/vda mkpart primary 2MB 500MB (boot)
parted -s /dev/vda mkpart primary 500MB 15GB (Windows)
parted -s /dev/vda mkpart primary 15GB 23GB (Linux)
parted -s /dev/vda mkpart extended 23GB 100%
parted -s /dev/vda mkpart logical 23GB 25GB (swap)
parted -s /dev/vda mkpart logical 25GB 100% (data)