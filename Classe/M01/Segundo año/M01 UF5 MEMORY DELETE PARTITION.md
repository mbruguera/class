# MEMORY
Entramos en una maquina virtual, con un SO cualquiera (en mi caso el Ubuntu) y descargamos unas cuantas imagenes (guardadas en descargas). 

Cerramos la maquina y arrancamos con el RSCD.

![IMAGEN DE DESCARGAS](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/Screenshot.png)

#### RSCD
Una vez dentro primero vamos a cargarnos la particion donde estan todos los datos. 

Lo haremos con el parted. Entraremos al parted para ver donde esta la partición. En mi caso es la 5.
```
parted -s /dev/vda rm 5
```
Una vez con la partición muerta vamos a recuperar los datos de esta. Como?
Pues primero haremos una carpeta en /mnt en el RSCD y otra en nuestra maquina fisica para hacernos una carpeta compartida entre las dos.
```
(maquina virtual) mkdir /mnt/rescat
(maquina fisica) mkdir /opt/UBURESCAT
#NOTA: El nombre no influye en nada
(maquina fisica) sudo chown ism49298715 /opt/UBURESCAT
#NOTA: Vamos a dar permisos al usuario para poder hacer todo el proceso sin problemas
(maquina virtual) sshfs ism49298715@10.200.246.210:/opt/UBURESCAT /mnt/rescat
#NOTA: Esto lo que hace es que las carpetas se enlacen. Pero antes tenemos que poner la contraseña de SUDO para poder acceder a dicha maquina.
```
![sshd](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/Screenshot_2.png)

Abrimos el photorec
```
> /dev/vda
> No Partition
> ext?
> La ruta donde queiras que se guarde, en mi caso en la carpeta compartida, se ha puesto automaticamente asi que C
> Y esperamos hasta que se complete
```
Y por ultimo, para buscar entre las 1000 carpetas que hay se ha de hacer:
```
cd /opt/UBURESCAT
find /opt/UBUNTURESCAT/ | grep jpg$
```
Y aqui te saldran todos las fotos que has descargado.
```
[ism49298715@a09 ~]$ find /opt/UBUNTURESCAT/ | grep jpg$
/opt/UBUNTURESCAT/recup_dir.54/f34870192.jpg
/opt/UBUNTURESCAT/recup_dir.54/f34870088.jpg
/opt/UBUNTURESCAT/recup_dir.31/f18117912.jpg
/opt/UBUNTURESCAT/recup_dir.31/t18117912.jpg
/opt/UBUNTURESCAT/recup_dir.31/f18128384.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638488.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47637864.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638424.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638384.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638312.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638400.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47638536.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47638144.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638440.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47637960.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638344.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47637960.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638624.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47638624.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638360.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638040.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638232.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638368.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47637784.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638352.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47637664.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638144.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638472.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638432.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47637784.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638416.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638536.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47637664.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47637864.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638336.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638448.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638328.jpg
/opt/UBUNTURESCAT/recup_dir.122/t47638040.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638392.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638528.jpg
/opt/UBUNTURESCAT/recup_dir.122/f47638408.jpg
/opt/UBUNTURESCAT/recup_dir.32/f18133320.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14673664.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14673664.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14503424.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14560000.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14561024.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14560000.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14561024.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14563072.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14503424.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14563072.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14566144.jpg
/opt/UBUNTURESCAT/recup_dir.28/f14678272.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14566144.jpg
/opt/UBUNTURESCAT/recup_dir.28/t14678272.jpg
/opt/UBUNTURESCAT/recup_dir.23/t14208360.jpg
/opt/UBUNTURESCAT/recup_dir.23/f14214656.jpg
/opt/UBUNTURESCAT/recup_dir.23/t14214656.jpg
/opt/UBUNTURESCAT/recup_dir.23/f14290944.jpg
/opt/UBUNTURESCAT/recup_dir.23/f14208360.jpg
/opt/UBUNTURESCAT/recup_dir.23/t14220776.jpg
/opt/UBUNTURESCAT/recup_dir.23/t14290944.jpg
/opt/UBUNTURESCAT/recup_dir.23/f14220776.jpg
/opt/UBUNTURESCAT/recup_dir.61/f35094248.jpg
[ism49298715@a09 ~]$ 
```


