# Teoría
## Fonements de l'electricitat
Senyals electrics:
- Voltatge (Volts = V)
    - Diferencial de potencial = Tensió = Volts
        - Ús domèstic: 220V en CA (AC)
        - Placa base: 12, 5 i 3 V en CC (DC)
- Intensitat (Ampers = A)
    - Càrrega elèctrica que travessa una secció d'un conductor en la unitat del temps
- Resistencia (Ohms = [omega])
    - Es la dificultat que ofereix un circuit elèctric al pas d'un corrent.
- Potència (Watts = W)
    - Quantitat d'energia lliurada o absorbida per un element en un temps determinart.

#### Corrent alterna - Corrent continua
- **Corrent Alterna (CA o AC)**: És un tipus de corrent elèctric que es caractritza per canviar al llarg del temps, ja sigui en intensitat o en sentit, a intervals regulars.
- **Corrent Continua (CC o DC)**: És un tipus de corrent elèctrica on el sentit de la circulaci´ó de flux de càrregues elèctriques no varia. El fluxe de corrent va en un sol sentit.
#### Contrucció de la matèria i estructura atòmica
Dintre de la matèria hi han 3 capes:
- **Partícules**: Part més petita que resulta es descompon un cos.
- **Molécules**: Part més petita que resulta de la descomposició d'un cos per procediments físics.
- **Àtom**: Part més petita que resulta de la descomposició d'un cos per procediments químics.
    - Nucli: Format per dos tipos d'elements:
        - Protó: Part de l'àtom amb carrega positiva. 
        - Neutró: Part de l'àtom amb carrega neutre, o sense carrega.
    - Electrons: Circulen pel voltant del nucli per òrbites amb carrega negativa.
    
### Conceptes bàsics
El corrent elèctric es un desplaçament continu de càrregues al llarg del cable. Hi tenen diferents efectes:
- **Tèrmic o calorífic**
- **Magnètic**
- **Químic**

Dintre de l'electricitat, n'hi han uns generadors, hi han 3 tipus:
- **Electromagnètics**: giren un conductor en un camp magnètic.
- **Solar**: amb l'exposició solar són capaços de generar energia.
- **Químics**: es basen en reaccions químiques.

#### Llei d'Ohm
Estableix que el corrent que travessa un circuit elèctric és directament proporcional a la diferència de potencial que hi ha entre els seus extrems i inversament proporcional a la resistencia del circuit.
- **V = R * I**
- **R = V/I**
- **I = V/R**

#### Potència elèctrica
Piràmide on es troben col·locats les magnituds elèctriques de la potència i serveix per deduir les fórmules que ens permetran calcular problemes.
- **P = V * I**
- **V = P/I**
- **I = P/V**
    
## Aparells de mesura
Es basen en la mesura del corrent elèctric a partir del camp magnètic que genera el pas d'aquest corrent:
- **Exactitud**: Aproximació de la lectura mesurada.
- **Precisió**: Aproximació de donar resultats amb exactitud.
- **Apreciació**: Valor de fracció minima de la unitat de mesura que es pot llegir.
- **Sensibilitat**: Resposta del intrument al canvi en la variable mesurada.

### Polímetre
Serveix per comprovar si hi ha continuitat entre dos punts (dos punts de circuit). Podem mirar la resistencia, la intensitat o el voltatje, lo normal es veure aquest últim. 

Ara ja no tant, pero abans hi havien dos tipos clarament confrontats, pero ara ja no tant, ja que s'utilitza més els digitals:
- **Analògics**
- **Digitals** 

## Fonts d'alimentació
Lo que fa una font d'alimentació és convertir de corrent alterna a corrent continua. Funciona de la seguent manera:
- **Tranformador**: Permet aumentar o disminuir el voltatge i la intensitat d'un corrent altern manteninguent una freq de 50Hz.
- **Rectificador**: Permet eliminar el señal altern usant diodes rectificador o ponts de diodes.
- **Filtre**: Permet treure el arrissat del senyal. Normalment s'utilitza un condensador paral·lel a la carrega que es carrega a cada pols positius.
- **Regulador**: Manté el voltatge estable, similar al que ofereix una bateria.

Tipos de fonts d'alimentació:
- AT (OBSOLET)
- ATX
- SFX
- TFX
    - Modular
    - Semi-Modular
    - No modular

Connectors dels dispositius:
- 24pin ATX
- 4pin molex
- 4pin fdd
- 4pin p4_12V
- 15pin sata2
- 6pin pci express
- 6pin aux
- 4pin video pw
- 6pin dell_p6

Potència:
Totes les fonts d'alimentació tenen una potència nominal de treball, per damunt de la qual no poden funcionar. Aquestes xifres, en la pràctica, és dificil que la font arribi a donar la potència que indica en l'etiqueta.

FPC actiu / passiu:
    - PFC actiu: sistema de correcció que modifica la fase, reestructura i inclós amplifica el seyal electric perquè a la sortida de les línies hi hagi un fluxe constant i uniforme.
    - PFC passiu: és el mode economic del PFC. Utilitza elements passius com bobines per treure el soroll del reactiu.
- Una FA sense PFC té un factor de potència del voltant d'un 60%
## SAI
El SAI lo que fa es que en cas de que no hi hagi energia, manté amb corrent el ordinador.

Quin tipos de SAI hi han:
- SAI Fora de línia:
- SAI en línia:

## Seguretat en el treball
La seguretat en el treball es fer tot alló que no provoqui accidents, i no fer mal bé tant d'un propi, com del material o com del medi ambient. Una manera de prevenir els riscos és:
- Conèixer els riscos a que estem exposats
- Pensar que l'accident por succeir en qualsevol moment
- Ser conscient que a tu també et pot passar
- Coneixer les consequencies del accident
- Tenir coneixement de les pèrdues tant econòmiques com humanes que ocasionen els accidents

Factor de riscos en la manipulació d'ordinadors:
- Instal·lacions d'energia elèctrica
- Material amb risc d'incendi
- Manipulació d'Eines
- Ambient de treball
- Postures forçades
- Manipulació de carregues
- Carrega mental

Dins dels riscos hi han diferents factors:
- Organització
- Confort Psíquic
- Eines de treball
- Entorn ambiental

Amb quines precaucions?
- Lloc sec i ventilat
- Amb llum per poder veure bé
- Una bona elecció seria una superficie sense revestiment
- Que no hi hagi estatica:
    - Ens podem fer mal nosaltres i el material
    - Es imporant utilitzar una corretga d'antiestatica
- Talls
- Desmuntatge components
- Aixafament
- Toxicitat
- Bacteries
- Curtcircuits
