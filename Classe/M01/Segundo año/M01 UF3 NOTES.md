# Muntatge d'un equip informàtic
## Disseny d'un equip informàtic
Hi han dues maneres d'adquirir un PC:
- Comprar-lo fet
- Montar-lo a peces (**Més economic**)

**Sistema informàtic**: conjunt de parts que funcionen entre si per un objectiu concret. Les seves parts son les següents: maquinari, programari i les persones que l'utilitzen.

Un ordinador es una maquina programable que és capaç de respondre un conjunt d'instruccions d'una manera predefinida. Els ordinadors actuals son capços de fer moltisimes tasques. Hi han molt ordinadors destinats a determinades coses (ús personal, servidors, *gaming*, producció...)

### Parts d'un ordenador
- CPU
- Memòria principal (RAM)
- Memòria secundaria (Disc Dur)
- Perifèrics E/S

### Cicle de vida d'un PC
El cicle de vida d'un equip es el temps que viu. La linea temporal es des de el disseny del ordinador fins a que es tira/recicle.

L'utilització de l'anàlisi estructurada té com objectiu rendir aquestes dificultats mitjaçant la subdivisió del sistema.
- Es centra en l'especificació d'allò que requereix que faci el sistema, despres es podra elaborar un disseny disc que especifiqui els components per al seu es previst.

### Requeriment d'un ordinador
Es considera un requeriment una necessitat documentada sobre el contingut, forma o funcionalitat d'un producte o servei.

Molt poques vegades podrem fer un equip a mida:
- Haurem de cercar un equip amb prestacions superiors
- Equip amb prestacions inferiors i ajuntar components

1. S'ha de fer un anàlisis dels requeriments del ordinador
2. Definir que ha de fer el sistema per satisfer els requeriments de l'usuari
3. MODEL ESSENCIAL:
    - Model ambiental: Que forma part del sistema o que no
        - Fronteres: determinen fins on arriben el sistema
        - Ambient: grup de sistemes, persones o organitzacions amb els quals un sistema interactua
        - Interfícies: mostra l'intercanvi de dades entre el sistema i l'ambiental
        - Esdeveniments: determina els esdeveniments que ocorren en l'ambient als quals el sistema ha de reaccionar.
    - Model de comportament: és el conjunt de models que reflecteixen que ha de fer el sistema i com ho ha de fer.

Passos a seguir:
- **Obtenció de requeriments**
- **Anàlisi**
- **Verificació**
*L'analista s'encarregarà de definir els requeriments del sistema*

L'ordinador ha de ser:
- **Actual**: que no estigui obsolet
- **Cohesió**: l'oridnador s'ha de centrar en una cosa.
- **Complert**: Que no li falti informació
- **Consistent**: No ha de contradir cap altre requeriment i ha de ser coherent amb la documentació.
- **Correcte/necessari**: ha de complir amb la necessitat demandada pels interesats en el sistema/programari.
- **Factible/viable**: el requeriment s/ha de poder implementar


# Cerca d'informació de la wiki
# Valors clau
## Honestedat (Lo hace Alex)

## Integritat
Una persona que demostra integritat es aquella que sempre fa lo correcte, fa tot alló que es considera bo per si mateixa sense afectar els interesos de altres persones. En rigor de veritat, no és el fet de canviar d'opinió en si mateix, sinó la consideració que el canvi d'opinió es va deure, per un intent de treure avantatge.

Hi han molts exemples del que es pot extrapolar de les paraules a la vida real:
- En una parella de casats, portes dequedes junts i mai s'enganyen. 
- Un alumne que aprova tot sense fer trampes
- Una persona que, te un fisic més elevat que la reste, mai l'utilitza per tenir la raó.

![IMAGE](https://concepto.de/wp-content/uploads/2019/10/integridad-e1571077766883.jpg)

# Components d'una placa base
## CPU:

### Que és la CPU?
És la unitat de processament encarregada d'interpretar les instucciones d'un maquinari fent ús de diferents operacions aritmeticas i matematicas.

### Per a que serveix?
Per a executar una seqüència d'instruccions i processa les dades, és el que organitza tot perquè les tasques es facin on toquen i de manera que han de realitzar-se per a obtenir els resultats desitjats.

### Parts de CPU
- Nucleo
- Unitat de control
- Unitat aritmètica lògica
- Unitat de coma flotant
- Memòria cache
- Registres
- Controlador de memòria
- Bus
- Targeta grafica

### Cracteristicas principals d'un processador
- Freqüència de rellotge
- Consum energetico
- Numero de Nucleos
- Sòcol
- Numero de fils
- Memòria cache

### Tipus de CPU: INTEL & AMD
Avantatges i desavantatges d'un processador Intel
- (+) Gran potència en mononúcleo
- (+) Major eficiència energetica
- (-) Preus una mica elevats

Avantatges i desavantatges dels processadors AMD
- (+) Major número de núcelos
- (+) Excel·lent relació qualitat/preu
- (-) Poca potència en mononúcleo

### Millor processador per a ofimatica
Intel: Intel Core i3-8100 3.6GHz BOX
AMD: AMD Ryzen 3 1200 3.4Ghz
!(INTEL)[https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.ldlc.com%2Fes-es%2Fficha%2FPB00236231.html&psig=AOvVaw20jzluO4J6J9iMGvyJyrLK&ust=1618483481016000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCIDp9frG_e8CFQAAAAAdAAAAABAD]
!(AMD)[https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.pccomponentes.com%2Famd-ryzen-3-2200g-35ghz&psig=AOvVaw2TvFyvpF3cV1ji7OIwUiw_&ust=1618483498198000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCLDN4YLH_e8CFQAAAAAdAAAAABAD]

### Millor processador per a gaming
Intel: Intel Core i5-9600K 3.7Ghz
AMD: AMD Ryzen 7 2700X 3.7 Ghz

### Millor processador per a multitasca
Intel: Intel Core i9-9900K 3.6Ghz
AMD: AMD Ryzen 9 3900X 3.8 GHz BOX

## BIOS 
Tot el món coneix el bios com la pantalla blava de l'arrencada del ordinador, però res més fora de la realitat.

#### Que és el bios?
BIOS són la segles en anglés "BASIC INPUT OUTPUT SYSTEM", que la traducció sería "SISTEMA BÀSIC D'ENTRADA I SORTIDA". És un *firmware* instal·lat a la memòria ROM (molt diferent de la memòria RAM que és volàtil, al contrari que la ROM que no ho és) del ordinador, freqüentment és un xip incorporat a la placa base del nostre ordinador. El bios és l'element fonamental en l'arrancada del PC, ja que és el pont entre el hardware (placa base, processador, targeta gràfica, etc.) i el software (S.O.).

!(CHIP BIOS)[https://upload.wikimedia.org/wikipedia/commons/9/9a/Elitegroup_761GX-M754_-_AMIBIOS_%28American_Megatrends%29_in_a_Winbond_W39V040APZ-5491.jpg]

#### Per a que serveix el bios?
El bios, com anteriorment hem explicat, és un pont entre el hardware i el software, però s'encarrega de les funcions a molt baix nivell, com la seqüència d'arrencada. També serveix per identificar i configurar components de hardware com els discs durs, processador, RAM, etc. I una de les coses que és pot modificar des de el bios és els paràmetres del funcionament del processador (activar/desactivar nuclis, activar/desactivar *HyperThreading*, fer *Overclock*)

Dintre del bios podem modificar una gran quantitat d'opcions del hardware. Per norma general, hauràs de fer el canvi, guardar i reiniciar. I en tornar a obrir el PC allà, la pròpia BIOS, sabrà com està el hardware i amb quines modificacions, ja que és l'encarregada de dir a tot com s'ha de comportar.

Aquestes són les principals funcions que es poden modificar:
- Canviar l'ordre de la seqüència d'arrencada.
- Carregar la configuració de fàbrica.
- Actualitzar el bios.
- Crear / canviar / desactivar la contrasenya d'accés.
- Canviar la data i l'hora de l'equip.
- Per canviar els paràmetres de les unitats d'emmagatzematge.
- Per canviar els paràmetres de les unitats òptiques / disc.
- Veure la quantitat de memòria instal·lada al sistema.
- Configura si volem que a l'arrencar estigui actiu o no el pad numèric del teclat.
- Activar o desactivar el logo de fabricant de la placa base en l'arrencada.
- Activar o desactivar el POST (Power On Self Test).
- Activar o desactivar la memòria cau interna del processador.
- Canviar les opcions i el comportament del processador.
- Canviar les opcions i la velocitat de la memòria RAM.
- Canviar els voltatges.
- Crear sistemes RAID de dispositius d'emmagatzematge.
- Activar o desactivar IEE1394.
- Activar o desactivar la targeta de so integrada a la placa.
- Activar o desactivar els ports RS232 / LPT.
- Activar o desactivar ACPI.
- Canviar el comportament del botó d'encesa del PC.
- Canviar les opcions d'arrencada.
- Activar o desactivar diversos monitors en l'arrencada.
- Canviar el comportament dels ventiladors PWM.
- Monitorar les temperatures del PC.

### Com s'accedeix al BIOS?
Normalment, a l'arrencar l'ordinador si li donem ràpidament SUPR, encara que en altres BIOS canvia la tecla, tot segons del fabricant. Aquí adjunto una taula que "per norma general" són les possibles tecles que tenen a l'iniciar l'ordinador.
| Fabricant | Tecla habitual d'accés a BIOS | Tecles addicionals |
| --------- | ----------------------------- | ----------------- |
| ACER | F2 | SUPR, F1 |
| ASROCK | F2 | SUPR |
| ASUS | F2 | SUPR, Insert, F12, F10 |
| Dell | F2 | SUPR, Insert, F12, F10 |
| GiGABYTE | F2 | SUPR |
| HP | ESC | ESC, F2, F10, F12 |
| Lenovo | F2 | F1 |
| MSI | SUPR | F2 |
| TOSHIBA | F2 | F12, F1, ESC |
| ZOTAC | DEL | F2, SUPR |

Pero normalment en arrencar l'ordinador et surt a baix quina tecla és:

!(GiGABYTE BIOS)[https://acf.geeknetic.es/imgw/imagenes/noticias/2019/15939-como-entrar-bios/25.jpg?f=webp]

En aquest cas és la tecla DEL, encara que la F12 et faria el mateix treball.

### Principals fabricants
Encara que la finalitat sigui la mateixa, són alguns fabricants de BIOS que ens podem trobar:
- Phoenix Technologies
- American Megatrends
- IBM
- Dell
- Gateway
- BYOSOFT
- Insyde Software

### Història del bios
L'acrònim BIOS va ser inventat per **Gary Kildall** i va aparèixer per primera vegada en 1975 en el sistema operatiu CP/M 3 4 7 8, descrivint la part específica de la màquina de l'CP/M carregat durant l'arrencada que interactua directament amb el hardware. Aquest fitxer es coneix com el bios DOS o Sistema I/O DOS, i conté la part de maquinari específic de baix nivell de sistema operatiu. Juntament amb el maquinari específic, però independent del bios de sistema subjacent al sistema operatiu que resideix en la memòria ROM, aquest representa l'anàleg a l'CP / M BIOS. Alguns equips basats en PowerPC i Sun utilitzen Open Firmware per a aquest propòsit.

Amb la introducció de les màquines PS / 2, IBM va dividir el sistema BIOS en porcions en manera real i manera protegida.
!(BIOS)[https://www.construyasuvideorockola.com/imagenes/Bios/Bios01.jpg]

### Pila del BIOS
La **pila del BIOS** és un acumulador d'energia que s'encarrega de conservar els paràmetres quan l'ordinador està apagat, ja que si no estigues aquesta s'haurien d'introduir els paràmetres (data, hora, característiques del disc dur...) cada vegada que iniciem l'ordinador.

Es tracta d'un acumulador d'energia, però com qualsevol bateria recarregable es va desgastant amb el pas del temps, i s'hauria de canviar al voltant de 2 i 6 anys després de la compra de l'ordinador. Això es pot veure si l'hora de l'ordinador és endarrereix més del normal.

GLOSSARI:
- Firmware: *El firmware o soporte lógico inalterable es un programa informático que establece la lógica de más bajo nivel que controla los circuitos electrónicos de un dispositivo de cualquier tipo.* wikipedia
- HyperThreading: *Innovación de hardware que permite que en cada núcleo se ejecute más de un hilo. Más hilos significa que se puede hacer más trabajo en paralelo.*
- Overclock: *El overclocking es un método para aumentar el rendimiento de los componentes estándar de una computadora a sus velocidades potenciales más allá de las especificaciones del fabricante.*

## Connexions d'energia
### Molex
Molex és un connector que té ja molts d'anys, i pot subministrar un **5V** (vermell) o **12V** (groc) als perifèrics. S'ha de dir que el seu funcionament està caient en el desús, i avui en dia a penes s'utilitza per alimentar els ventiladors de la caixa.

Gràcies al disseny que tenen, és molt difícil connectar-ho malament, no obstant son bastant problemàtic de treure.

!(MOLEX)[https://www.wikiversus.com/informatica/fuentes-alimentacion/guia-tipos-cables-conectores/img/conector-molex_hu883366f8ae7409a6236cf0bbd19f5c65_115855_650x0_resize_q85_lanczos.jpg]

### Mini-molex / Floppy
És un tipus de connector que, actualment, està completament obsolet, ja que s'utilitzava per alimentar a les antigues disqueteres. Les **disqueteres** són aquells discs magnètics quadrats que podien tenir fins a 1.44MB de dades.

!(Floppy)[https://www.wikiversus.com/informatica/fuentes-alimentacion/guia-tipos-cables-conectores/img/conector-mini-molex-floppy_hu045b0ff3b59bdeaab51cd253f5d46b17_194315_760x0_resize_q85_lanczos.jpg]

### Adaptadors habituals
### De Molex a SATA
En cas de tenir una font d'alimentació antiga, un cable de Molex a SATA és una bona ajuda en cas de tenir connectors de MOLEX que et sobren. Són barats i al transmetre poca energia, solen ser molt segurs.

!(adap1)[https://www.wikiversus.com/informatica/fuentes-alimentacion/guia-tipos-cables-conectores/img/adaptador-molex-sata_hue191a060c86b3124da172a27536b1008_58460_650x0_resize_q85_lanczos.jpg]

### De Molex a PCI-E [6 PINS]
En cas que necessitis un altre cable PCIe de 6 pins per alimentar la targeta gràfica. Aixo si, assegurat de connectar els MOLEX a diferents cables, ja que així no sobrecarregues tant la font d'alimentació, en cas de no fer-ho els 75W es poden transmetre a través d'un sol cable, cosa que donaria problemes.

!(adap2)[https://www.wikiversus.com/informatica/fuentes-alimentacion/guia-tipos-cables-conectores/img/molex-pcie-6-pines_hu09aa07380333eabe282b18d05d27944b_31301_498x0_resize_q85_lanczos.jpg]

### Adaptador ATX
Amb la introducció de **ATX12 V2.0** es va canviar a un connector de 24 pins. L'antic ATX12V (1.0, 1.2 i 1.3) utilitzava un connector de 20 pinnes.

La compatibilitat enrere és més fàcil, la majoria de les fonts d'alimentació moderna permeten desconnectar els últims 4 pins del connector principal.

Això no obstant, també existeixen adaptadors, com el de la imatge, amb compatibilitat cap endavant. D'aquesta manera, podem utilitzar fonts antigues en places base modernes.

!(adap3)[https://www.wikiversus.com/informatica/fuentes-alimentacion/guia-tipos-cables-conectores/img/adaptador-atx-24-a-20-pines_huf0280a309c4436423ed340b83e0e6b72_160858_760x0_resize_q85_lanczos.jpg]

## Consells
**Que passa si un cable d'alimentació no arriba?**
Hauries de comprar una extensió o un adaptador de qualitat.

**Compte amb confondre els connectors EPS de 8 pins amb el del PCIe**

Aquests dos es semblés bastant en la imatge, no obstant això, són diferents i si ens equivoquem posan't-los farem mal bé l'ordinador.

La millor forma de no equivocar-nos és mirar amb detall el connector, aquest te un adhesiu, una etiqueta o tenir el nom del tipus de connector imprès en el mateix connector.

**Com connectar els cables**
L'ordre pot variar molt depén la persona que ho m'unti, però això és un possible exemple de com fer-ho:
- Començar pel cable de 24 pins
- A continuació, connectar el cable de 8 pins, per alimentar la CPU. Algunes plaques porten un connector de 4 pins.
- Ara, connecta el cable d'alimentació PCIe de 6 o 8 pins a la targeta gràfica, en cas que la targeta ho necessiti.
- Connecta els ports USB de la caixa, els ports de so, els lectors de targetes, etc. a la placa base.
- Connecta els POWER SW, RESET SW i l'indicador LED de la placa base.

**Per què és important ordenar els cables?**
Si tens una caixa gran, és important dedicar-li uns minuts a col·locar els cables en ordre. A part que ens facilitarà canviar els components, li ampliarà la vida útil a l'ordinador, augmentarem el fluxe del aire i evitarem que els cables solts puguin fer mal a altres components.

# Documentació de muntatge/desmuntatge d'un ordenador
- Agafeu el 'vostre' ordinador i prepareu un lloc de treball.
- Comproveu que l'ordinador funciona i li ensenyeu al professor.
- Atureu l'ordinador i desmunteu tots els components, inclosa la placa base.
- Un cop desmuntats, feu un inventari detallat de tots els components, recollint les característiques més importants de cada component, cerqueu a Internet tota la informació possible. Podeu fer-ne fotografies per tal de documentar-ho millor.
El processador es un 'Intel Pentium e2180', o sigui que el socket es de Intel. La placa base es una Intel 82801gb, amb una capacitat de ram de 8GB. Té un disipador bastant gran per aire.. Le seves connexions son:
- 6 pci
- 4 slots de ram
- 2 ps2
- 1 vga
- 1 dvi
- 6 usb 1.0
- 1 connector rj45
- 4 sata
Tot ha sigut ampliable amb 1 tarjeta de xarxa (100/LNK/ACT o 10/LNK/ACT), una tarjeta gràfica (HP Serial Port Adapter 2004 HPDC COMM PORT *15 pin) i un adaptador de xarxa.
Te un disc dur data de 250GB amb un lector de discos, els dos connectats per sata.
Te dos ventiladors amb alimentació fan.
La FA es de 240V per alimentar a tot el ordenador, es de tipos ATX.
- Aquest document l'haureu de passar a net i entregar.
- També haureu d'explicar les dificultats que heu tingut a l'hora de desmuntar i muntar l'ordinador i l'ajuda que hagueu pogut necessitar.
- Torneu a muntar l'ordinador i abans de tancar ensenyeu-li al professor.
- Tanqueu l'ordinador i comproveu que encara funciona. El professor ho ha de veure funcionar.
