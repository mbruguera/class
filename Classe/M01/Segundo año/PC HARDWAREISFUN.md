##CPU
Model
```
hism1@if2:~$ lscpu | grep "Model name"
Model name:            Intel(R) Xeon(R) CPU D-1528 @ 1.90GHz
```

Cores
```
hism1@if2:~$ lscpu | grep Core
Core(s) per socket:    6
```

Threads
```
hism1@if2:~$ lscpu | grep "Thread"
Thread(s) per core:    2
```

Caché
```
hism1@if2:~$ lscpu | grep cache
L1d cache:             32K
L1i cache:             32K
L2 cache:              256K
L3 cache:              9216K
```

Quantitat de memoria + slot de MEMORIA
```
ism1@if2:~$ sudo dmidecode -t memory
# dmidecode 3.1
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0019, DMI type 16, 23 bytes
Physical Memory Array
	Location: System Board Or Motherboard
	Use: System Memory
	Error Correction Type: Multi-bit ECC
	Maximum Capacity: 128 GB
	Error Information Handle: Not Provided
	Number Of Devices: 4

Handle 0x001B, DMI type 17, 40 bytes
Memory Device
	Array Handle: 0x0019
	Error Information Handle: Not Provided
	Total Width: 72 bits
	Data Width: 64 bits
	Size: 32 GB
	Form Factor: DIMM
	Set: None
	Locator: DIMMA1
	Bank Locator: P0_Node0_Channel0_Dimm0
	Type: DDR4
	Type Detail: Synchronous
	Speed: 2400 MT/s
	Manufacturer: Samsung
	Serial Number: 34E64EFD
	Asset Tag: (Date:17/07)
	Part Number: M393A4K40BB1-CRC
	Rank: 2
	Configured Clock Speed: 2133 MT/s
	Minimum Voltage: Unknown
	Maximum Voltage: Unknown
	Configured Voltage: 0.003 V

Handle 0x001D, DMI type 17, 40 bytes
Memory Device
	Array Handle: 0x0019
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: DIMMA2
	Bank Locator: P0_Node0_Channel0_Dimm1
	Type: DDR4
	Type Detail: Synchronous
	Speed: Unknown
	Manufacturer: NO DIMM
	Serial Number: NO DIMM
	Asset Tag: NO DIMM
	Part Number: NO DIMM
	Rank: 1
	Configured Clock Speed: Unknown
	Minimum Voltage: Unknown
	Maximum Voltage: Unknown
	Configured Voltage: 0.003 V

Handle 0x001E, DMI type 17, 40 bytes
Memory Device
	Array Handle: 0x0019
	Error Information Handle: Not Provided
	Total Width: 72 bits
	Data Width: 64 bits
	Size: 32 GB
	Form Factor: DIMM
	Set: None
	Locator: DIMMB1
	Bank Locator: P0_Node0_Channel1_Dimm0
	Type: DDR4
	Type Detail: Synchronous
	Speed: 2400 MT/s
	Manufacturer: Samsung
	Serial Number: 34E64754
	Asset Tag: (Date:17/07)
	Part Number: M393A4K40BB1-CRC
	Rank: 2
	Configured Clock Speed: 2133 MT/s
	Minimum Voltage: Unknown
	Maximum Voltage: Unknown
	Configured Voltage: 0.003 V

Handle 0x0020, DMI type 17, 40 bytes
Memory Device
	Array Handle: 0x0019
	Error Information Handle: Not Provided
	Total Width: Unknown
	Data Width: Unknown
	Size: No Module Installed
	Form Factor: DIMM
	Set: None
	Locator: DIMMB2
	Bank Locator: P0_Node0_Channel1_Dimm1
	Type: DDR4
	Type Detail: Synchronous
	Speed: Unknown
	Manufacturer: NO DIMM
	Serial Number: NO DIMM
	Asset Tag: NO DIMM
	Part Number: NO DIMM
	Rank: 1
	Configured Clock Speed: Unknown
	Minimum Voltage: Unknown
	Maximum Voltage: Unknown
	Configured Voltage: 0.003 V

```

Slots EN GENERAL
```
hism1@if2:~$ sudo dmidecode -t baseboard
# dmidecode 3.1
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
	Manufacturer: Supermicro
	Product Name: X10SDV-6C+-TLN4F
	Version: 2.00
	Serial Number: ZM16AS048569
	Asset Tag: To be filled by O.E.M.
	Features:
		Board is a hosting board
		Board is replaceable
	Location In Chassis: To be filled by O.E.M.
	Chassis Handle: 0x0003
	Type: Motherboard
	Contained Object Handles: 0
```

Model placa base
```
hism1@if2:~$ sudo dmidecode -t baseboard
# dmidecode 3.1
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.

Handle 0x0002, DMI type 2, 15 bytes
Base Board Information
	Manufacturer: Supermicro
	Product Name: X10SDV-6C+-TLN4F
	Version: 2.00
	Serial Number: ZM16AS048569
	Asset Tag: To be filled by O.E.M.
	Features:
		Board is a hosting board
		Board is replaceable
	Location In Chassis: To be filled by O.E.M.
	Chassis Handle: 0x0003
	Type: Motherboard
	Contained Object Handles: 0

```
versión BIOS
```
hism1@if2:~$ sudo dmidecode | grep BIOS
Getting SMBIOS data from sysfs.
SMBIOS 2.8 present.
BIOS Information
		BIOS is upgradeable
		BIOS shadowing is allowed
		BIOS ROM is socketed
		BIOS boot specification is supported
	BIOS Revision: 5.6
BIOS Language Information
```

##DISPOSITIVOS
Tarjetas de red
```
ism1@if2:~$ lspci | grep -i ethernet
03:00.0 Ethernet controller: Intel Corporation Ethernet Connection X552/X557-AT 10GBASE-T
03:00.1 Ethernet controller: Intel Corporation Ethernet Connection X552/X557-AT 10GBASE-T
06:00.0 Ethernet controller: Intel Corporation I350 Gigabit Network Connection (rev 01)
06:00.1 Ethernet controller: Intel Corporation I350 Gigabit Network Connection (rev 01)
```

tarjeta de audio
```
No hay audio
```

tarjeta de vídeo
```
hism1@if2:~$ lspci | grep -i vga
09:00.0 VGA compatible controller: ASPEED Technology, Inc. ASPEED Graphics Family (rev 30)
```
tarjeta controladora de discos
```
hism1@if2:~$ lspci -vm |grep -i sata
Class:	SATA controller
Device:	8 Series/C220 Series Chipset Family 6-port SATA Controller 1 [AHCI mode]
hism1@if2:~$ lspci -vm |grep -i nvme
Device:	NVMe Controller

```

##ALMACENAMIENTO
Disc durs + capacitat de disc durs
```
hism1@if2:~$ lsblk
NAME                       MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
sda                          8:0    0  55.9G  0 disk
├─sda1                       8:1    0   200M  0 part /boot/efi
├─sda2                       8:2    0   500M  0 part /boot
├─sda3                       8:3    0     1G  0 part [SWAP]
└─sda4                       8:4    0  54.2G  0 part /
nvme0n1                    259:0    0   477G  0 disk
├─drbdpool-isard_00000     253:0    0 470.1G  0 lvm
└─drbdpool-linstordb_00000 253:1    0   252M  0 lvm
```
tipo y modelo discos duros
```
hism1@if2:~$ sudo lshw -class disk
[sudo] password for hism1:
  *-disk
       description: ATA Disk
       product: INTEL SSDSC2CT06
       physical id: 0.0.0
       bus info: scsi@5:0.0.0
       logical name: /dev/sda
       version: 300i
       serial: BTMP313000BL060AGN
       size: 55GiB (60GB)
       capabilities: gpt-1.00 partitioned partitioned:gpt
       configuration: ansiversion=5 guid=f5132575-89ec-4f52-9922-c16ba2d57679 logicalsectorsize=512 sectorsize=512
```
