# UF4: Noves tendències de muntatge d'un equip informàtic
L'estètica ha canviat molt durant els anys, ja que de ser una habitació fins ara que ens hi cap a la butxaca. El factor de forma el determina la caixa:
- **Torre o gran torre**: Torres de gran dimensions, van molt bé per fer ampliacions o manipular l'ordinador, pero pesa molt, no es facil de transportar.
- **Mitja torre o semitorrre**: millora a la gran torre, pesa menys i es manipula més facil en menos espai.
- **Minitore - Microtorre**: no tenen molts bons recursos, serveixen per una pantalla en un transport pçublic i tal.
- **Barebone**: Es un ordenador obsolet.
- **Taula**: Es un ordenador per ficar un monitor a sobre, es pot veure tant en oficines, com en metges. Es igual que el barebone, pero aquest es manipulable.
- **Servidor**: En el servidor no cal tenir una caixa bonica, pero solen ser amples. El seu disseny esta fet per una major vnetilació. Solen tenir F.A d'extracció calent.
- **HTPC (home theater PC)**: Es un tipus de PC que esta més orientat a la multimedia.
- **All in one**: Esta tot integrat en el monitor.
- **Portàtils**: Aqui esta tot integrat (placa, monitor...) i a més els periferics externs d'un equip.

### Aventatges d'un portàtil
- Portabilidad
- Immediatesa
- Connectivitat
- Mida
- Baix consum d'energia
- Silenci
- Autonomia
- Tot inclòs

### Maquinari d'un portàtil
- Placa base
- CPU
- RAM
- Tarjetes d'expansió
- pantalla
- Dispositius òptics
- Emmagatzematge intern
- Dispositius d'entrada de dades
- Ports
- Estació d'acoblament (docking station)

