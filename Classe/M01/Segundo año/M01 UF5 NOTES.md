# Servidores en red
Con *ip a* -> veo la ip
    - eth**x**
    - enp**x**s**y**
    - Conectada "LOWER UP"

Con *ip r* -> mira mi gateway
    - default via

Con netstat -> Para ver los puertos abiertos
```
netstat -tunlp
    - t: TCP
    - u: UDP
    - n: numero de puerto
    - l: listen
    - p: programa
```
puertos
FTP 22
SSH 22
HTTP 80
HTTPS 443

### SSH
Para activar o desactivas el SSHD.
```
systemctl start sshd 
systemctl stop sshd
systemctl status sshd
```
### Servidor HTTP
Creas en una carpeta (mkdir) con una imagen importada a tal carpeta. Para activar el servidor se hace un **python3 -m http.server**.

Ataques a PC.

nmap -p 22 10.200.246.0/24
    nmap lo que hace es saber si esta abierto el puerto o no.
    nmap -p 22 10.200.246.0/24 | grep -B 4 open (Mira que personas estan con el puerto abierto)

Cargarse el firewall:
iptables -P INPUT ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F
iptables -X
setfont -

Una vez entrado en el pc hacemos:
- `# lsblk` Para ver las particiones del ordenador.

### Recuperar archivos de un PC en caso de averia con SRCD
Primero miramos cual es la particion de NTFS. Lo vamos a ver con `wipefs /dev/vdax`. En mi caso es /dev/vda3. Y lo montaremos con `mount /dev/vda3 /mnt` para ver que tenemos en el windows.

Todas las cosas se guardan en Users y allí podemos empezar a movernos por el Windows que conocemos por terminal. 

NOTA MENTAL: Para ver lo que ocupa un directorio se ve con `du -h -d 1`

Haremos un Backup para poder llevarlo a un pen o recuperarlo por SSH. Entonces lo que haremos, en este caso voy a recuperar las imagenes que hay en el SO, haciendo un `tar czvf backup.tar.gz /mnt/Users/isard/Pictures/` y lo guardamos en /root.

Para arreglar el sistema de ficheros, hacemos un `ntfsfix /dev/vda3` para que nos deje montar otra vez el /mnt sin errores.

Por ultimo miramos cual es la ip para poder hacer un SSH. Le quitaremos el cortafuegos y entraremos, copiaremos el targz y habremos recuperado las fotos del sistema.

Copiamos con `scp[secure copy] backup-tar.gz ism49298715[user]@10.200.248.209[IP]:/tmp[donde lo quieras guardar]`
```
[root@sysrescue ~]# scp backup.tar.gz ism49298715@10.200.246.209:/tmp/
The authenticity of host '10.200.246.209 (10.200.246.209)' can't be established.
ECDSA key fingerprint is SHA256:fLb2wGYl4/p1D+B+rwY6O/fhCYBA6YGboayWSX3aGBs.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.200.246.209' (ECDSA) to the list of known hosts.
ism49298715@10.200.246.209's password: 
Permission denied, please try again.
ism49298715@10.200.246.209's password: 
backup.tar.gz                                                                         100%  480    95.8KB/s   00:00    
[root@sysrescue ~]# 
```

### Recuperar archivos borrados anteriormente
Hemos perdido 3 archivos anteriormente descargados:
    - Un PDF
    - Un Word
    - Una imagen

Y iniciamos con SRCD para recuperar esos archivos.
**Linea de comandos**
```
lsblk (para ver donde esta la raíz)
wipefs vdx (que me puedo encontrar en el disco duro [info del disco])
mount /dev/vdx (donde este montado el SO) /mnt/win10 (o donde lo quieras montar se puede montar en una carpeta independiente con mkdir name)
En caso de no tener instalado el testdisk:
    sudo apt/dnf... -y install testdisk
```
Photorec
Se intala con el testidisk, es un paquete que viene junto al testdisk.
Abriremos el Photorec
```
Le decimos que particion está -- /dev/vdx
Le decimos si todo el espacio se ha de analizar -- free (la parte libre)
Le decimos donde se han de gusrdar las fotos -- (Lo suyo sería un USB)
``` 
## SSHFS (montar un directorio remoto en tu máquina)
scp user@server:/rutadedondequierasquevayaelcopiado
scp ism49298715@10.200.241.209:/mnt 
rsync

**MONTAR DIRECTORIO POR RED**
- Samba/SMB/CIFS (Windows)
- NFS
- SSHFS (No es eficiente pero es sencillo de montar)

sshfs [user@]host or IP:[dir] mountpoint [options]
sshfs ism49298715@10.200.246.209:/opt/rescat /mnt/rescat

## Recuperar archivos en caso de borrado de partición
En caso de que la partición de disco se rompa o algo, lo que vamos a hacer nosotros es entrar desde el RSCD al disco duro roto. Una vez dentro miraremos donde esta la raíz (/) para recuperar lo perdido.
```
lsblk
```
Vemos que solo hay una particion *vda*. Entonces vamos a ver los datos del disco duro, no lo que hay o había guardado, sino el sistema de ficheros y diferentes ayudas.
```
wipefs /dev/vda
```
Al no haber particiones no podemos montar nada en ningun sitio, así que vamos a abrir directamente el Photorec.
```
Photorec:
    >Disk /dev/vda
    >Unknown
    >[ ext4 ]
    >Directory ..
    >C
```

# Averías (TEORIA)
### Font d'alimentació
Es la parte que le proporciona energia al PC. Este convierte los 220V de corriente alterna en corriente continua. Hay de todos los precios y tamaños, pero las más tipicas son las ATX.

Para las averias influye:
- La calidad de los componentes de la fuente (que la fuente sea economica, és bastante probable que sean de mala calidad)
- Algunas fuentes tienen un *conmutador* para seleccionar si la corriente de entrada es de 230V o 125V. IMPORTANTE QUE ESTE LA "PALANQUITA" EN " 230V.
- HABITUALMENTE cuando una fuente no funciona se cambia por otra.
- Si la establidad de la tensión electrica (que hayan subidas o bajadas de tensión en la instalación electrica de casa/oficina/etc.) es probable que se estropeen.
    - En caso de subidas de tensión *fuertes*, llevan un fusible las fuentes, pero a veces es inaccesible.
    - En caso de cortocircuito en algun elemento conectado a la fuente, ejemplo:
        - Puertos USB que hacen cortocircuito entre don pines o terminales.

Orden de resolución de incidencias con fuentes:
- Revisar cables
- Sacar los connectores de la placa base i mirar si arranca haciendo *cortocircuito* entre cable verde i negro. (NO HACER EN CASA)

![COLORES PINES](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/atx-20.jpg)

El pin 16 con el cable de color verde, indica el PS-on (Power Supply On), cuando apretas el boton del ordenador para encender, lo que hace es cortocircuitar el pin verde con el negro.

- Si la fuente no se enciende o no giran los ventiladores de la fuente, substituirla.
- Si no hay tensión, substituirla.
- Si la fuente funciona, igualmente podemos substituirla por una mejor,
- Si una fuente de alimentación nueva no funciona:
    - Si se conectan muchos dispositivos, no puede estar dando la suficiente corriente.
    - The GeForce RTX 3080 is rated for 350W of total board power, and Nvidia recommends using a 750W power supply with it.
- Para hacer una prueba para adivinar donde tenemos el problema de sobrecarga cortocircuito es conectar el minimo de componentes.
- Si es la fuente de alimentación de un portatil, se ha de mirar con un tester.

### Refigeración del equipo
Problemas:
- Los ventiladores con el tiempo se vuelven más lentos, tienen más fricción i más polvo.
- Con limpieza se piede quitar el polvo de los radiadores y/o ventiladores:
    - AIRE COMPRIMIDO: NUNCA lo hagas a bocagarro, puedes dañar los componentes.
    - PINCEL: Es la mejor opción, ta que no deterioras los componentes.
- Si los ventiladores no giran o van muy lento, substituirlos.
- Si la temperatura del processador sube a más de 90º, por seguridad la BIOS corta la corriente.
- En los portatiles es más complicado hacer subtituciones, pues cada modelo teine su version de refigeracion y si forma de hacer el recambio.
- A veces hay que cambiar la pasta térmica que pone en contacto el procesador con el disipador.
- Es importante no poner en exceso de pasta térmica ya que puede generar averías en un futuro.
    - IMPORTANTE LIMPIAR LOS RESTOS DE PASTA TÉRMICA.

### Disco duro
Hay una herramienta que nos permite monitorizar la "*salud*" del disco duro: SMART.

Un disco duro tiene una vida limitada. Cuanto más se escriba, menos vida útil va a tener.Si el disco es rotacional, una caída del equipo puede hacer que deje de funcionar. 

Cuando tenemos un disco en un estado que "a veces funciona y a veces no", es muy importante hacer una copia (en caso de que haya algo que salvarse de ese disco duro). Si no se puede hacer una copia entera, se pueden hacer copias parciales con la herramienta **dd**. Si necesito recuperar ficheros no accesibles, lo puedo hacer con el **photorec**.
