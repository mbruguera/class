# Teoría
## Fonements de l'electricitat
Senyals electrics:
- Voltatge (V)
- Intensitat (A)
- Resistencia (Ohms)
- Potència (W)

#### Corrent alterna - Corrent continua
- **Corrent Alterna (CA o AC)**: És un tipus de corrent elèctric que es caractritza per canviar al llarg del temps, ja sigui en intensitat o en sentit, a intervals regulars.
- **Corrent Continua (CC o DC)**: És un tipus de corrent elèctrica on el sentit de la circulaci´ó de flux de càrregues elèctriques no varia. El fluxe de corrent va en un sol sentit.
#### Contrucció de la matèria i estructura atòmica
Dintre de la matèria hi han 3 capes:
- **Partícules**: Part més petita que resulta es descompon un cos.
- **Molécules**: Part més petita que resulta de la descomposició d'un cos per procediments físics.
- **Àtom**: Part més petita que resulta de la descomposició d'un cos per procediments químics.
    - Nucli: Format per dos tipos d'elements:
        - Protó: Part de l'àtom amb carrega positiva. 
        - Neutró: Part de l'àtom amb carrega neutre, o sense carrega.
    - Electrons: Circulen pel voltant del nucli per òrbites amb carrega negativa.
    
## Polímetre
Serveix per comprovar si hi ha continuitat entre dos punts (dos punts de circuit). Podem mirar la resistencia, la intensitat o el voltatje, lo normal es veure aquest últim. 

Es basen en la mesura del corrent elèctric a partir del camp magnètic que genera el pas d'aquest corrent:
- **Exactitud**: Aproximació de la lectura mesurada.
- **Precisió**: Aproximació de donar resultats amb exactitud.
- **Apreciació**: Valor de fracció minima de la unitat de mesura que es pot llegir.
- **Sensibilitat**: Resposta del intrument al canvi en la variable mesurada.

Ara ja no tant, pero abans hi havien dos tipos clarament confrontats, pero ara ja no tant, ja que s'utilitza més els digitals:
- **Analògics**
- **Digitals** 

## Fonts d'alimentació
Lo que fa una font d'alimentació és convertir de corrent alterna a corrent continua. Funciona de la seguent manera:
- **Tranformador**: Permet aumentar o disminuir el voltatge i la intensitat d'un corrent altern manteninguent una freq de 50Hz.
- **Rectificador**: Permet eliminar el señal altern usant diodes rectificador o ponts de diodes.
- **Filtre**: Permet treure el arrissat del senyal. Normalment s'utilitza un condensador paral·lel a la carrega que es carrega a cada pols positius.
- **Regulador**: Manté el voltatge estable, similar al que ofereix una bateria.



