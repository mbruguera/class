## Exercici 1: Configurar GIT
```
191019mb@localhost:~$ git version
git version 2.34.1

a191019mb@localhost:~$ git config --global user.name "Marc Bruguera"

a191019mb@localhost:~$ git config --global user.email "marcbruguera54@gmail.com"a191019mb@localhost:~$ git config --list
user.name=Marc Bruguera
user.email=marcbruguera54@gmail.com

a191019mb@localhost:~$ git config --global color.ui auto

a191019mb@localhost:~$ git config --list
user.name=Marc Bruguera
user.email=marcbruguera54@gmail.com
color.ui=auto

a191019mb@localhost:~$ 
```

## Exercici 2: Crear un repositori
```
a191019mb@localhost:~$ mkdir marc_llibre

a191019mb@localhost:~$ cd marc_llibre/

a191019mb@localhost:~/marc_llibre$ git init
hint: Using 'master' as the name for the initial branch. This default branch name
hint: is subject to change. To configure the initial branch name to use in all
hint: of your new repositories, which will suppress this warning, call:
hint: 
hint: 	git config --global init.defaultBranch <name>
hint: 
hint: Names commonly chosen instead of 'master' are 'main', 'trunk' and
hint: 'development'. The just-created branch can be renamed via this command:
hint: 
hint: 	git branch -m <name>
Initialized empty Git repository in /home/users/inf/wism2/a191019mb/marc_llibre/.git/

a191019mb@localhost:~/marc_llibre$ ls -la
total 12
drwxr-xr-x  3 a191019mb wism2 4096 de gen.  10 13:28 .
drwxr-xr-x 17 a191019mb wism2 4096 de gen.  10 13:28 ..
drwxr-xr-x  7 a191019mb wism2 4096 de gen.  10 13:28 .git
a191019mb@localhost:~/marc_llibre$ 
```

## Exercici 3: Comprovar l'estat del repositori
```
a191019mb@localhost:~/marc_llibre$ git status
On branch master

No commits yet

nothing to commit (create/copy files and use "git add" to track)

a191019mb@localhost:~/marc_llibre$ touch marc_index.txt

a191019mb@localhost:~/marc_llibre$ vim marc_index.txt 

a191019mb@localhost:~/marc_llibre$ git status 
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	marc_index.txt

nothing added to commit but untracked files present (use "git add" to track)

a191019mb@localhost:~/marc_llibre$ git add marc_index.txt 
a191019mb@localhost:~/marc_llibre$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   marc_index.txt

a191019mb@localhost:~/marc_llibre$ 
```

## Exercici 4: Realitzar un commit dels últims canvis
```
a191019mb@localhost:~/marc_llibre$ git commit -m "Afegit índex del llibre"
[master (root-commit) 923c1fa] Afegit índex del llibre
 1 file changed, 47 insertions(+)
 create mode 100644 marc_index.txt

a191019mb@localhost:~/marc_llibre$ git status
On branch master
nothing to commit, working tree clean

a191019mb@localhost:~/marc_llibre$ 
```

## Excercici 5: Canviar contingut del fitxer
```
a191019mb@localhost:~/marc_llibre$ git diff
diff --git a/marc_index.txt b/marc_index.txt
index a44000e..d4a241a 100644
--- a/marc_index.txt
+++ b/marc_index.txt
@@ -45,3 +45,53 @@ drwxr-xr-x 17 a191019mb wism2 4096 de gen.  10 13:28 ..
 drwxr-xr-x  7 a191019mb wism2 4096 de gen.  10 13:28 .git
 a191019mb@localhost:~/marc_llibre$ 
 
+## Exercici 3: Comprovar l'estat del repositori
+```
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+nothing to commit (create/copy files and use "git add" to track)
+
+a191019mb@localhost:~/marc_llibre$ touch marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ vim marc_index.txt 
+
+a191019mb@localhost:~/marc_llibre$ git status 
+On branch master
+
+No commits yet
+
:
+++ b/marc_index.txt
@@ -45,3 +45,53 @@ drwxr-xr-x 17 a191019mb wism2 4096 de gen.  10 13:28 ..
 drwxr-xr-x  7 a191019mb wism2 4096 de gen.  10 13:28 .git
 a191019mb@localhost:~/marc_llibre$ 

## Exercici 3: Comprovar l'estat del repositori

+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+nothing to commit (create/copy files and use "git add" to track)
+
+a191019mb@localhost:~/marc_llibre$ touch marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ vim marc_index.txt 
+
+a191019mb@localhost:~/marc_llibre$ git status 
+On branch master
+
+No commits yet
+
+Untracked files:
+  (use "git add <file>..." to include in what will be committed)
+       marc_index.txt
+
+nothing added to commit but untracked files present (use "git add" to track)
+
+a191019mb@localhost:~/marc_llibre$ git add marc_index.txt 
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+Changes to be committed:
+  (use "git rm --cached <file>..." to unstage)
+       new file:   marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ 
+```
+
+## Exercici 4: Realitzar un commit dels últims canvis
+```
+a191019mb@localhost:~/marc_llibre$ git commit -m "Afegit índex del llibre"
+[master (root-commit) 923c1fa] Afegit índex del llibre
+ 1 file changed, 47 insertions(+)
+ create mode 100644 marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+nothing to commit, working tree clean
+
+a191019mb@localhost:~/marc_llibre$ 
+```
(END)
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+nothing added to commit but untracked files present (use "git add" to track)
+
+a191019mb@localhost:~/marc_llibre$ git add marc_index.txt 
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+Changes to be committed:
+  (use "git rm --cached <file>..." to unstage)
+       new file:   marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ 


## Exercici 4: Realitzar un commit dels últims canvis
+a191019mb@localhost:~/marc_llibre$ git commit -m "Afegit índex del llibre"
+[master (root-commit) 923c1fa] Afegit índex del llibre
+ 1 file changed, 47 insertions(+)
+ create mode 100644 marc_index.txt
+
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+nothing to commit, working tree clean
+
+a191019mb@localhost:~/marc_llibre$ 
+```

a191019mb@localhost:~/marc_llibre$ 
a191019mb@localhost:~/marc_llibre$ git add marc_index.txt 
a191019mb@localhost:~/marc_llibre$ git commit -m "afegeixo capitol 3 i capitol 4"
[master 07b8807] afegeixo capitol 3 i capitol 4
 1 file changed, 50 insertions(+)
a191019mb@localhost:~/marc_llibre$ 
```

## Excercici 6: Mostrar els canvis
```
a191019mb@localhost:~/marc_llibre$ git show
commit 07b880752a7186afaaf25841f79a2faf23c086af (HEAD -> master)
Author: Marc Bruguera <marcbruguera54@gmail.com>
Date:   Tue Jan 10 13:36:59 2023 +0100

    afegeixo capitol 3 i capitol 4

diff --git a/marc_index.txt b/marc_index.txt
index a44000e..d4a241a 100644
--- a/marc_index.txt
+++ b/marc_index.txt
@@ -45,3 +45,53 @@ drwxr-xr-x 17 a191019mb wism2 4096 de gen.  10 13:28 ..
 drwxr-xr-x  7 a191019mb wism2 4096 de gen.  10 13:28 .git
 a191019mb@localhost:~/marc_llibre$ 
 
+## Exercici 3: Comprovar l'estat del repositori
+```
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+nothing to commit (create/copy files and use "git add" to track)
+
a191019mb@localhost:~/marc_llibre$ git commit --amend -m "afegeixo capitol 3 fluxe de treball bàsic i capitol 4 gestió de branques"
[master 78805d6] afegeixo capitol 3 fluxe de treball bàsic i capitol 4 gestió de branques
 Date: Tue Jan 10 13:36:59 2023 +0100
 1 file changed, 50 insertions(+)
a191019mb@localhost:~/marc_llibre$ git show
commit 78805d69e5bc042960e654f2d9a6a7a67db3e88d (HEAD -> master)
Author: Marc Bruguera <marcbruguera54@gmail.com>
Date:   Tue Jan 10 13:36:59 2023 +0100

    afegeixo capitol 3 fluxe de treball bàsic i capitol 4 gestió de branques

diff --git a/marc_index.txt b/marc_index.txt
index a44000e..d4a241a 100644
--- a/marc_index.txt
+++ b/marc_index.txt
@@ -45,3 +45,53 @@ drwxr-xr-x 17 a191019mb wism2 4096 de gen.  10 13:28 ..
 drwxr-xr-x  7 a191019mb wism2 4096 de gen.  10 13:28 .git
 a191019mb@localhost:~/marc_llibre$ 
 
+## Exercici 3: Comprovar l'estat del repositori
+```
+a191019mb@localhost:~/marc_llibre$ git status
+On branch master
+
+No commits yet
+
+nothing to commit (create/copy files and use "git add" to track)
+
a191019mb@localhost:~/marc_llibre$ 
```
