P1. Crea una función al_cuadrado(numero) a la que se le pasa un número y
devuelve el cuadrado de un número. Con un for y la función, escribe el
cuadrado de todos los números de 1 a 20.

function al_cuadrado(){
    for ($i = 1; $i <= 20; $i++) {
        $numero_al_cuadrado = pow($i, 2);
        echo "$i elevado a 2 = $numero_al_cuadrado";
        echo " // ";
    }
}

echo al_cuadrado();

P2. Haz una función que escriba en letras los números del 30 al 49. Utiliza
las constantes T que vale "Treinta" y Q que vale "Cuarenta"

function letras(){
    define("T","Treinta");
    define("Q","Cuarenta");
    $arr = array ("uno". "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve");

    for ($i=0; $i<=9; $i++) { 
        echo T + $arr[i];
    }

    for ($i=0; $i<=9; $i++) { 
        echo Q + $arr[i];
    }
}

letras();