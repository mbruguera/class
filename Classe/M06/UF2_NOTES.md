# Backups en red (NAS)
Los NAS son un PC pequeño, con un hardware/softwere especifico a lectura y escritura de datos.

### Rsync
Sincorniza dos carpetas diferentes, por ejemplo entre servidor y cliente. Puedes hacer una sicronización en modo local o en modo remoto.
- Local: este funciona contantemente, osea borra un backup anterior y lo crea de nuevo con esa linia modificada, ya que si se modifica solo, necesita mucho procesamiento.
- Remoto: en vez de hacer un borrado de un backup anterior, lo que hace es añadirle o quitarle lineas a medida de que se modifica. Usa el protocolo SSH. Y tenemos que tener en cuenta diferentes cosas:
    - Ha de estar instalado el rsync en el cliente y en el servidor, para que pueda hacer su función.
    - Para poder hacer las modificaciones, el servidor necesitara un user y una contraseña, como un ssh, para poder funcionar. Lo que se tendría que hacer en el cliente es configurarlo para que se haga automaticamente.
    - Hay diferentes parametros que se pueden configurar, por ejemplo. Hacer que del servidor no elimine lo que se borre en el local, como esas muchas opciones.

Rsync funcona por línea de comandos.
```
rsync -opcion origen destino
ejemplo:
cd /home/fulanito/rsync
rsync archivo1.txt archivo2.txt
```
En caso de querer hacerlo remoto:
```
rsync -e "ssh -p 1234" archivo1 usuario@host.dominio:/tmp/
```

*Ventajas de Rsync*
- Soporta copiado de permisos
- Dispone de opciones de exclusión
- No requiere root
- Puedes utilizar claves públicas

### Rdiff
Es muy parecido a rsync, pero con sus diferencias. No solo tiene copia exacata, sino que puede ir acumulando. Se lanza desde el servidor, no desde el cliente como el rsync, y es en el servidor donde se intala y configurarlo para que pueda coger los archivos en el cliente. Es la que se usa más en la actualidad.

Rdiff funcona por línea de comandos.
```
rdiff -opcion origen destino
```

Restauración de ficheros se puede hacer una restauracion actual o una restauración especifica, gracias a los metadatos que guarda el comando.

La opción _–remove-older-than_ nos permitirá realizar este proceso. Al igual que la opción
de restauración, tendremos que indicarle una marca del tiempo a partir de la cual
borrará los backup incrementales.
```
rdiff-backup –remove-older-than 2W /mnt/backup
```
