#/bin/bash
##############################################################################################
# Nombre: Marc                                                                               #
# Apellidos: Bruguera López                                                                  #
# Curso: 2o año de microinformatica y redes                                                  #
# Clase: WISM2                                                                               #
# Programa: bash                                                                             #
# Función: Lo que hace este script és comprimir /etc/passwd y /etc/shadow en bz2 con un date #
# NOTA: FUNCIÓN EN SUDO                                                                      #
##############################################################################################
tar -cjhpf /tmp/COP_SEG__$(date +%d_%m_%y).bz2 /home/users/inf/wism2/ism49298715/backup.txt

