Una ley son un conjunto de normas, por ejemplo:
    
    - La ley del codigo de circulación y la norma de no poder saltarte un semaforo en rojo.

## Donde estan las leyes?
La leyes oficiales estan recogidas en el Boletin Oficial del Estado (BOE). Se aplican las leyes una vez que esten en dicha página.

Consta de:
    - Nombre
    - Preambulo o disposicione generales
    - Titulo
    - Articulos
        - Descripción de falta/delito
        - Sanciones

## Legislación informatica
