#/bin/bash
##############################################################################################################
# Nombre: Marc                                                                                               #
# Apellidos: Bruguera López                                                                                  # 
# Curso: 2o año de microinformatica y redes                                                                  #
# Clase: WISM2                                                                                               #
# Programa: bash                                                                                             #
# Función: Lo que hace este script és comprimir el archivo backup.txt guardado en la raíz en bz2 con un date #
# NOTA: FUNCIÓN EN SUDO                                                                                      #
############################################################################################################
tar -cjhpGf /tmp/CD_1$(date +%m)-$(date +%d%m)
