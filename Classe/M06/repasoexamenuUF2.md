# RAID
Para hacer un raid por comandos se utiliza el **mdadm**, con los parametros siguientes:
    - Tipo o nivel [--level=]
    - Nº de dispositivos [--raid-device=]
    - Identificador de despositivos [/dev/sdx]
    - Nombre (te da el nombre md127 de forma predeterminada) []
```
mdadm -C --level=raid5 --raid-device=3 /dev/sdb /dev/sdc /dev/sdd 
```
Diferencia entre raid 5 y raid 6: Raid 6 tiene más seguridad ya que tiene un disco duro más de paridad, así hay más margen de error. 

Los discos duros han de estar formateados para poderse utilizar. 

Para poder cambiar el nombre (md127) y cambiarlo al nombre que quieras, se hace de la siguiente manera:
    - mdadm --stop [para parar el raid]
    - mdadm --assembled --update=name --name=0 /dev/sd[abc]
    - update-initmfs -c
    - nano /etc/fstab
        - /dev/md0 /mnt/raid

# Backups
rdiff-backup [origen, en caso remoto usuario@ip /directorio para hacer el backup] [destino /directorio donde se va a guardar]
```
rdiff-backup root@192.168.10.10 /home /mnt/raid
```
La primera vez que se hace un backup hará una copia total, a partir de la segunda se haran diferenciales.

Comando para forzar a hacer una copia total con el rdiff
    - rdiff-backup **-b** = total

Borrar copias antiguas:
    - rdiff-backup --remove-older-than=[tiempo: 2Y(years), 3M(meses)...]
    
