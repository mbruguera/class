# E1. Exercise 4. Operating System structure

## Introduction

We can divide operating system in following parts:
- Kernel
- Memory manager
- Input/Output manager
- Filesystem manager

## Contents

Let's analyze operating system behaviour on each module. We will be using linux comands to see what's going on on a linux OS.

## Delivery

### Kernel

1. **Where does the kernel reside in disk?** Write down the command with the output and tell exactly where it is the kernel.

ls / boot -> initramfs or vmlinuz, there is more than one file since there is more than 1 version.

2. **How can we show the actual kernel version that it is loaded on our system**? Please show command output

ls /boot -> you get the different versions, and there is one that is higher than all (new version), and leave the old ones just in case.

3. **How can we show the detected hardware done by the kernel?** Please show command output.

lshw

f32-isard                   
    description: Computer
    width: 64 bits
    capabilities: smp vsyscall32
  *-core
       description: Motherboard
       physical id: 0
     *-memory
          description: System memory
          physical id: 0
          size: 4GiB
     *-cpu:0
          product: Intel(R) Core(TM) i7-5930K CPU @ 3.50GHz
          vendor: Intel Corp.
          physical id: 1
          bus info: cpu@0
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp x86-64 constant_tsc arch_perfmon rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt arat umip
     *-cpu:1
          product: Intel(R) Core(TM) i7-5930K CPU @ 3.50GHz
          vendor: Intel Corp.
          physical id: 2
          bus info: cpu@1
          width: 64 bits
          capabilities: fpu fpu_exception wp vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ss syscall nx pdpe1gb rdtscp x86-64 constant_tsc arch_perfmon rep_good nopl xtopology cpuid tsc_known_freq pni pclmulqdq ssse3 fma cx16 pcid sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave avx f16c rdrand hypervisor lahf_lm abm cpuid_fault invpcid_single pti fsgsbase tsc_adjust bmi1 avx2 smep bmi2 erms invpcid xsaveopt arat umip
     *-pci
          description: Host bridge
          product: 82G33/G31/P35/P31 Express DRAM Controller
          vendor: Intel Corporation
          physical id: 100
          bus info: pci@0000:00:00.0
          version: 00
          width: 32 bits
          clock: 33MHz
        *-display
             description: VGA compatible controller
             product: QXL paravirtual graphic card
             vendor: Red Hat, Inc.
             physical id: 1
             bus info: pci@0000:00:01.0
             version: 05
             width: 32 bits
             clock: 33MHz
             capabilities: vga_controller rom
             configuration: driver=qxl latency=0
             resources: irq:21 memory:f4000000-f7ffffff memory:f8000000-fbffffff memory:fce14000-fce15fff ioport:c040(size=32) memory:c0000-dffff
        *-pci:0
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2
             bus info: pci@0000:00:02.0
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce16000-fce16fff ioport:1000(size=4096) memory:fcc00000-fcdfffff ioport:fea00000(size=2097152)
           *-network
                description: Ethernet controller
                product: Virtio network device
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:01:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: bus_master cap_list rom
                configuration: driver=virtio-pci latency=0
                resources: irq:22 memory:fcc40000-fcc40fff memory:fea00000-fea03fff memory:fcc00000-fcc3ffff
              *-virtio0
                   description: Ethernet interface
                   physical id: 0
                   bus info: virtio@0
                   logical name: enp1s0
                   serial: 52:54:00:77:28:a1
                   capabilities: ethernet physical
                   configuration: autonegotiation=off broadcast=yes driver=virtio_net driverversion=1.0.0 ip=192.168.122.88 link=yes multicast=yes
        *-pci:1
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.1
             bus info: pci@0000:00:02.1
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce17000-fce17fff ioport:2000(size=4096) memory:fca00000-fcbfffff ioport:fe800000(size=2097152)
           *-usb
                description: USB controller
                product: QEMU XHCI Host Controller
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:02:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: xhci bus_master cap_list
                configuration: driver=xhci_hcd latency=0
                resources: irq:22 memory:fca00000-fca03fff
        *-pci:2
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.2
             bus info: pci@0000:00:02.2
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce18000-fce18fff ioport:3000(size=4096) memory:fc800000-fc9fffff ioport:fe600000(size=2097152)
           *-communication
                description: Communication controller
                product: Virtio console
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:03:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: bus_master cap_list
                configuration: driver=virtio-pci latency=0
                resources: irq:22 memory:fc800000-fc800fff memory:fe600000-fe603fff
              *-virtio1 UNCLAIMED
                   description: Virtual I/O device
                   physical id: 0
                   bus info: virtio@1
                   configuration: driver=virtio_console
        *-pci:3
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.3
             bus info: pci@0000:00:02.3
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce19000-fce19fff ioport:4000(size=4096) memory:fc600000-fc7fffff ioport:fe400000(size=2097152)
           *-scsi
                description: SCSI storage controller
                product: Virtio block device
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:04:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: scsi bus_master cap_list
                configuration: driver=virtio-pci latency=0
                resources: irq:22 memory:fc600000-fc600fff memory:fe400000-fe403fff
              *-virtio2
                   description: Virtual I/O device
                   physical id: 0
                   bus info: virtio@2
                   logical name: /dev/vda
                   configuration: driver=virtio_blk
        *-pci:4
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.4
             bus info: pci@0000:00:02.4
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce1a000-fce1afff ioport:5000(size=4096) memory:fc400000-fc5fffff ioport:fe200000(size=2097152)
           *-generic
                description: Unclassified device
                product: Virtio memory balloon
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:05:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: bus_master cap_list
                configuration: driver=virtio-pci latency=0
                resources: irq:22 memory:fe200000-fe203fff
              *-virtio3 UNCLAIMED
                   description: Virtual I/O device
                   physical id: 0
                   bus info: virtio@3
                   configuration: driver=virtio_balloon
        *-pci:5
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.5
             bus info: pci@0000:00:02.5
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce1b000-fce1bfff ioport:6000(size=4096) memory:fc200000-fc3fffff ioport:fe000000(size=2097152)
           *-generic
                description: Unclassified device
                product: Virtio RNG
                vendor: Red Hat, Inc.
                physical id: 0
                bus info: pci@0000:06:00.0
                version: 01
                width: 64 bits
                clock: 33MHz
                capabilities: bus_master cap_list
                configuration: driver=virtio-pci latency=0
                resources: irq:22 memory:fe000000-fe003fff
              *-virtio4 UNCLAIMED
                   description: Virtual I/O device
                   physical id: 0
                   bus info: virtio@4
                   configuration: driver=virtio_rng
        *-pci:6
             description: PCI bridge
             product: QEMU PCIe Root port
             vendor: Red Hat, Inc.
             physical id: 2.6
             bus info: pci@0000:00:02.6
             version: 00
             width: 32 bits
             clock: 33MHz
             capabilities: pci normal_decode bus_master cap_list
             configuration: driver=pcieport
             resources: irq:22 memory:fce1c000-fce1cfff ioport:7000(size=4096) memory:fc000000-fc1fffff ioport:fde00000(size=2097152)
        *-multimedia
             description: Audio device
             product: 82801I (ICH9 Family) HD Audio Controller
             vendor: Intel Corporation
             physical id: 1b
             bus info: pci@0000:00:1b.0
             version: 03
             width: 32 bits
             clock: 33MHz
             capabilities: bus_master cap_list
             configuration: driver=snd_hda_intel latency=0
             resources: irq:42 memory:fce10000-fce13fff
        *-isa
             description: ISA bridge
             product: 82801IB (ICH9) LPC Interface Controller
             vendor: Intel Corporation
             physical id: 1f
             bus info: pci@0000:00:1f.0
             version: 02
             width: 32 bits
             clock: 33MHz
             capabilities: isa
             configuration: driver=lpc_ich latency=0
             resources: irq:0
        *-sata
             description: SATA controller
             product: 82801IR/IO/IH (ICH9R/DO/DH) 6 port SATA Controller [AHCI mode]
             vendor: Intel Corporation
             physical id: 1f.2
             bus info: pci@0000:00:1f.2
             version: 02
             width: 32 bits
             clock: 33MHz
             capabilities: sata ahci_1.0 bus_master cap_list
             configuration: driver=ahci latency=0
             resources: irq:31 ioport:c060(size=32) memory:fce1d000-fce1dfff
        *-serial
             description: SMBus
             product: 82801I (ICH9 Family) SMBus Controller
             vendor: Intel Corporation
             physical id: 1f.3
             bus info: pci@0000:00:1f.3
             version: 02
             width: 32 bits
             clock: 33MHz
             configuration: driver=i801_smbus latency=0
             resources: irq:16 ioport:700(size=64)
     *-pnp00:00
          product: PnP device PNP0501
          physical id: 3
          capabilities: pnp
          configuration: driver=serial
     *-pnp00:01
          product: PnP device PNP0303
          physical id: 4
          capabilities: pnp
          configuration: driver=i8042 kbd
     *-pnp00:02
          product: PnP device PNP0f13
          physical id: 5
          capabilities: pnp
          configuration: driver=i8042 aux
     *-pnp00:03
          product: PnP device PNP0b00
          physical id: 6
          capabilities: pnp
          configuration: driver=rtc_cmos


4. **How can we show the modules (drivers) actually in use?** Which module it is being used for video graphics card?

lspci -v
lsmod


### Memory manager

1. **What is the 'cache' memory that shows the command 'free -m'?** Show also the command output

 total        used        free      shared  buff/cache   available
Mem:           1880         331         854           1         693        1314

2. **What is the 'swap' memory that shows the command 'free -m'?**

Swap:          4015           0        4015

3. **How does the memory manager handle an out-of-memory problem?**

Kill the process that is consuming the most RAM mem.

4. **How can we show the actual 'out of memory score' of a Gimp instance that it is running?**

htop / filter "/gimp" (memorize lowest process number)
/proc/(process number)
cat oom_score


### Input/Output manager

1. **How can we list all the interrupts that it is aware our Operating System?** Show also command output

/proc/interrupts	   cat interrupts

CPU0       CPU1       
1:        183        681   IO-APIC   1-edge      i8042
8:          0          0   IO-APIC   8-edge      rtc0
9:          0          0   IO-APIC   9-fasteoi   acpi
12:          0        144   IO-APIC  12-edge      i8042
16:          0          0   IO-APIC  16-fasteoi   i801_smbus
21:        252       1154   IO-APIC  21-fasteoi   qxl
22:       2094        123   IO-APIC  22-fasteoi   virtio4, virtio3
24:          0          1   PCI-MSI 32768-edge      PCIe PME, aerdrv, pciehp
25:          1          0   PCI-MSI 34816-edge      PCIe PME, aerdrv, pciehp
26:          0          1   PCI-MSI 36864-edge      PCIe PME, aerdrv, pciehp
27:          1          0   PCI-MSI 38912-edge      PCIe PME, aerdrv, pciehp
28:          0          1   PCI-MSI 40960-edge      PCIe PME, aerdrv, pciehp
29:          1          0   PCI-MSI 43008-edge      PCIe PME, aerdrv, pciehp
30:          0          6   PCI-MSI 45056-edge      PCIe PME, aerdrv, pciehp
31:          0          0   PCI-MSI 512000-edge      ahci[0000:00:1f.2]
32:         25          9   PCI-MSI 1048576-edge      xhci_hcd
33:          0          0   PCI-MSI 1048577-edge      xhci_hcd
34:          0          0   PCI-MSI 1048578-edge      xhci_hcd
35:          0          0   PCI-MSI 1572864-edge      virtio1-config
36:        162        219   PCI-MSI 1572865-edge      virtio1-virtqueues
37:          0          0   PCI-MSI 2097152-edge      virtio2-config
38:          0      15903   PCI-MSI 2097153-edge      virtio2-req.0
39:          0          0   PCI-MSI 524288-edge      virtio0-config
40:       6814        520   PCI-MSI 524289-edge      virtio0-input.0
41:         18        886   PCI-MSI 524290-edge      virtio0-output.0
42:        780          0   PCI-MSI 442368-edge      snd_hda_intel:card0
NMI:          0          0   Non-maskable interrupts
LOC:     148074     124006   Local timer interrupts
SPU:          0          0   Spurious interrupts
PMI:          0          0   Performance monitoring interrupts
IWI:          0          0   IRQ work interrupts
RTR:          0          0   APIC ICR read retries
RES:        608       5288   Rescheduling interrupts
CAL:      29104      17382   Function call interrupts
TLB:        683        631   TLB shootdowns
TRM:          0          0   Thermal event interrupts
THR:          0          0   Threshold APIC interrupts
DFR:          0          0   Deferred Error APIC interrupts
MCE:          0          0   Machine check exceptions
MCP:         38         38   Machine check polls
ERR:          0
MIS:          0
PIN:          0          0   Posted-interrupt notification event
NPI:          0          0   Nested posted-interrupt event
PIW:          0          0   Posted-interrupt wakeup event


2. **In multiprocessor systems, how does the operating system distribute interrupts by default?** How can we change the default behaviour?

with tuned-adm
  ex tuned-adm list
    list all options
      ex -powersave
         -latency-Performance
         -active
          active profiles (powersave)
3. **How does an operating system control a device that has no interrupts?** Explain the process

Well, it will do it by pooling, the pooling system manages the communication between the devices


4. **What will happen if two applications want to send data through network device at the same time?**

The one with the highest priority will come out first

### Filesystem manager

1. **Which is the typical linux folder structure? ** Describe what it is and the use of each folder.
- bin: static directory and it is where all the necessary binaries are stored to guarantee the basic functions at the user level.
- boot: operating system initialization process performed by Linux kernel
- dev: devices
- etc: in charge of storing the configuration files both at the level of operating system components
- home: Is a start folder
- lib: All libraries
- lib64: All libaries x64 bits
- lost+found:
- media: in charge of storing the configuration files both at the level of operating system components
- mnt: An empty directory that serves similar functions to / media, but is rarely used today, since most distributions make use of the latter for temporary mount points.
- opt: will store the packages or programs installed on the system that are from an other applications.
- proc: all process
- root: Is a superuser with all privileges
- run: Executable Linux-based application containing code associated with a specific program developed for the Linux platform.
- sbin: means "super user". In other words, they are binaries that are not run by normal users.
- srv: It is used to store files and directories related to servers that you may have installed within your system, be it a www web server, an FTP server, CVS, etc.
- sys: systems
- tmp: temporal folders, when you shotdown the computer, this folders disappear
- usr: user
- var: contains data that changes when the system runs normally. It is specific to each system and therefore it is not shared over the network with other computers.

2. **How can we:?**
ls /usr/local/src/file.md
- Move a file that resides in /usr/local/src/file.md to the folder /opt: mv file.md /opt
- Copy a file that resides in /usr/local/src/file.md to the folder /opt: cp file.md /opt
- Move a file in /usr/file.md to /usr/local if we are currently in path /usr/local: touch /usr/file.md
                                                                                    cd/usr/local
                                                                                    mv /usr/file.md /usr/local
- Create the file .gitignore using command 'touch' and then try to list it (ls). What happens?
  hidden folder, don't see
