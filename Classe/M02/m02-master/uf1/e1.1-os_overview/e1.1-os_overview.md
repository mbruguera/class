# E1. Exercise 1. Operating Systems Overview

## Introduction

There are mainly two approaches to administer an Operating System:

- Using command line interface
- With the graphical user interface

The quickest one it is usually the CLI, as you could also automate actions through scripting. Linux and bash scripting is the main Operating System and language to reach that goal, but other languages like python are getting popular.

## Contents

Using IsardVDI (or if not available use VirtualBox) create three different virtual machines:

- Centos 7
- Debian 9
- Windows 7

Start each one and reproduce the next administration tasks:

- Show disk usage
- Show hardware resource consumption
- Show storage devices in system
- Create a new text file named 'ohmygod.md' and fill it with your personal information.

## Delivery

Answer following questions regarding each Operating System:

1. **Which command (or commands) can be used to show disk usage on this OS using CLI?**

   - Centos 7 CLII (terminal/terminator):
   - Debian 9 CLI (terminal/terminator):
   - Windows 7 CLI (cmd):

2. **Where can you find disk usage in this OS using GUI?**

   - Centos 7 GUI:
   - Debian 9 GUI:
   - Windows GUI:

3. **Which command (or commands) show hardware resource usage?**

   - Linux:
   - Windows:

4. **How can we show storage devices detected on our system?**

   - Linux:
   - Windows:

5. **Which commands (or applications) can be used to create a new text file?**

   - Linux:
   - Windows:

6. **When will updates will be downloaded and applied on each Operating System by default?**

   - Centos 7:
   - Debian 9:
   - Windows:

7. **How can we trigger updates manually? **

   - Centos 7:
   - Debian 9:
   - Windows:

8. **Why are Operating System updates so important?**

   

9. **Describe the process/command(s) to follow to install GIMP photo editor in each Operating System:**

   - Fedora 27:

   

   - Windows:

   

10. **What are repositories on a linux distribution?**



****