# e1.1 Operating System Overview

### Básico
En este tema se habla de lo basico de FEdora y lo que se puede hacer en el propio CLI. Aqui habran diferentes comandos para máás o menos saber lo basico de tu PC o de moverte por el CLI:

**df -h** enseña el disk usage que hay en el PC. (Fedora y CentOS)
**wmic** y **logicaldisk get/size/freespace/caption** (Windows cmd)

**htop** o **top** son para saber cuantos recursos esta utilizando el propio ordenador(Linux)
**wmic /?** para saber cunatos recursos esta utilizando el propio ordenador (Windows)

**lsblk** para saber cuantos dispositivos de almacenamiento hay. (Linux)
**logicaldisk get/size/freespace/caption** (Windows cmd)

### Actualizar
Para actualizar hay la misma forma pero en los diferentes distribuciones de Linux hay prefijos diferentes. Aqui hay unos ejemplos:

    sudo yum update (CentOS)
    sudo apt-get update (Debian)
    sudo dnf update (Fedora)
Dichas actualizaciones son importatntes para fijar *bugs* que pueden haber en la propia version que tengamos instalada.

### Texto
Para poder visualizar un archivo de texto en terminal (.txt) por ejemplo. Hay muchas herramientas que se pueden utilizar.

    cat
    vi
    vim
    less
    ...

# e1.2 Conversiones de base

| Decimal | Binari | Hexadecimal | Octal |
| ------- | ------ | ----------- | ----- |
| 0       | 0000   | 0           | 0     |
| 1       | 0001   | 1           | 1     |
| 2       | 0010   | 2           | 2     |
| 3       | 0011   | 3           | 3     |
| 4       | 0100   | 4           | 4     |
| 5       | 0101   | 5           | 5     |
| 6       | 0110   | 6           | 6     |
| 7       | 0111   | 7           | 7     |
| 8       | 1000   | 8           | 10    |
| 9       | 1001   | 9           | 11    |
| 10      | 1010   | A           | 12    |
| 11      | 1011   | B           | 13    |
| 12      | 1100   | C           | 14    |
| 13      | 1101   | D           | 15    |
| 14      | 1110   | E           | 16    |
| 15      | 1111   | F           | 17    |

Normalmente, por facilidad, se ha de pasar todo a binario. Y la mayoria con estos pesos

### Decimal
                128  64   32   16   8   4   2   1
**D** to **B** = h +  g +  f +  e + d + c + b + a

### Hexadecimal
En hexadecimal a partir del nueve son letras, has de saber lo siguente:

    A = 10 / 1010
    B = 11 / 1011
    C = 12 / 1100
    D = 13 / 1101
    E = 14 / 1110
    F = 15 / 1111

A partir de eso se puede hacer la tranferencia de hexadecimal a binario o viceversa.

### Octal
Este sin duda, para mi, es el máás dificl de "jugar" con el.
```
DÍGIT OCTAL		BINARI		
	0			0 0 0		
	1			0 0 1		
	2			0 1 0		
	3			0 1 1		
	4			1 0 0		
	5			1 0 1	
	6			1 1 0		
	7			1 1 1
```
Esta es una pequeña ayuda que se puede usar, normalmente te van a dar 9 digitos, cuando un binario "NORMAL" usa 8. En este caso nuestros pesos para hacer sumas, se añade el 256, para el noveno digito.

# e1.3 Transacciones
En este tema ayuda a hacer una pequeña base de datos. 

```

~~~python
#!/usr/bin/python3
# coding=utf-8

########################################################################
## AUTOR: 		Josep Maria Viñolas Auquer			      			  ##
## VERSIO: 		0.2													  ##
## LLICENCIA: 	AGPLv3                                                ##
## DESCRIPCIO:  Es conectarà a la api json de bicing, sumara les bicis##
##              de cada estacio, ho desarà al fitxer databicing.csv i ##
## 				ho mostrarà en una gràfica.							  ##
## REQUISITS:   sudo dnf install python3-tkinter -y                   ##
##              sudo pip3 install requests numpy matplotlib           ##
## EXECUCIO:	python3 4-bikesInUse_entrada.py                       ##
########################################################################

# Aquesta llibreria ens permet fer peticions a una URL.
# En el nostre cas l'usarem per conectar a les dades de la API.
import requests

# Ara a més importarem la llibreria time que té funcions com sleep per
# fer que esperi un cert temps el nostre programa
import time

# Amb aquesta llibreria podrem desar les dades descarregades en un fitxer
# amb un format estandard csv
import csv

# La llibreria numpy ens permetra llegir de nou les dades des del fitxer
# csv columna per columna per tal de poder-les posteriorment graficar.
import numpy

# La llibreria matplotlib permet fer gràfiques i gestionar-ne els paràmetres
# a partir de les dades llegides del fitxer databicing.csv
# En aquest cas nomes importem una classe des del fitxer (que en conté diverses)
from matplotlib import pyplot

# Definirem una variable iteració de tipus entera que tindrà com a valor
# inicial 0 i que s'anirà incrementant en el bucle while. Aquest bucle
# finalitzarà quan iteració sigui més gran que 10.

# Guardarem totes les dades que ens vagi retornant el web en una llista
# anomenada databicing. El sistema operatiu serà l'encarregat de trobar
# un lloc a la memòria RAM de l'ordinador on desar-les
databicing=[]
iteracio=0
while iteracio < 30:
	#######################
	#### ENTRADA DE DADES: Agafem les dades des de la URL de la API en 
	####  format JSON
    #######################
    dataapi=requests.get("https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status").json()
    time.sleep(1)
    iteracio=iteracio+1
    print("Petició Nº:"+str(iteracio))
    
    #######################
    #### CONVERSIO/PROCESSAT DE DADES: Sumarem el total de mecàniques i 
    ####  elèctriques de totes les estacions
    #######################
    totals={"mecaniques":0,
            "electriques":0}
    for station in dataapi["data"]["stations"]:
        totals["mecaniques"] = totals["mecaniques"] + station["num_bikes_available_types"]["mechanical"]
        totals["electriques"] = totals["electriques"] + station["num_bikes_available_types"]["ebike"]
    databicing.append(totals)

#######################
#### EMMAGATZEMATGE: Guardem les dades de databicing en un fitxer csv
#######################
with open('databicing.csv', 'w') as fitxer:
	punter = csv.DictWriter(fitxer, ["mecaniques","electriques"])
	punter.writeheader()
	punter.writerows(databicing)

# Llegim les dades del fitxer
dades = numpy.genfromtxt('databicing.csv',delimiter=',', skip_header=1, usecols=(1))
#######################
#### SORTIDA: Graficarem les dades del fitxer per pantalla amb pyplot
#######################
# Dibuixem les dades en memòria i posem títols a la gràfica
pyplot.plot(dades)
pyplot.title('Nom Cognom1 cognom2')
pyplot.ylabel('Bicis')
pyplot.xlabel('Temps')

# Generem una finestra on mostrar la gràfica que tenim en memòria.
pyplot.show()
```

# e1.4 Estructura de un sistema operativo
### Kernel
En */boot/initramfs- (tab)*sales todas las versiones que hay instaladas en el kernel.
Con *uname -a* te dice que version hay activa y cuando se instaló.
```
[mbruguera@localhost ~]$ ls /boot/initramfs-
initramfs-0-rescue-53a735fc8e264384b015481482fee10a.img  initramfs-5.8.15-201.fc32.x86_64.img
initramfs-5.8.11-200.fc32.x86_64.img                     initramfs-5.8.18-200.fc32.x86_64.img
[mbruguera@localhost ~]$ uname -a
Linux localhost.localdomain 5.8.15-201.fc32.x86_64 #1 SMP Thu Oct 15 15:56:44 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux
```

Para saber que kernel hay instalado en nuestro ordenador hay varios comandos para saber eso, como lspci, lsusb, lscpu o lshw. Pero usaremos *lspci* que hace una busqueda máás extensa.
```
[mbruguera@localhost ~]$ lspci
00:00.0 Host bridge: Intel Corporation 8th Gen Core Processor Host Bridge/DRAM Registers (rev 07)
00:01.0 PCI bridge: Intel Corporation 6th-9th Gen Core Processor PCIe Controller (x16) (rev 07)
00:02.0 Display controller: Intel Corporation UHD Graphics 630 (Desktop)
00:12.0 Signal processing controller: Intel Corporation Cannon Lake PCH Thermal Controller (rev 10)
00:14.0 USB controller: Intel Corporation Cannon Lake PCH USB 3.1 xHCI Host Controller (rev 10)
00:14.2 RAM memory: Intel Corporation Cannon Lake PCH Shared SRAM (rev 10)
00:16.0 Communication controller: Intel Corporation Cannon Lake PCH HECI Controller (rev 10)
00:17.0 SATA controller: Intel Corporation Cannon Lake PCH SATA AHCI Controller (rev 10)
00:1c.0 PCI bridge: Intel Corporation Cannon Lake PCH PCI Express Root Port #5 (rev f0)
00:1d.0 PCI bridge: Intel Corporation Cannon Lake PCH PCI Express Root Port #9 (rev f0)
00:1f.0 ISA bridge: Intel Corporation Device a308 (rev 10)
00:1f.3 Audio device: Intel Corporation Cannon Lake PCH cAVS (rev 10)
00:1f.4 SMBus: Intel Corporation Cannon Lake PCH SMBus Controller (rev 10)
00:1f.5 Serial bus controller [0c80]: Intel Corporation Cannon Lake PCH SPI Controller (rev 10)
01:00.0 VGA compatible controller: NVIDIA Corporation GP107 [GeForce GTX 1050 Ti] (rev a1)
01:00.1 Audio device: NVIDIA Corporation GP107GL High Definition Audio Controller (rev a1)
02:00.0 Ethernet controller: Realtek Semiconductor Co., Ltd. RTL8111/8168/8411 PCI Express Gigabit Ethernet Controller (rev 16)
```
Y *lsmod para saber los *drivers* que hay instaldos en el kernel:
```
[mbruguera@localhost ~]$ lsmod
Module                  Size  Used by
uinput                 20480  0
xt_CHECKSUM            16384  1
xt_MASQUERADE          20480  3
xt_conntrack           16384  1
ipt_REJECT             16384  2
nf_nat_tftp            16384  0
nf_conntrack_tftp      20480  3 nf_nat_tftp
tun                    57344  1
bridge                233472  0
stp                    16384  1 bridge
llc                    16384  2 bridge,stp
nft_objref             16384  2
nf_conntrack_netbios_ns    16384  1
nf_conntrack_broadcast    16384  1 nf_conntrack_netbios_ns
nft_fib_inet           16384  1
nft_fib_ipv4           16384  1 nft_fib_inet
nft_fib_ipv6           16384  1 nft_fib_inet
nft_fib                16384  3 nft_fib_ipv6,nft_fib_ipv4,nft_fib_inet
nft_reject_inet        16384  5
nf_reject_ipv4         16384  2 nft_reject_inet,ipt_REJECT
nf_reject_ipv6         20480  1 nft_reject_inet
nft_reject             16384  1 nft_reject_inet
nft_ct                 20480  24
nft_chain_nat          16384  4
ip6table_nat           16384  1
ip6table_mangle        16384  1
ip6table_raw           16384  0
ip6table_security      16384  0
iptable_nat            16384  1
nf_nat                 49152  5 ip6table_nat,nf_nat_tftp,nft_chain_nat,iptable_nat,xt_MASQUERADE
iptable_mangle         16384  1
iptable_raw            16384  0
iptable_security       16384  0
rfkill                 28672  3
ip_set                 57344  0
nf_tables             237568  337 nft_ct,nft_reject_inet,nft_fib_ipv6,nft_objref,nft_fib_ipv4,nft_chain_nat,nft_reject,nft_fib,nft_fib_inet
nfnetlink              16384  3 nf_tables,ip_set
ip6table_filter        16384  1
ip6_tables             32768  5 ip6table_filter,ip6table_raw,ip6table_nat,ip6table_mangle,ip6table_security
iptable_filter         16384  1
sunrpc                565248  1
vfat                   20480  1
fat                    81920  1 vfat
snd_sof_pci            24576  0
snd_sof_intel_byt      20480  1 snd_sof_pci
snd_sof_intel_ipc      20480  1 snd_sof_intel_byt
snd_sof_intel_hda_common    90112  1 snd_sof_pci
snd_soc_hdac_hda       24576  1 snd_sof_intel_hda_common
snd_sof_xtensa_dsp     16384  2 snd_sof_intel_hda_common,snd_sof_intel_byt
snd_sof_intel_hda      20480  1 snd_sof_intel_hda_common
snd_sof               131072  4 snd_sof_pci,snd_sof_intel_hda_common,snd_sof_intel_byt,snd_sof_intel_ipc
snd_soc_skl           180224  0
snd_soc_sst_ipc        20480  1 snd_soc_skl
snd_soc_sst_dsp        40960  1 snd_soc_skl
snd_hda_ext_core       36864  4 snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_soc_skl,snd_sof_intel_hda
snd_soc_acpi_intel_match    45056  3 snd_sof_pci,snd_sof_intel_hda_common,snd_soc_skl
snd_soc_acpi           16384  4 snd_soc_acpi_intel_match,snd_sof_intel_hda_common,snd_sof_intel_byt,snd_soc_skl
snd_soc_core          315392  4 snd_sof,snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_soc_skl
snd_compress           32768  1 snd_soc_core
ac97_bus               16384  1 snd_soc_core
intel_rapl_msr         20480  0
snd_pcm_dmaengine      16384  1 snd_soc_core
snd_usb_audio         319488  7
intel_rapl_common      32768  1 intel_rapl_msr
snd_usbmidi_lib        45056  1 snd_usb_audio
snd_hda_codec_realtek   143360  1
x86_pkg_temp_thermal    20480  0
intel_powerclamp       20480  0
snd_rawmidi            45056  1 snd_usbmidi_lib
snd_hda_codec_generic    98304  1 snd_hda_codec_realtek
coretemp               20480  0
snd_hda_codec_hdmi     73728  2
mc                     61440  1 snd_usb_audio
ledtrig_audio          16384  3 snd_hda_codec_generic,snd_hda_codec_realtek,snd_sof
kvm_intel             319488  0
snd_hda_intel          57344  6
snd_intel_dspcfg       24576  4 snd_hda_intel,snd_sof_pci,snd_sof_intel_hda_common,snd_soc_skl
joydev                 28672  0
snd_hda_codec         163840  5 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_codec_realtek,snd_soc_hdac_hda
kvm                   823296  1 kvm_intel
snd_hda_core          110592  10 snd_hda_codec_generic,snd_hda_codec_hdmi,snd_hda_intel,snd_hda_ext_core,snd_hda_codec,snd_hda_codec_realtek,snd_sof_intel_hda_common,snd_soc_hdac_hda,snd_soc_skl,snd_sof_intel_hda
iTCO_wdt               16384  0
snd_hwdep              16384  2 snd_usb_audio,snd_hda_codec
irqbypass              16384  1 kvm
rapl                   20480  0
snd_seq                86016  0
snd_seq_device         16384  2 snd_seq,snd_rawmidi
snd_pcm               131072  12 snd_hda_codec_hdmi,snd_hda_intel,snd_usb_audio,snd_hda_codec,snd_sof,snd_sof_intel_hda_common,snd_compress,snd_soc_core,snd_soc_skl,snd_hda_core,snd_pcm_dmaengine
snd_timer              49152  2 snd_seq,snd_pcm
intel_cstate           20480  0
snd                   106496  40 snd_hda_codec_generic,snd_seq,snd_seq_device,snd_hda_codec_hdmi,snd_hwdep,snd_hda_intel,snd_usb_audio,snd_usbmidi_lib,snd_hda_codec,snd_hda_codec_realtek,snd_timer,snd_compress,snd_soc_core,snd_pcm,snd_rawmidi
soundcore              16384  1 snd
intel_pmc_bxt          16384  1 iTCO_wdt
intel_uncore          163840  0
iTCO_vendor_support    16384  1 iTCO_wdt
ee1004                 20480  0
mei_hdcp               24576  0
i2c_i801               32768  0
i2c_smbus              20480  1 i2c_i801
pcspkr                 16384  0
wmi_bmof               16384  0
mei_me                 45056  1
mei                   122880  3 mei_hdcp,mei_me
intel_pch_thermal      16384  0
acpi_pad              184320  0
nf_conntrack_pptp      20480  0
ip_tables              32768  5 iptable_filter,iptable_security,iptable_raw,iptable_nat,iptable_mangle
i915                 2621440  13
nouveau              2342912  6
crct10dif_pclmul       16384  1
crc32_pclmul           16384  0
mxm_wmi                16384  1 nouveau
ttm                   122880  1 nouveau
crc32c_intel           24576  6
i2c_algo_bit           16384  2 i915,nouveau
drm_kms_helper        262144  2 i915,nouveau
cec                    61440  2 drm_kms_helper,i915
ghash_clmulni_intel    16384  0
drm                   626688  20 drm_kms_helper,i915,ttm,nouveau
r8169                  98304  0
uas                    32768  1
usb_storage            81920  1 uas
wmi                    36864  3 wmi_bmof,mxm_wmi,nouveau
video                  53248  2 i915,nouveau
pinctrl_cannonlake     36864  0
pinctrl_intel          32768  1 pinctrl_cannonlake
nf_conntrack          163840  9 xt_conntrack,nf_nat,nf_conntrack_tftp,nft_ct,nf_conntrack_pptp,nf_conntrack_netbios_ns,nf_nat_tftp,nf_conntrack_broadcast,xt_MASQUERADE
nf_defrag_ipv6         24576  1 nf_conntrack
nf_defrag_ipv4         16384  1 nf_conntrack
fuse                  139264  5
```

### Memory maneger

- **/bin** : All the executable binary programs (file) required during booting, repairing, files required to runinto single-user-mode, and other important, basic commands viz., cat, du, df, tar, rpm, wc, history, etc.
- **/boot** : Holds important files during boot-up process, including Linux Kernel.
- **/dev** : Contains device files for all the hardware devices on the machine e.g., cdrom, cpu, etc/etc : Contains Application’s configuration files, startup, shutdown, start, stop script for everyindividual program.
- **/home** : Home directory of the users. Every time a new user is created, a directory in the name of user iscreated within home directory which contains other directories like Desktop, Downloads, Documents,etc.
- **/lib** : The Lib directory contains kernel modules and shared library images required to boot the systemand run commands in root file system.
- **/lost+found** : This Directory is installed during installation of Linux, useful for recovering files which maybe broken due to unexpected shut-down.
- **/media** : Temporary mount directory is created for removable devices viz., media/cdrom.
- **/mnt** : Temporary mount directory for mounting file system.
- **/opt** : Optional is abbreviated as opt. Contains third party application software. Viz., Java, etc.
- **/proc** : A virtual and pseudo file-system which contains information about running process with aparticular Process-id aka pid.
- **/root** : This is the home directory of root user and should never be confused with ‘/‘
- **/run** : This directory is the only clean solution for early-runtime-dir problem.
- **/sbin** : Contains binary executable programs, required by System Administrator, for Maintenance. Viz.,iptables, fdisk, ifconfig, swapon, reboot, etc.
- **/srv** : Service is abbreviated as ‘srv‘. This directory contains server specific and service related files.
- **/sys** : Modern Linux distributions include a /sys directory as a virtual filesystem, which stores andallows modification of the devices connected to the system.
- **/tmp** :System’s Temporary Directory, Accessible by users and root. Stores temporary files for user andsystem, till next boot.
- **/usr** : Contains executable binaries, documentation, source code, libraries for second level program.
- **/var** : Stands for variable. The contents of this file is expected to grow. This directory contains log, lock,spool, mail and temp files.

*mv* sirve para mover archivos

    **mv** input output

*cp* hace un copy

    **cp** input output

*pwd* dice en que directorio te encuentras.

*touch* sirve para crear un file

*mkdir* sirve para crear un directorio
