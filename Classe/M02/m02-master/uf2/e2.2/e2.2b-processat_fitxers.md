# E2. Exercici 2b. Processat de fitxers II

## Introducció

Una de les grans ventatges de l'entorn UNIX és la facilitat per a processar fitxers, especialment fitxers amb dades, i transformar-les.

## Continguts

Feu servir el help i el manual per a cada comanda

## Entrega

Feu les següents preguntes amb el fitxer data2b.csv

1. Indiqueu el nombre de línies que conté
2. Creeu un nou fitxer que contingui les dades però no hi hagi cap **"** fent servir l'ordre **tr**. Feu servir a partir d'aquí aquest fitxer.
3. Ordeneu de major a menor l'última columna (7).
4. Mostreu només les columnes 4,3,1 en aquest ordre i separades per un tabulador.
5. Mostreu només les 10 últimes línies del fitxer substituïnt els **;** per **#**.
