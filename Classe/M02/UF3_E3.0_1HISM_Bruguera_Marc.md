# Autor:          UF3_E3.0_HISM_BRUGUERA_MARC.md
# Date:           20/04/21
# Description:    E3. Exercici 0. Treball de recerca sobre el SystemD

## 1. Qué és systemD?. Qué és SysV?. 
Systemd es un conjunt de processos creats per interactuar amb el kernel
SysV: Manera de gestionar el serveis de UNIX antic

## 2. En quin fitxer es troba la configuració general de systemd?
/etc/systemd/system.conf

## 3. Quina comanda fem servir per saber la versió actual de SystemD? 
systemctl --version

## 4. Que són les units en un systemD?. Fica 3 exemples.
Les units son tot lo que es gestiona el unix

Exemples:
gpm.service
gssproxy.service
import-state.service

## 5. Quina informació trobem en els fitxers amb extensió .swap?
.swap tenen dades que s'han intercambiat en memoria de disc.
## 6. Que consultem amb la següent comanda?
```
   systemctl list-units --type service
```
llistar unitat que son serveis
## 7. Quins estats pot tenir una Unit?. Fes una descripció.
enable
disable
active
inactive
failed

## 8. Els arxius de configuració de les Units ( siguin del tipus que siguin ) en quines 3 carpetes poden estar repartides?
/usr/lib/systemd/system: Paquets instalats del SO
/run/systemd/system: Instal·lats durant una execució 
/etc/systemd/system: Paquets preferents

## 9. Com podem afegir ( o sobreescriure ) opcions de configuració concretes, sense tenir que tocar les configuracions genèriques de la unit?
Anar al etc i crear la nostra configuració

## 10. Algunes "units" contenen el símbol @ en el seu nom (per exemple, nom@cadena.service); Que significa?
A partir d'una instancia s'ha creat una plantilla

## 11. Les següents comandes serveixen per gestionar Units. Descriu el que fa cada una d'elles:
systemctl --state inactive = llista els processos que estan inactius
systemctl --failed = llista els processos que han fallat
systemctl --all = llista tots els processos

## 13. Com reiniciem una Unit?. Posa un exemple per exemple de una Unit de tipus socket
systemctl {restart} nomUnit.socket

## 14. Com sabem si un unit està activado, desactivado o ha fallat?
systemctl status x.service

## 15. Quina comanda fem servir si volem instalLar el servidor web apache2 (httpd)?
sudo apt install httpd

## 16. Què són els targets en SystemD?
Serveixen per determinar la manera de com arrenquem el sistema

## 17. Qué és un servei?. Com es crea un servei amb SystemD?. Quina estructura té? A on es creen?
cd /etc/systemd/system
.service
[unit]
Descpription = Nou script

[service]
	ExecStart = 
[install]
	wanted by = multi-user.target

## 18. Una vegada creat un servei, que hem de fer perque el systemctl el tingui en compte?
systemctl daemon-reload

## 19. Com podem comprovar informació relacionada amb la execucio dels serveis? 
systemctl status nom.service
	loaded = carregat al sistema
	active = correns pel sistema
	main PID = ID
	

## 20. Sota systemD, qui és l'encarregat de recolectar i emmagatzemar l'activitat del que va passant al sistema?
journal
