# Maquines virtuals
QEMU
## Disc Durs dins de les maquines virtuals.
Amb Fedora, Debian, etc. Podem instalar el virt-maneger per gestionar maquines.
```
# dnf install @Virtualization
or
# dnf -y install bridge-utils libvirt virt-install qemu-kvm virt-maneger
```
Per activar el stat active(running) hem de mirar si esta actiu o no. Amb el CLI fem un "*systemctl status libvirtd*". Si esta **active** bé, pero en cas contrari s'ha de fer un "*systemctl start libvirt*" per deixar el procés correns.

Si estem conectats ens ha de deixar crear una maquina (en el botó de la part superior- esquerra)

# Processat de fitxers
## Enllaços simbòlics (soft / symbolic links) i enllaços durs (hard)
**Enllaços soft** = En Windows el coneixem com accés directe, lo que fa un enllaç directe reedirigir per poder anar al executable directament sense necesitat de buscar-ho expressament. En Linux existeis algo similar.

Per crear enllaços simbolics es fa amb la commanda **ln**. Si es fa nomes el target serà un enllaç hard, pero si li fiques diferents opcions es converteix en un enllaç soft. Es fa amb el **-s** [ES LA OPCIÓ QUE EL CONVERTEIX EN SOFT]
```
ln -s /etc/hosts maquines
ln [OPTION] [DIRECTORI] [NOM DEL FITXER]
```
Aquesta commanda lo que fa es fer un accés directe, es lo mateix fer **cat /etc/hosts** que **cat maquines**.

**Enllaços hard** = Aquests enllaços no s'utilitzen molt. Lo que fa es que fer un accés directe pero aquest accés es permanent, es com una copia del fitxer pero tots els cambis que es fan influeixen en tots els enllaços hard que s'hagin fet.

Per fer un enllaç hard es lo mateix que el soft pero treient el **-s**.
```
ln /etc/hosts maquineshard
ln [DIRECTORI] [NOM DEL FITXER]
```
Aquets enllaços el ordinador el detecta com un fitxer i no com un enllaç [nom del arxiu soft **->** carpeta]. Segons el disc dur es el mateix fitxer. 

## Ordres de Processat
**cut**: Ens permet extreure camps d'un llistat
- **-f number**: Indicar files *concretes* que tu vulguis.
```
    Command nomral
    cut -f 1 data.cvs

    Rang de files:
    cut -f 1-4 data.csv
```
- **-d "delimitador"**= lo que fa es delimitar com es separen les columnes o files del propi arxiu.
```
    cut -f 1 -d ";" data.cvs
```
**sort**: Ordena les files
```
sort data.csv
Combinació de l'anterior
cut -f 1 -d ";" data.cvs | sort
```
**head**: Diu el "cap" del arxiu

**tail**: Diu la part final del arxiu
```
Aqui comença treure per pantalla a partir de la segona fila:

tail -n +2 data.csv
```
**uniq**: serveix per descartar lineas duplicades "adyacentes"
```
sort data.csv | uniq 
```
## Scripts

#### Comparacions de cadenes alfanumèriques

|Operador	Veritat (TRUE) |si:                                                         |
|------------------------|------------------------------------------------------------|
|cadena1 = cadena2			 |cadena1 és igual a cadena2                                  |
|cadena1 != cadena2			 |cadena1 no és igual a cadena2                               |
|cadena1 < cadena2			 |cadena1 és menor que cadena2                                |
|cadena1 > cadena 2			 |cadena1 és major que cadena 2                               |
|-n cadena1					     |cadena1 no és igual al valor nul (longitud més gran que 0)  |
|-z cadena1					     |cadena1 té un valor nul (longitud 0)                        |

#### Comparació de valors numèrics

|Operador	Veritat (TRUE) |si:                    |
|------------------------|-----------------------|
|x -lt y					       | x menor que y         |
|x -le y					       | x menor o igual que y |
|x -eq y					       | x igual que y         |
|x -ge y					       | x major o igual que y |
|x -gt y					       | x major que y         |
|x -ne y					       | x no igual que y      |

#### Comprobació d'atributs de fitxer

|Operador	Veritat (TRUE) | si:                               |
|------------------------|-----------------------------------|
|-d fitxer				       | fitxer existeix i és un directori          |
|-e fitxer				       | fitxer existeix                            |
|-f fitxer				       | fitxer existeix i és un fitxer regular (no un directori, o un altre tipus de fitxer especial) |
|-r fitxer				       | Tens permís de lectura en fitxer           |
|-s fitxer				       | fitxer existeix i no aquesta vacio         |
|-w fitxer				       | Tens permís d'escriptura en fitxer         |
|-x fitxer				       | Tens permís de ejecucion en fitxer (o de busqueda si és un directori) |
|-O fitxer				       | Ets el propietari del fitxer               |
|-G fitxer				       | El grup del fitxer és igual al teu.        |
fitxer1 -nt fitxer2		   | fitxer1 és mes recent que fitxer2      |
fitxer1 -ot fitxer2		   | fitxer1 és mes antic que fitxer2       |

#### if/*else

La sintaxi d'aquesta construcció és la següent:

```
#!/bin/bash
if [[ "condició" ]];
then
  "ordres"
[elif "ordres"
then
  "ordres"]
[else
  "ordres"]
fi
```
#### while
La sintaxi d'aquesta construcció és la següent:

```
#!/bin/bash
while condició
do
  ordres
done
```

#### Cron
Executar el cron en /etc/cron.d/nomdebash en sudo.



# Privilegios SUDO
```
su -
vi /etc/group
    wheel:x:10:**nombre de usuario**
    :wq
reboot
```
or
```
usermod -aG wheel **nombre de usuario**
```

