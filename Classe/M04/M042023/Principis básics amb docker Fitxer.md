```
docker run \
    --name openldap-server \
    -p 389:389 -p 636:636 \
    --hostname ldap.juegos.epicos \
    --env LDAP_ORGANISATION="juegos epicos" \
    --env LDAP_DOMAIN="juegos-epicos" \
    --env LDAP_ADMIN_PASSWORD="Jupiter1" \
    --env LDAP_BASE_DN="dc=juegos,dc=epicos" \
    --volume /data/slapd/database:/var/lib/ldap \
    --volume /data/slapd/config:/etc/ldap/slapd.d \
    --detach osixia/openldap:latest

docker run \
    --name phpldapadmin \
    -p 10080:80 \
    -p 10443:443 \
    --hostname phpldapadmin-service \
    --link openldap-server:ldap-host \
    --env PHPLDAPADMIN_LDAP_HOSTS=ldap.juegos.epicos \
    --detach osixia/phpldapadmin:latest
```

https://localhost:10443/
Login DN = cn=admin,dc=computingforgeeks,dc=com
Password = Jupiter1

https://fossbytes.com/tools/random-name-generator

!(JUEGOS)[https://moodle.escoladeltreball.org/pluginfile.php/252210/mod_resource/content/2/photo_2023-02-28_14-09-25.jpg]
