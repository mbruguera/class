#!/bin/bash
echo "" > illes_balears.ldif

sucursal=$(cat /home/users/inf/wism2/a191019mb/Escriptori/class/Classe/M04/M042023/Illes_Balears.csv | cut -d ";" -f 8 | sort -u)

ubi=$(cat /home/users/inf/wism2/a191019mb/Escriptori/class/Classe/M04/M042023/Illes_Balears.csv | cut -d ";" -f 9 | sort -u | tr " " -t)

#SCRIPT QUE IMPRIME LA SUCURSAL
echo -e "dn: ou= "$sucursal",dc=juegos,dc=epicos\nou: "$sucursal"\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif

#SCRIPT QUE IMPRIME SUCURSAL + UBICACIÓN
for i in $ubi
do
	echo "dn: ou="$i",ou="$sucursal",dc=juegos,dc=epicos" >> illes_balears.ldif
	echo "ou: "$i >> illes_balears.ldif
	echo "objectClass: organizationalUnit" >> illes_balears.ldif
	echo -e "objectClass: top\n" >> illes_balears.ldif
done

