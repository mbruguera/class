Abans de començar amb l'explicació, posarem en "tesitura" a tothom. L'objectiu de l'activitat és fer un script en el qual, amb l'ajuda d'un fitxer .csv, traduir una base de dades per pujar-la al LDAP.

### Pas 0
El pas zero és el més senzill de tots, ja que nosaltres no hem de fer res, o en tot cas és la part més fàcil, i és tenir el fitxer CSV amb la base de dades que haurem de pujar. El fitxer estarà separat per ';' amb el següent ordre:

data;DNI;cognom1;cognom2;nom;data de naixement;e-mail;sucursal;ubicació;sector 


### Explicació de les comandes
L'script està fet a partir d'un llenguatge que es diu BASH. Aquest llenguatge té moltes comandes amb les quals podem interactuar amb ell. Un exemple és el 'select' o el 'tail', entre molts. Ara introduirem algunes de les comandes que hem utilitzat en el nostre script.


- echo : s'utilitza als scripts de Bash i als fitxers per lot per mostrar el text d'estat en un fitxer o a la pantalla.
    - \n : s'utilitza per fer un salt de línia.
    - ">>" : redirigeix la sortida estàndard d'una ordre.
```
echo -e "Hello World!\nNice to see you" >> file.txt
```
En aquest cas, l'opció "-e" ens permet fer servir la seqüència d'escapament "\n" per inserir un salt de línia després del missatge. L'operador ">>" redirigeix la sortida de l'ordre "feta" al fitxer "miarchivo.txt" en mode "afegir", la qual cosa significa que el contingut anterior del fitxer no serà esborrat i la nova línia serà agregada al final del fitxer.

- for : l'ordre for ens permet crear un petit bucle que faci alguna acció, executant una ordre, recorrent una llista d'elements. És una ordre que implementa les sentències iteratives dels llenguatges de programació estructurats.
    - do: és la paraula clau que inicia els bucles.
    - done: atura el bucle
```
for var in valors
do
  command
done
```
On "var" és el nom de la variable de control, "valors" és el conjunt de valors que s'utilitzaran per iterar, "command" és la tasca que es realitzarà en cada iteració del bucle, i "done" indica el final del bucle.

- while : l'ordre while és una instrucció que genera un bucle. Quan la condició deixi de complir-se, l'execució del programa sortirà del bucle i no realitzarà més iteracions.

- read: la comanda read llegeix la seva entrada estàndard i assigna les paraules llegides a la (s) variable (s) el nom de la qual es passa com a argument.

Aquestes tres comandes ens serviran per reduir la cerca en el moment de buscar el que la nostra comanda dicti.

- cat: és utilitzat per llegir un fitxer seqüencialment i imprimir-lo a la consola de manera estàndard. El seu nom és derivat de la seva funció per concatenar fitxers.

- grep: ens permet cercar cadenes de text i paraules dins d'un fitxer de text o de l'entrada estàndard de la terminal.

- sort: és una ordre per ordenar línies de fitxers de text. Permet ordenar alfabèticament, en ordre invers, per nombre, mes i també pot eliminar duplicats.

- tail: és una excel·lent ordre utilitzada per imprimir els últims N números o cues (tails) d'una entrada.

- cut: permet extreure columnes o camps seleccionats a partir de la vostra entrada estàndard o de fitxers.

- uniq: la comanda uniq serveix per trobar i eliminar del text els duplicats.

- tr: permet a l'usuari definir explícitament com estarà compost el conjunt o bé proveeix una col·lecció de caràcters i conjunts predefinits, que poden ser utilitzats per definir-los.

Un exemple sería aixó:
```
cat /home/users/inf/inf/vrey/Desktop/Illes_Balears.csv | cut -d ";" -f 5 | sort | uniq | tr " " -t
```
La comanda "cat" imprimeix per pantalla tot el contingut del fitxer "/home/users/inf/inf/vrey/Desktop/Illes_Balears.csv".
```
2023-09-04;98765432F;Sánchez;Martínez;Pedro;1958-02-28;pedro.sanchezmartinez@empresa.com;Illes Balears;Oficina;Administración 
2023-09-05;23698753N;González;Pérez;Ana;1969-03-19;agonzalez@empresa.com;Illes Balears;Oficina;Seguretat
2023-08-02;64298573H;Diaz;Rodriguez;Ana;1962-03-04;ana.diaz.rodriguez@banc.com;Illes Balears;Magatzem;Administración 
2023-02-07;01234567B;Sánchez;Pérez;Ana;1984-01-12;ana.sanchez.perez@empresa.com;Illes Balears;Magatzem;Seguretat
2023-02-07;X1977825D;Cuesta;Maldonado;Bernardo;1958-12-15;bernardo.cuesta@empresa.com;Illes Balears;Oficina;Seguretat
2023-08-02;94256372G;Jimenez;Garcia;David;1960-12-17;david.jimenez.garcia@banc.com;Illes Balears;Magatzem;Neteja 
2023-09-06;32198654H;López;Gómez;David;1956-11-19;david.lopezgomez@empresa.com;Illes Balears;Oficina;Administración 
2023-09-06;15567824G;Jiménez;Muñoz;Diana;1962-09-10;djimenez@empresa.com;Illes Balears;Tenda física;Venedor
2023-02-05;46632773C;Jurado;Esteban;Elvira;1982-06-02;elvira.jurado@empresa.com;Illes Balears;Oficina;Seguretat
2023-08-02;72358647B;Lopez;Perez;Isabel;1957-05-08;isabel.lopez.perez@banc.com;Illes Balears;Magatzem;Distribució
2023-09-03;12349876Z;Martínez;González;Isabel;1959-08-17;isabel.martinezgonzalez@empresa.com;Illes Balears;Oficina;Recursos Humans
2023-09-01;19292083R;García;López;Juan;1954-06-12;jgarcia@empresa.com;Illes Balears;Oficina;Neteja 
2023-09-10;17907864R;López;Fernández;Juan;1963-07-15;jlopez@empresa.com;Illes Balears;Tenda física;Venedor
2023-09-04;13907862F;Martín;Giménez;Javier;1963-01-08;jmartin@empresa.com;Illes Balears;Tenda física;Venedor
2023-09-07;24569875H;Sánchez;Gómez;José;1968-11-12;jsanchez@empresa.com;Illes Balears;Tenda física;Neteja 
2023-08-02;83724563C;Garcia;Santos;Juan;1959-02-19;juan.garcia.santos@banc.com;Illes Balears;Magatzem;Distribució
2023-07-02;12345678A;Pérez;Rodríguez;Juan;1992-07-02;juan.perez@empresa.com;Illes Balears;Magatzem;Administración 
2023-09-02;85367214C;Pérez;Rodríguez;Juan;1960-05-21;juan.perezrodriguez@empresa.com;Illes Balears;Oficina;Recursos Humans
2023-02-07;33157564C;Rodríguez;López;Juan;1983-12-11;juan.rodriguez.lopez@empresa.com;Illes Balears;Tenda física;Seguretat
2023-02-07;56432870M;Gómez;Martín;Laura;1983-07-23;laura.gomez.martin@empresa.com;Illes Balears;Magatzem;Seguretat
2023-09-05;65432198G;Gómez;Pérez;Lucía;1957-06-13;lucia.gomezperez@empresa.com;Illes Balears;Oficina;Administración 
2023-02-07;37890297A;García;Fernández;María;1985-06-22;maria.garcia.fernandez@empresa.com;Illes Balears;Tenda física;Seguretat
2023-06-05;87654321B;García;López;María;1988-06-05;maria.garcia@empresa.com;Illes Balears;Magatzem;Reponedor
2023-09-08;43219876K;Ruiz;Martínez;Mario;1954-12-23;mario.ruizmartinez@empresa.com;Illes Balears;Oficina;Administración 
2023-08-02;58673528D;Gonzalez;Martin;Marta;1962-06-12;marta.gonzalez.martin@banc.com;Illes Balears;Magatzem;Distribució
2023-09-09;8765432L;Gutiérrez;Ruiz;Marta;1953-04-06;marta.gutierrezruiz@empresa.com;Illes Balears;Oficina;Neteja 
2023-09-03;11457864P;Rodríguez;Martínez;María;1968-08-14;mrodriguez@empresa.com;Illes Balears;Oficina;Becari
2023-02-07;72813905K;Martínez;González;Pablo;1987-03-17;pablo.martinez.gonzalez@empresa.com;Illes Balears;Tenda física;Encarregat
2023-08-02;72638456E;Rodriguez;Gomez;Pablo;1960-01-03;pablo.rodriguez.gomez@banc.com;Illes Balears;Magatzem;Distribució
2023-02-07;70981256J;Hernández;Ruiz;Pedro;1986-05-16;pedro.hernandez.ruiz@empresa.com;Illes Balears;Magatzem;Seguretat
2023-09-02;12457896L;Ruiz;Hernández;Pedro;1962-05-17;pruiz@empresa.com;Illes Balears;Tenda física;Neteja 
2023-08-02;82963455F;Ruiz;Martinez;Rosa;1958-10-28;rosa.ruiz.martinez@banc.com;Illes Balears;Magatzem;Neteja 
2023-09-08;16907852H;Alonso;Santos;Sonia;1960-12-24;salonso@empresa.com;Illes Balears;Tenda física;Venedor
2023-09-07;98765421J;Díaz;Sánchez;Sara;1955-07-01;sara.diazsanchez@empresa.com;Illes Balears;Oficina;Administración 
```

La comanda "cut" amb l'opció -d (delimitador) i -f (fila) elimina els delimitadors per facilitar la cerca i mostra tota la informació de la fila especificada, en aquest cas la fila 5.
```
Pedro
Ana
Ana
Ana
Bernardo
David
David
Diana
Elvira
Isabel
Isabel
Juan
Juan
Javier
JosÃ©
Juan
Juan
Juan
Juan
Laura
LucÃ­a
MarÃ­a
MarÃ­a
Mario
Marta
Marta
MarÃ­a
Pablo
Pablo
Pedro
Pedro
Rosa
Sonia
Sara
```
La comanda "sort" ordena tot el contingut de la fila per ordre alfabètic.
```
Ana
Ana
Ana
Bernardo
David
David
Diana
Elvira
Isabel
Isabel
Javier
JosÃ©
Juan
Juan
Juan
Juan
Juan
Juan
Laura
LucÃ­a
MarÃ­a
MarÃ­a
MarÃ­a
Mario
Marta
Marta
Pablo
Pablo
Pedro
Pedro
Pedro
Rosa
Sara
Sonia
```

La comanda "uniq" elimina els duplicats.
```
Ana
Bernardo
David
Diana
Elvira
Isabel
Javier
Jose
Juan
Laura
Lucia
Maria
Mario
Marta
Pablo
Pedro
Rosa
Sara
Sonia
```
I la comanda "tr -t" reemplaça tots els espais en blanc amb un guió, cosa que pot ser útil per crear identificadors d'usuari o noms de fitxers sense espais en blanc.

### Comentant l'script
Borrarem el ldif anteriorment creat, per si hi ha alguna falla, i crearem un de nou.
```
rm illes_balears.ldif
echo "" > illes_balears.ldif
```
Llegeix el contingut del fitxer "Illes_Balears.csv",  busca totes les sucursals (columna 8) del fitxer "Illes_Balears.csv", les ordena per ordre alfabètic, les agafa sense repetir i les guarda en una variable anomenada "sucursal".
```
sucursal=$(cat /home/users/inf/inf/vrey/Desktop/Illes_Balears.csv | cut -d ";" -f 8 | sort | uniq | tr " " -t)
```
Aqui introduim al nostre arxiu .ldif la sucursal de la fila 8, que en aquest cas es Illes Balears.
```
echo -e "dn: ou="$sucursal",dc=computingforgeeks,dc=com\nou: "$sucursal"\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif

echo -e "dn: ou=Illes Balears,dc=computingforgeeks,dc=com\nou: Illes Balears\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif
```
Llegeix el contingut del fitxer "Illes_Balears.csv",  busca totes les ubicacions (columna 9) del fitxer "Illes_Balears.csv", les ordena per ordre alfabètic, les agafa sense repetir i les guarda en una variable anomenada "ubicacio".
```
ubicacio=$(cat /home/users/inf/inf/vrey/Desktop/Illes_Balears.csv | cut -d ";" -f 9 | sort -u | tr " " -t)
```
Aquesta ordre crea una estructura de directoris en què cada unitat organitzativa representa una ubicació (o sucursal), i dins de cadascuna es creen altres unitats organitzatives per a empleats, usuaris i grups.
```
for i in $ubicacio 
do 
echo -e "dn: ou="$i",ou="$sucursal",dc=computingforgeeks,dc=com\nou: "$i"\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif

echo -e "dn: ou=empleados, ou="$i",ou="$sucursal",dc=computingforgeeks,dc=com\nou: empleados\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif

echo -e "dn: ou=users,ou=empleados, ou="$i",ou="$sucursal",dc=computingforgeeks,dc=com\nou: users\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif

echo -e "dn: ou=groups,ou=empleados, ou="$i",ou="$sucursal",dc=computingforgeeks,dc=com\nou: groups\nobjectClass: organizationalUnit\nobjectClass: top\n" >> illes_balears.ldif
done
```
Aquestes dos comandes, el seu rol es:
    
- $gidNumber : Aquesta comanda cerca el valor més gran del camp 'gidNumber' dins la base de dades LDAP, corresponent al número d'identificació del grup, i el desa en la variable 'gidNumber'. Utilitza la comanda 'ldapsearch' per buscar els grups amb l'objecte 'posixGroup', filtra els resultats amb 'grep', ordena les línies amb 'sort', selecciona l'última línia amb 'tail', i finalment talla el valor de 'gidNumber' amb 'cut'.

- $grups : Aquesta comanda llegeix, ordena i guarda en una variable les dades de dues columnes separades pel punt i coma (;) del fitxer "Illes_Balears.csv", eliminant les entrades duplicades i substituint els espais per guions: "grups=$(cat Illes_Balears.csv | cut -d ';' -f 9,10 | sort -u | tr ' ' '-')".

```
gidNumber=$(ldapsearch -x -LLL -D 'cn=admin,dc=computingforgeeks,dc=com' -w 'Jupiter1' -b 'dc=computingforgeeks,dc=com' '(objectClass=posixGroup)' | grep gidNumber | sort | tail -n 1 | cut -d " " -f 2)

grups=$(cat /home/users/inf/inf/vrey/Desktop/Illes_Balears.csv | cut -d ";" -f 9,10 | sort -u | tr " " "-")
```
Fa el bucle per poder inserir al arxiu el gidnumber i el grup en el que es troba.
```
for g in $grups
do
  group=$(echo $g | cut -d ";" -f 2) 
  ((gidNumber+=1))
  ubi=$(echo $g | cut -d ";" -f 1) 
  echo -e \
  "dn: cn="$group",ou=groups, ou=empleados,ou="$ubi",ou="$sucursal", dc=computingforgeeks,dc=com\ncn: $group\ngidNumber: $gidNumber\nobjectClass: posixGroup\nobjectClass: top\n">>illes_balears.ldif 
done
```
Aquesta comanda fa diverses coses:

- Utilitza el programa ldapadd per afegir els continguts del fitxer illes_balears.ldif a la base de dades LDAP, utilitzant l'usuari i la contrasenya especificats amb les opcions -D i -w.
- Mostra el contingut del fitxer illes_balears.ldif a la sortida estàndard, utilitzant el programa cat.
- Llegeix línia per línia el contingut del fitxer Illes_Balears.csv, i per a cada línia:
- Extreu diversos camps separats per punt i coma (;) i els assigna a les variables "nom", "cognom", "DNI", "data" i "password".
- Crea una contrasenya per a l'usuari a partir del segon camp i de la data de naixement, i la guarda a la variable "password".
```
ldapadd -c -x -D 'cn=admin,dc=computingforgeeks,dc=com' -w 'Jupiter1' -f illes_balears.ldif

cat illes_balears.ldif

while read fila
do
nom=$(echo $fila | cut -d ";" -f 5)
cognom=$(echo $fila | cut -d ";" -f 3)
DNI=$(echo $fila | cut -d ";" -f 2)
data=$(echo $fila | cut -d ";" -f 6)
password=$(echo $fila | cut -d ";" -f 2)$(echo $data | cut -d "-" -f 2,3 | tr -d "-" )   
done < Illes_Balears.csv)
```
