# M04: SISTEMES OPERATIUS PROPIETARIS EN XARXA
Quan tens una xarxa mantenen 2 o més equips amb l'objectiu de compartir recursos:

**Servidors**: Centralitzen la gestió de la infraestructura. Els SO servidors porten incorporat tot el software necessari per a que els clients es connecten a ells. Així mateix, també disposen de les eines especifiques per suministrar els serveis als clients.

**Client**: El que reclama els recursos. Avui en dia contem amb Windows, Linux o Catalina (Apple) com a clients. Aquests sistemes solen venir preparats per a connectar amb servidors.

Caracteristiques dels S.O. - Servidors en xarxa
- Compartir recursos 
- Gestió d'usuaris (Crear, modificar o borrar users. Modificar drets i permisos, assiganr o denegar permisos.)
- Gestionar la xarxa
    - Els serveis més habituals que proporcionen son els següents:
        - Compartició de recursos (Carpetas o NFS)
        - seguretat
        - Impressió
        - Xarxa:
            - Missatgeria o Correu
            - Gestió d'incidències
            - Exploració ()
            - Assignació (DHCP)
            - Interoperabilitat
            - PXE
            - Vigilància

Diferents paràmetres que cal tenir una infraestructura:
- Nivell de seguretat
- Nombre d'usuaris
- Nombre d'equips
- Ampliacions posteriors
- Nivell d'interoperabilitat* a la xarxa
- Economia

### ACTIVITAT 1
**DIFERENCIA ENTRE CLIENT I SERVIDOR**

El _servidor_ es el que s'encarrega de portar la xarxa, ja que té tots els privilegis, pot limitar quins ports té oberts o tancats. Ha d'estar encés 24/7 per tenir els serveis actius. Necesiten altres tipos de nivells de seguretat, com per exemple en cas de sobrecarrega o apagada un metode de seguretat es un SAI.

El _client_ és el que es connecta al propi servidor amb uns privilegis restringits, pot entrar-hi només on tingui permís del propi servidor. Pero no té perque, ja que tota la part de HHTP o Firewall en cas de entorn gran, ho porten diferents clients.


*Interoperabilitat: Jugar ser diferents clients
