# Pràctica 1: Instal·lació de Windows Server
### Pre-Instal·lació del servidor
Anem a crear un nou servidor amb el S.O. de Windows. Aquest servidor l'haurem de descarregar abans en la pagina oficial de Windows o en un d'aquests dos links:
```
Windows server 2022: https://www.microsoft.com/es-xl/evalcenter/evaluate-windows-server-2022
Windows server 2019 (escola): https://drive.google.com/file/d/11V5Ek4ODhA-3beIdEFdrCivCZhLCmp3R/view?usp=sharingç
```

Una vegada descarregada dita ISO, ja que tardarà una estona depenent de la teva conexió, obrirem el programa amb el que instal·larem el servidor. En el cas d'aquesta pràctica ho farém en una màquina virtual, amb el programari de "Virtual Box".
```
Virtual Box: https://www.virtualbox.org/wiki/Downloads
    VirtualBox 6.1.26 platform packages (depén de quin S.O. utilitzis...)
        - Windows hosts
        - OS X hosts
        - Linux distributions
        - Solaris hosts
        - Solaris 11 IPS hosts 
```
Per l'instal·lació de **Windows** (VirtualBox) és molt senzilla, ja que es un _.exe_ que practicament es tot donar-li a "_següent_" i ja l'instal·lador fa el reste.

Per l'instal·lació en **Debian** (VirtualBox) és més complexa pero res fora de lo normal, només has de copiar i enganxar les seguents comandes al teu terminal. [1]
```
$ sudo apt install gnupg2
$ wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
$ wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
$ echo "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian bullseye contrib" | sudo tee /etc/apt/sources.list.d/virtualbox.list
$ sudo apt update
$ sudo apt install linux-headers-$(uname -r) dkms
$ sudo apt install virtualbox-6.1
$ virtualbox
```
[1] Comandes tretes de la pagina web _https://linoxide.com/how-to-install-virtualbox-on-debian-11/_

Una vegada amb totes les eines, anem a començar a fer l'instal·lació del nostre servidor.

### Instal·lació del servidor
Dintre del _VirtualBox_ crearem una nova maquina de la següent manera:
1. Cercarem el botó blau que surt a la part superior de la _benvinguda a VirtualBox_ que es diu **new** o **nuevo** o **nova**, tot depén en quin idioma ho tinguis.
2. Li triem el nom que volguem, en el meu cas "WinServer2022" i li diem on ho volem guardar. En el meu cas, de Windowsho guardaré en una carpeta en el escriptori. *C:\Users\marcb\Desktop\VirtualBox machines* o */home/users/inf/wism2/ism49298715/VirtualBox VMs*
3. Li triem la quantitat de RAM que es reservaran per la maquina virtual (en megabytes). En el meu cas, destinaré 1024MB ja que el meu ordinador no es molt potent.
4. Crearem un disc dur virtual amb l'opció de *CREA UN DISC DUR VIRTUAL ARA* de uns 32,00 GB.
5. El tipus de fitxer de disc dur será VHD (Virtual Hard Disk)
6. **IMPORTANT** L'emmagatzematge del disc dur serà *ubicat de forma dinàmica* ja que així podrém jugar molt més.
7. Li diem de quan será el disc dur, que com anteriorment he dit serà de 32GB, i on estará guardat: C:\Users\marcb\Desktop\VirtualBox machines\WinServer2022.vhd

Amb tot aixo, ja tindríem la maquina creada amb tot lo básic per funcionar, pero sense S.O. ni sense els canvis que ara li ficarem.

El configurarem amb l'opció de *paràmetres* que es l'icone taronja de la part superior. Una vegada dins dels paràmetres, modificarem lo següent.
1. Dins de la xarxa, canviarem **NAT** per **adaptador pont** o **bridge adapter**
2. Dins del emmagatzematge modificarem l'icone del disc virtual i li insertarem la nostre ISO, anteriorment descarregada. Li donarém clic a l'icone del disc i a la part de la dreta li donarém en aquestes opcions. [2]
![FOTO DISCO](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/FOTO_DISCO.png)
[2] Es una fotografía de el VirtualBox de les opción per configurar el disc optic.
3. Li donem a l'icone d'arrencada.

### Intal·lació del sistema operatiu
Alhora d'iniciar ens preguntará per l'idioma de l'instal·lació, l'idioma del teclat i en quin format i moneda funciona on estem, tot depén on i com ho vols configurar. En el meu cas ho deixaré tot en castellá, també es l'unica opció que et dona. [3]

![IDIOMA](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualBox_WinServer_idiom.png)

[3] Es la primera pantalla que ens pregunta pel idioma

I li donem al botó que posa "Instalar ahora" que surt en el mig de la pantalla. I ara ve la part on administrarem tota la part d'on estrá ubicat el nostra S.O. i com l'instal·larem:
1. Seleccionarem quin sistema volem, i en sortiràn 4 opcions:
    - Windows Server 2019 Standard Evaluation 
    - Windows Server 2019 Standard Evaluation (Experiencia de es...) <-- Aquesta serà la opció que triarem ja que té entorn gràfic
    - Windows Server 2019 Datacenter Evaluation
    - Windows Server 2019 Datacenter Evaluation (Experiencia de es...)
2. Acceptem els termes de lliçencia
3. El tipos d'instal·lació que farém es personalitzada per nosaltres:
    - Seleccionem el disc dur, que només sortirà un ja que només tenim 1.
    - Li donem a "nuevo" i apliquem els canvis per que es fagin totes les particions.
    - Finalitzem aquesta part donan-li a *siguiente*

Una vegada s'acabi tot, ens demanarà posar una contrasenya, en el meu cas serà "WinServer2019!", pero es pot posar algo més segur ja que es un servidor. I ja amb això podrem disfrutar del nostre servidor.

## Exercici 2
Hi han moltissimes maneres d'apagar un ordenador, una millors i altres pitjors. Ara os presentaré 4 exemples de com es poden apagar el PC.

**Apagar el SO desde botó d'inici:** Desde un servidor, la apagada desde l'inici es més diferent que desde un Windows de casa convencional. En el servidor has de escollir com i que vols apagar (hardware instalació o manteniment o sistema operatiu en instalació o manteniment o otros (planeado o no planeado) entre altres)

**Apagar la maquina bruscament:** La maquina al iniciar et pregunta el perque s'ha apagat d'aquesta manera i et pregunta perque has apagat aixì. [4]

![Pregunta](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualB.png)

**Apagar la maquina per ACPI:** Apagar per ACPI es la funció que fem al mantender pulsat el botó d'arrencada de la propi pc, és menos brusca que l'anterior ja que es com si li comuniques que s'apagara en uns moments, no com l'anteror que es treura-li la corrent de cop. I amb la diferencia que no salta el missatge com a l'anterior.

**Per arrencar el SO de forma remota**, s'ha de entrar en l'inici avançat entran a l'inici amb l'F8 i escollir el mode *modo seguro con funciones de red*

## Exercici 3
Tota la informació treta de la següent pagina web.
```
https://www.solvetic.com/tutoriales/article/3284-instalar-y-configurar-servidor-dns-windows-server-2016/
```
Per poder instal·lar el servidor DNS sha de fer de la següent manera.


Per poder tenir el nostre servidor DNS amb la nostra IP fixe i tot, ho farem de dos maneres.

**IMPORTANT**: Per poder veure la IP que tením es veu en el cmd amb el comando **ipconfig**

La gràfica:
```
https://www.muycomputer.com/2017/12/04/encontrar-cambiar-direccion-ip-windows-10/
```

- Hem d'afegir un rol per el nostre servidor  i picar a la opcio de "Agregar roles y caracteristicas"[5]

![ROL](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualBox_WinServer_DNS.png)

[5] La pagina d'administració de servidor amb l'opcio que hem de donar-li.

- Hem de fer la configuració ara de quin rol volem que fagi el nostre servidor, llavors farem tot seguent fins l'apartat *Roles de servidor* i dir-li que volem un Servidor DNS[6]

![DNS](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualBox_WinServer_DNS2.png)

[6]La pantalla que ens dona a l'hora de voler instalar el servidor

- Li donem a la següent pantalla acceptan tot  (Intal·lació per rols i escollir un servidor, i com tenim un doncs no fa falta menjar-se el cap) i al final ens sortirà un botò al costat que diu *instalar*, li piquem. I començarà el procés d'instalació del servidor DNS.

- Ara li crearem una IP fixe desde la configuració de Windows en l'apartat de xarxa/ethernet. [7]

![Config](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualBox_WinServer_config.png)

[7] Pantalla on hem d'anar per començar el procés

- Li donarme a l'opció *cambiar opciones de apaptador i enj l'opcio d'*ethernet* (en el meu cas) o en el cas de que vagi la nostra xarxa conectada al servidor, li donem click dret i propietats.

- Li piquem a l'opcio *protocolo de internet version 4) i li afegirem l'ip 10.200.245.3 (en el meu cas), amb la seva mascara /24 [255.255.255.0] i la direcció de DNS preferit. [8]

![IP](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/VirtualBox_WinServer_IP.png)

[8]El resultat final de com t'ha de quedar més o menys



Per terminal: 
```
https://www.piavant.es/cambiar-la-ip-desde-la-linea-de-comandos-de-windows/
```

Per norma general la comanda que s'utilitza es la següent:

<netsh interface ip set address "Description" static %ip propia% %mascara% %gateway% %metric%>

Metric= representa la tarjeta de xarxa, per norma general es 1.

Per parar/iniciar el servei n'hi han dos maneres:
- Administración del servidor > DNS > (*click dret sobre el servidor*) Administración de DNS > Todas las tareas > Detener/Iniciar
- Administrador de tareas > Servicios > DNS > (*click dret sobre DNS*) > Detener/Iniciar
- stop/start-service dns (POWER SHELl)

Per canviar el nom fem:
- Administración del sistema > Sistema local > (*click izquierdo sobre el nombre que te salga [Win489019etc..]*) > Cambiar > server03


## Exercici 4
Ara crearem una nova tasca per que fagi el nostre servidor, que lo que farà serà una comprovació del nostre disc amb la commanda *chkdsk**:
- Per crear una tasca obrirem el nostre panel de control i cercarem la paraula 'tasca'.
- Ens obrira algunes opcions, pero la que ens interesa en la de **Herramientas administrativas**, ja que en petit ens dona la opció de *programar tareas*.
- Una vegada dins, crearem una a la pestanya de *Acción > Crear tarea...*
- Li donarem un nom i una descripció sencilla, que ens recordi que fa aquesta tasca.
- A la part de *desencadenadores* li direm cada quan volem que executi la tasca. [4]

![desencadenadores](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/Foto_tarea_creada.png)

[4]*la configuració que he fet per el servidor en la pestanya 'desencadenadores'*

- Llavors per casi finalitzar, entrarem a la part de *acciones* per dir-li quina comanda vol que fem. En el nostre cas com volem fer el **chkdsk**, doncs l'haurem de crear.
    - Amb el botó Nueva, haurem d'examinar per saber on es troba el *chkdsk.exe*, guardada a <C:\Windows\SysWOW64\chkdsk.exe>[5]
    
![acciones](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/foto_acci%C3%B3n.png)

[5] *La configuració a la pestanya 'acciones'.
- Guardem i ja la tendriem creada per entorn gràfic.
**chkdsk**: es una commanda utilitzada per comprobar la integritat tant de unitats de disc durs com unitats de discs flexibles, i per reparar errors logics en el sistema de arxius. [wiquipedia.org/wiki/chkdsk]

## Exercici 5
Per desactivar les actualitzacions automáticas s'ha de fer desde terminal, ja que desde el *Windows Update* és més complicat.
- Entrarem al PowerShell com *administrador*
- Escriu al terminal *sconfig* (Configuració del servidor) per iniciar la utilitat de la configuració del servidor.
- S'obrira una especie de "pantalla" on ens donaràn moltes opcions amb el seu numero per seleccionar-ho. Seleccionarem el numero **5** (Parametres de Windows Update).
- Ara afegeix la lletra **m** per configurar les actualitzacions manualment.
- Mostrara un missatje que ens indicara que Windows Update està configurat en mode manual. Clic a Aceptar.
