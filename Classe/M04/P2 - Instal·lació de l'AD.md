### *Marc Bruguera López - WISM1 - 13092021*

# P2 - Instal·lació de l'AD

## Índex
- [Instal·lació del Rol](#rol)
- [Install i config del domini](#domini)

### Rol
Com aquesta es la segona part de les multiples pràctiques que farém al nostre servidor, llavors en aquest apartat crearem un domini per *active directory*.

Començarém a instal·lar tot el tema com en l'apartat anterior, crean un nou rol per el nostre servidor. [1]

![Caracteristicas y rol](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_Rol_y_caracteristica.png) [1]

Agreguem la caracteristica, i una vegada que iniciem tot, ens sortirá un pop-up que ens preguntará basicament quin rol li volem ficar. Li agregarem el rol de **ACTIVE DIRECTORY de DOMINIO**, que es el que ens servirá per donar-li un domini.

Li donem següent.

La següent pestanya ens preguntara per si volem crear una delegació al DNS, no ho acceptem. I li donarem *siguiente* fins a l'intal·lació, ja que ens sortirá uns warnings, pero no els hi farém cas. [2]

![Intalación](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_instalando.png) [2]

### Domini
Una vegada amb el nostre rol instal·lat, ens saltarà un warning a la part de dalt al costat de la bandera. Li picarém a l'opció que ens dona i  li crearem un nom amb la seva contrasenya, com si en fos una pagina web. Jo li diré cfedt20.es amb la contrasenya *WinServer2019!*. Fen els següents passos. [3]

![Name_domini](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_dominioname.png) [3]

Afagirem el bloc amb el seu nom, dit amb anterioritat. Ara li donarém una contrasenya, anteriorment dita abans *WinServer2019!*, i li dirém quina "retrocompatibilitat" tindrá. [4-5]

![Opcions](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_opciones_retro.png)
[4] Les opcions que tenim alhora de escollir la retrocompatibilitat.

![Passwd_domini](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_passwd.png)
[5] La pantalla amb la contrasenya i les opcions que tením.

Una vegada instal·lat ens fará reiniciar el servidor i ficat el domini, es reiniciará sol. Deixem que es reinicií i ens cambiarà el nom del domini al següent: [6]

![ENTRADA](https://gitlab.com/mbruguera/class/-/raw/master/Pictures/activedirectory_-_cambio_de_nombre.png) [6]

Quasi al final ens surt com unes direccións de la ubicació d'arxius, es pot cambiar la direcció pero no els noms ja que estan predeterminats per el propi servidor i si es canvia, peta. Al igual que el sistema de fitxers, per determinat esta en NTFS ja que es el més eficient en Windows, pero tan fat com ext4 deixa instal·lar-ho pero donaría moltíssims errors.
